from flask.wrappers import Response
from flask_restplus import Namespace, fields
import os
class Management:
    api = Namespace('management', description='management core operations')