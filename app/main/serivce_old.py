import os

from sqlalchemy.sql.visitors import CloningVisitor
from sqlalchemy.sql.expression import true
import jwt
import json
import logging
import re
#from flask_weasyprint import HTML, render_pdf
from app.main import db
from sqlalchemy.sql import base, func, desc, asc, distinct
from sqlalchemy import extract, or_, and_, not_, exc, update
from app.main.model import *
import datetime
from flask import jsonify, render_template_string
import base64
import os
import sys
from decimal import Decimal
from sqlalchemy.sql.sqltypes import Enum, Numeric

DATABASE_NAME = os.environ["MYSQL_DATABASE"]
PAYROLL_DBNAME = os.environ['PAYROLL_DBNAME']
SECURIT_DBNAME = os.environ['SECURIT_DBNAME']
INVENTORY_DBNAME = os.environ['INVENTORY_DBNAME']

# response error method 🤝 🔂       
def responseError(error,status_code,*message):
    # message get like array, if you need return a message (Not required)⬇ 
    if message:
        return {'success':False,'message':message[0],'error':error},int(status_code)
    #  if not need message, just use error, and status code (http code)
    else:
        return {'success':False,'error':error},int(status_code)

def disciplinary_process_information(id):
    try:
        disciplinary_processes_detail = """
            SELECT * FROM `"""+ DATABASE_NAME +"""`.disciplinary_processes where id = {};
        """.format(id)
        sql = db.engine.execute(disciplinary_processes_detail)
        cont_row = sql.rowcount
        query = sql.fetchone()
        
        if cont_row != 0:
            result = dict(query)
            return {"success": True, "result": result}, 200
        else:
            return {"success": False, "message": "El detalle del proceso disciplinario no está disponible"}, 400
        
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        db.session.rollback()
        return {'success': False, 'message': e },400
    finally:
        db.session.close()

def get_disciplinary_process_detail(id):
    try:
        query = """
            SELECT * FROM `"""+ DATABASE_NAME +"""`.disciplinary_proc_details where id = {}
        """.format(id)
        sql = db.engine.execute(query)
        cont_row = sql.rowcount
        query = sql.fetchone()
        
        if cont_row != 0:
            result = dict(query)
            return {"success": True, "result": result}, 200
        else:
            return {"success": False, "result": ""}, 400
    
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        return responseError(e, 500)
    finally:
        db.session.close()
        
def update_disciplinary_process_detail(data, id):
    disciplinary_detail = get_disciplinary_process_detail(id)

    if not data:
        return {"success": False, "message": "Es necesario diligenciar la información para hacer alguna de las acciones dentro del proceso disciplinario."}, 400
    
    if not id:
        return {"success": False, "message": "El código del proceso disciplinario no está disponible"},400
    
    if not disciplinary_detail[0].get("success"):
        return {"success": False, "message": "El código del proceso disciplinario no está disponible"},400
    
    try:
    
        data_update = ""
        update_detail = """UPDATE `"""+ DATABASE_NAME +"""`.`disciplinary_proc_details` SET """
        
        if data.get("support_file"):
            if data_update == "":
                data_update = ' `support_file` = '+ "'" + data.get("support_file")+ "'"
            else:
                data_update = data_update+' , `support_file` ='+ "'" + data.get("support_file")+ "'"
        
        # where 
        update_detail = update_detail + data_update + " WHERE id = "+ id + ";"
        update_info = db.session.execute(update_detail)
        db.session.commit()
        
        return {"success": True, "message": "Proceso disciplinario se actualizó correctamente", "detail_disciplinary_process": id}, 200

    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        db.session.rollback()
        return {'success': False, 'message': e },400
    finally:
        db.session.close()

def update_disciplinary_process_actions(data, id):
    type_date = data.get("type_date")
    type = data.get("type")
    title = data.get("title")
    description = data.get("description")
    entity_account = data.get("entity_account")
    created_by = data.get("created_by")
    created_by_name = data.get("created_by_name")
    last_action = data.get("last_action")
    disc_process = disciplinary_process_information(id)
    status = ""
    
    if not entity_account:
        return {"success": False, "message": "La cuenta es un campo obligatorio"}, 400
    
    if not adjustment:
        return {"success": False, "message": "La deducción es un campo obligatorio"}, 400
    
    if not employee:
        return {"success": False, "message": "El empleado es un campo obligatorio"}, 400
    
    if not type:
        return {"success": False, "message": "El tipo de descuento o libranza es un campo obligatorio"}, 400
    
    if not start_date:
        return {"success": False, "message": "La fecha de inicio es un campo obligatorio"}, 400
    
    if not fee_value:
        return {"success": False, "message": "El valor de la cuota es un campo obligatorio"}, 400
    
    if not installments:
        return {"success": False, "message": "El número de cuotas es un campo obligatorio"}, 400
    
    # validations 
    if int(installments) < 0:
        return {"success": False, "message": "El número de cuotas debe ser mayor a 0"}, 400
    
    now = datetime.datetime.now()
    date_st = datetime.datetime.strptime(start_date,"%Y-%m-%d")
    
    if  date_st >=  now:
        return {"success": False, "message": "La fecha de inicio debe ser mayor o igual al día en curso."}, 400
    
    # End validations ########
    try:
        if not disc_process[0]["success"]:
            return {"success": False, "message": "Este proceso disciplinario no existe"}, 400
        
        if disc_process[0]["result"]["status"] == "closed":
            return {"success": False, "message": "Este proceso disciplinario ya está cerrado."}, 400
    
        if not type:
            return {"success": False, "message": "El tipo de acción sobre el proceso disciplinario es obligatorio, los tipos son citaciones, actas o registro de resultados."}, 400
        
        if not entity_account:
            return {"success": False, "message": "La cuenta en sesión es un atributo obligatorio."}, 400
        
        if not created_by:
            return {"success": False, "message": "El usuario que crea el registro es obligatorio."}, 400
        
        if not created_by_name:
            return {"success": False, "message": "El nombre del usuario que crea el registro es obligatorio."}, 400
            
        if type == "citation" or type == "minute":
            
            if not type_date:
                return {"success": False, "message": "La fecha es un campo obligatorio."}, 400
            
            if not title:
                return {"success": False, "message": "El concepto es un campo obligatorio"}, 400
            
            if not description:
                return {"success": False, "message": "La descripción es un campo obligatorio"}, 400
            
        elif type == "result":
            status = "closed"
            title = "Resultado del proceso disciplinario."
            
            # Update disciplinary process
            disciplinary_process = """
                UPDATE `"""+ DATABASE_NAME +"""`.`disciplinary_processes`
                SET
                `last_action` = :last_action,
                `status` = :status
                WHERE `id` = :id;
            """
            update_params = {
                "id": str(id),
                "last_action" : last_action,
                "status": status 
            }
            
            updated = db.session.execute(disciplinary_process, update_params)
            
        
        # Create disciplinary process detail
        detail_process_detail = """
            INSERT INTO `"""+ DATABASE_NAME +"""`.`disciplinary_proc_details`
            (`disciplinary_proc`, `type`, `type_date`, `title`,`description`, `created_by`, `created_by_name`, `created_at`, `entity_account`)
            VALUES                
            (:disciplinary_proc, :type, :type_date, :title,:description, :created_by, :created_by_name, :created_at, :entity_account);
        """
        
        params = {
            "type": type,
            "type_date": type_date,
            "title": title, 
            "created_at": datetime.datetime.utcnow(),
            "disciplinary_proc": str(id),
            "description": description,
            "created_by": created_by,
            "created_by_name": created_by_name,
            "entity_account": entity_account
        }
        
        result = db.session.execute(detail_process_detail, params)
        db.session.commit()
        detail = result.lastrowid
        
        return {"success": True, "message": "Proceso disciplinario gestionado.", "detail_disciplinary_process": detail}, 200
    
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        db.session.rollback()
        return {'success': False, 'message': e },400
    finally:
        db.session.close()

def get_complaint_info(id):
    if not id:
        return {"success": False, "message": "Esta queja o reclamo no está disponible."}, 400
    
    try:
        query = """
            SELECT * FROM `"""+ DATABASE_NAME +"""`.complaints where id = {}
        """.format(id)
        sql = db.engine.execute(query)
        cont_row = sql.rowcount
        query = sql.fetchone()
        
        if cont_row != 0:
            result = dict(query)
            return {"success": True, "result": result}, 200
        else:
            return {"success": False, "result": ""}, 400
    
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        return responseError(e, 500)
    finally:
        db.session.close()
        
        
def create_disciplinary_processes(data):
    try:
        employee = data.get("implicated_employees")
        if employee is not None or len(employee) != 0:
            values = ""
            i = 0
            
            for e in employee:
                values = values + "("+ "'"+str(data.get("complaint"))+"'" + "," + str(e) + "," + "'"+"pending"+"'" +")"
                i = i +1
                if i < len(employee):
                    values = values + ","
            
            disciplinary = """
                INSERT INTO `"""+ DATABASE_NAME +"""`.`disciplinary_processes`
                (`complaint`,`involved`, `status`)
                VALUES
            """
            disciplinary = disciplinary + values
            result_m = db.session.execute(disciplinary)
            db.session.commit()
            result_m.close()
            
            return {"success": True, "message": "Se han creado correctamente los procesos disciplinarios"}, 200     
        else:
            return {"success": False, "message": "Es necesario añadir los empleados a los cuales se les hará el llamado de atención"}, 400
    
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        db.session.rollback()
        return {'success': False, 'message': e },400
    finally:
        db.session.close() 
    
def create_develmot_annotaions(data):
    try:
        employee = data.get("implicated_employees")
        if employee is not None or len(employee) != 0:
            values = ""
            i = 0
            
            for e in employee:
                values = values + "("+ str(e) + "," + "'"+"Llamado de atención"+"'" + "," +  "'"+str(datetime.datetime.utcnow())+"'" + "," + "'"+data.get("annotations")+"'" + "," + str(data.get("result_by")) + "," +  "'"+data.get("result_by_name")+"'" + "," + "'"+str(datetime.datetime.utcnow())+"'" + "," + str(data.get("entity_account"))+")"
                i = i +1
                if i < len(employee):
                    values = values + ","
            
            develnot = """
                INSERT INTO `"""+ DATABASE_NAME +"""`.`develmot_annotations`
                (`employee`,`title`, `ann_date`, `description`, `created_by`, `created_by_name`, `created_at`,`entity_account`)
                VALUES
            """
            develnot = develnot + values
            result_m = db.session.execute(develnot)
            db.session.commit()
            result_m.close()
            return {"success": True, "message": "Se han creado correctamente las anotaciones para el llamado de atención"}, 200     
        else:
            return {"success": False, "message": "Es necesario añadir los empleados a los cuales se les hará el llamado de atención"}, 400
    
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        db.session.rollback()
        return {'success': False, 'message': e },400
    
    finally:
        db.session.close()        
    
def update_complaints_actions(data, id):
    
    complaint_if = get_complaint_info(id)
    
    if not data:
        return {"success": False, "message": "Es necesario diligenciar la información para hacer alguna de las acciones dentro del reporte o queja."}, 400
    
    if not id:
        return {"success": False, "message": "El código de la queja o reporte no está disponible"},400
    
    if not complaint_if[0].get("success"):
        return {"success": False, "message": "El código de la queja o reporte no está disponible"},400
    
    if complaint_if[0]["result"].get("status") != "pending":
        return {"success": False, "message": "El código de la queja o reporte no está disponible"},400
        
    justify = data.get("justification")
    result_type = data.get("result_type")
    
    if not result_type:
        return {"success": False, "message": "El tipo de resultado es un atributo obligatorio"}, 400
    
    if result_type == "received" or result_type == "memorandum" or result_type == "disciplinary":
        if not justify:
            return {"success": False, "message": "La justificación es un atributo obligatorio"}, 400
            
    try:
        aditional_info = {}
        update_actions_complaint = """
            UPDATE `"""+ DATABASE_NAME +"""`.`complaints`
            SET
            `status` = "done",
            `result_type` = :result_type,
            `result_justification` = :result_justification,
            `result_by` = :result_by,
            `result_at` = :result_at
            WHERE `id` = :id;
        """
        
        params = {
            "result_type": result_type,
            "result_justification": justify,
            "result_by": data.get("result_by"), 
            "result_at": datetime.datetime.utcnow(),
            "id": str(id)
        }
        
        result_m = db.session.execute(update_actions_complaint, params)
        db.session.commit()
        result_m.close()
        
        # if the result_type is memorandum, create develmot_annotations records
        if result_type == "memorandum":
            data["annotations"] = justify
            develmot_annotations = create_develmot_annotaions(data)
            
        # if the result_type is disciplinary, create disciplinary process records
        if result_type == "disciplinary":
            data["complaint"] = id
            disciplinary_process = create_disciplinary_processes(data)
        
        if result_type == "committee":
            info = {
                "entity_account": data.get("entity_account"),
                "id": id
            }
            additional = get_complaints_management(info)
            if additional[0]["success"]:
                aditional_info = additional[0]["results"][0]
        
        return {"success": True, "message": "Se ha actualizado correctamente el reclamo o la queja", "complaint": id, "aditional_info": aditional_info}, 200
    
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        db.session.rollback()
        return {'success': False, 'message': e },400
    
    finally:
        db.session.close()
        
def get_supports_collaborators_complaint(complaint):
    try:
        supports = """ 
            SELECT rel.employee, cd.full_name FROM `"""+ DATABASE_NAME +"""`.rel_complaint_employee as rel
            INNER JOIN `"""+ DATABASE_NAME +"""`.candidates as cd
            ON cd.id = rel.employee
            where complaint = {};
        """.format(complaint)
        sql = db.engine.execute(supports)
        complaint_employee = [(dict(row)) for row in sql.fetchall()]
        
        collaborators = """
            SELECT support_file FROM `"""+ DATABASE_NAME +"""`.rel_complaint_support where complaint = {};
        """.format(complaint)
        sql_ = db.engine.execute(collaborators)
        complaint_support = [(dict(row)) for row in sql_.fetchall()]
        
        return {'complaint_support': complaint_support, "complaint_employee": complaint_employee}, 200
        
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        db.session.rollback()
        return {'success': False, 'message': e },400
    finally:
        db.session.close()

def get_complaints_management(args):
    entity_account = args.get("entity_account")
    page = args.get("page")
    perpage = args.get("perpage")
    search = args.get("search")
    type_ = args.get("type")
    status = args.get("status")
    id = args.get("id")
    
    if entity_account is None:
        return {"success": False, "message": "El atributo cuenta en sesión es obligatorio"}, 400
    
    try:
        results = []
        records = """
            SELECT cp.id as complaint_id ,cd.id as candidate, cp.type, cd.full_name, cp.title, cp.area_name, st.value,
            stc.fontcolor, stc.background, DATE_FORMAT(cp.report_date, '%%d/%%m/%%Y') as report_date,
            cp.reporter, cp.description as complaint_description
            
            FROM `"""+ DATABASE_NAME +"""`.complaints as cp 
            INNER JOIN `"""+ DATABASE_NAME +"""`.candidates as cd
            ON cd.id = cp.reporter
            INNER JOIN `"""+ DATABASE_NAME +"""`.status as st
            ON cp.status = st.name
            INNER JOIN `"""+ DATABASE_NAME +"""`.status_color as stc
            ON stc.id = st.color_id
            where cp.entity_account = {0}
        """.format(entity_account)
        
        if id:
            records = records + " AND cp.id = " + str(id)
        
        if type_:
            records = records + ' AND cp.type = ' + '"' + str(type_) + '"'
        
        if status:
            records = records + ' AND cp.status = ' + '"' + str(status) + '"'
        
        if search:
            records = records + " AND (cp.title LIKE ('%%"+ str(search)+"%%')"
            records = records + " OR cd.full_name LIKE ('%%"+ str(search)+"%%'))"
        
        records = records + " group by cp.id"
        sql = db.engine.execute(records)
        cont_row = sql.rowcount
        
        if page and perpage:
            page = int(page)
            perpage = int(perpage)
            offset = (perpage*page)-perpage
            records = records + ' LIMIT '+ str(perpage) + ' OFFSET ' + str(offset)
        
        records+=';'
        
        final = db.engine.execute(records)
        for f in final:
            area_info = get_candidate_area_info(f["reporter"])
            complaint_info = get_supports_collaborators_complaint(f["complaint_id"])
            results.append({
                "complaint_id": f["complaint_id"],
                "type": "Queja" if f["type"] == "complaint" else "Reporte",
                "report": f["full_name"],
                "concept": f["title"],
                "status": f["value"],
                "fontcolor": f["fontcolor"],
                "background": f["background"],
                "report_date": f["report_date"],
                "job_position": area_info.get('job_title') if area_info.get('job_title') is not None else None,
                "area": area_info.get('area_name') if area_info.get('area_name') is not None else None,
                "report_description": f["complaint_description"],
                "supports": complaint_info[0]['complaint_support'] if complaint_info[0]['complaint_support'] is not None else None,
                "collaborators_involved": complaint_info[0]['complaint_employee'] if complaint_info[0]['complaint_employee'] is not None else None,
            })
            
        return {"success": True, 'row_total': cont_row, "results": results}, 200
    
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        db.session.rollback()
        return {'success': False, 'message': e },400
    finally:
        db.session.close()
#S T A R T      Payroll > create survey work environment #####################
def get_work_environment_survey(args):
    account = args.get("entity_account")
    page = args.get("page")
    perpage = args.get("perpage")
    survey_date= args.get("survey_date")
    title= args.get("title")
    
    if not account:
        return {"success": False, "message": "El atributo cuenta en sesión es obligatorio"}, 400
    
    try:
        query = """
            SELECT DATE_FORMAT(we.survey_date, '%%d/%%m/%%Y') as survey_date, we.id as id, we.title as title, we.surveyed as surveyed,
            we.satisfaction_level as satisfaction_level, we.conclusions as conclusions, we.actions_to_take as actions_to_take, 
            we.results_file as results_file
            FROM `"""+ DATABASE_NAME +"""`.work_environment as we where entity_account = {0}
        """.format(account)
        
        if survey_date:
            query = query + ' AND survey_date = ' + '"' + str(survey_date) + '"'
        
        if title:
            query = query + "AND title LIKE ('%%"+ str(title)+"%%')"
        
        query = query + " order by survey_date desc"
        sql = db.engine.execute(query)
        cont_row = sql.rowcount
        
        if page and perpage:
            page = int(page)
            perpage = int(perpage)
            offset = (perpage*page)-perpage
            query = query + ' LIMIT '+ str(perpage) + ' OFFSET ' + str(offset)
                
        query+=';'
        
        final = db.engine.execute(query)
        return jsonify({'results': [(dict(row)) for row in final.fetchall()], 'row_total': cont_row})
    
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        return {'success': False, 'message': e },400
    finally:
        db.session.close()

def work_environment_survey(id):
    survey_i = """
        SELECT * FROM `"""+ DATABASE_NAME +"""`.work_environment where id = {};
    """.format(id)
    sql = db.engine.execute(survey_i)
    cont_row = sql.rowcount
    query = sql.fetchone()
    
    if cont_row != 0:
        result = dict(query)
        return {"success": True, "result": result}, 200
    else:
        return {"success": False, "message": "La encuesta  no está disponible"}, 400
    
def update_work_environment_survey(data, id):
    data_update = ""
    update_work_environment_survey = ""
    
    # Get contract information
    info_adjustment = work_environment_survey(id)
    
    try:
        if not id:
            return {"success": False, "message": "La encuesta no está disponible."}, 400
        
        if not data:
            return {"success": False, "message": "Es necesario diligenciar la información para actualizar la encuesta."}, 400
        
        if not info_adjustment[0]["success"]:
            return {"success": False, "message": "Esta encuesta no existe"}, 400
        
        update_work_environment_survey = 'UPDATE `'+ DATABASE_NAME +'`.`work_environment` SET '
        
        if data.get("results_file"):
            if data_update == "":
                data_update = ' `results_file` = '+ "'" + data.get("results_file")+ "'"
            else:
                data_update = data_update+' , `results_file` ='+ "'" + data.get("results_file")+ "'"
        
        # where 
        update_work_environment_survey = update_work_environment_survey + data_update + " WHERE id = "+ id + ";"
        update_adjustment = db.session.execute(update_work_environment_survey)
        db.session.commit()
        update_adjustment.close()
        
        return {"success": True, "message": "Encuesta actualizada correctamente", "survey": id}, 200
        
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        return {'success': False, 'message': e },400
    
    finally:
        db.session.close()
    
    
def create_work_environment_survey(data):
    entity_account = data.get("entity_account")
    title = data.get("title")
    survey_date = data.get("survey_date")
    surveyed = data.get("surveyed")
    satisfaction_level = data.get("satisfaction_level")
    conclusions = data.get("conclusions")
    actions_to_take = data.get("actions_to_take")
    created_by = data.get("created_by")
    
    if not entity_account:
        return {"success": False, "message": "La cuenta es un campo obligatorio"}, 400
    
    if not title:
        return {"success": False, "message": "El título es un campo obligatorio"}, 400
    
    if not survey_date:
        return {"success": False, "message": "La fecha de encuenta es un campo obligatorio"}, 400
    
    if not surveyed:
        return {"success": False, "message": "El número de entrevistados es un campo obligatorio"}, 400
    
    if not satisfaction_level:
        return {"success": False, "message": "El nivel de satisfacción es un campo obligatorio"}, 400

    if not conclusions:
        return {"success": False, "message": "Las conclusiones son un campo obligatorio"}, 400
    
    if not actions_to_take:
        return {"success": False, "message": "las acciones a tomar son un campo obligatorio"}, 400
    
    if not created_by:
        return {"success": False, "message": "El usuario que crea el registro es un campo obligatorio"}, 400
    
    # validations 
    if int(surveyed) < 0:
        return {"success": False, "message": "El número de encuentados debe ser mayor a 0"}, 400
    
    if int(satisfaction_level) < 0 or int(satisfaction_level) > 100:
        return {"success": False, "message": "El nivel de satisfacción debe ser mayor a cero y menor e igual a 100"}, 400
    
    # End validations ########
    try:
        query = """ INSERT INTO `"""+ DATABASE_NAME +"""`.`work_environment`
                    (`title`,`survey_date`,`surveyed`,`satisfaction_level`,`conclusions`,`actions_to_take`,
                    `entity_account`,`created_by`,`created_at`)
                    
                    VALUES (:title,:survey_date,:surveyed,:satisfaction_level,:conclusions,
                :actions_to_take,:entity_account,:created_by,:created_at);
        """
        
        params = {
            "title": title,
            "survey_date": survey_date,
            "surveyed" : surveyed,
            "satisfaction_level" : satisfaction_level,
            "conclusions" : conclusions,
            "actions_to_take" : actions_to_take,
            "entity_account" : entity_account,
            "created_by": created_by,
            "created_at": datetime.datetime.utcnow()
        }
    
        result = db.session.execute(query, params)
        db.session.commit()
        lastInsert = result.lastrowid
        result.close()
        
        return {'success': True,  'message': "Encuesta creada exitosamente", 'id_inserted': lastInsert},200
    
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        return {'success': False, 'message': e },400
    finally:
        db.session.close()
##############################################################################
#S T A R T      Payroll > get adjustments ####################################
def get_dues_adjustment(adjustment_subscription):
    if not adjustment_subscription:
        return []
    
    try:
        query = """
            SELECT installment as "number", DATE_FORMAT(payment_date, '%%d/%%m/%%Y') as "Fecha" FROM `mod-payroll-core-dev`.rel_adjsubs_empdet where adj_subscription = {};
        """.format(adjustment_subscription)
        sql = db.engine.execute(query)
        cont_row = sql.rowcount
        finalresult = []
        if cont_row != 0:
            for s in sql.fetchall():
                rowdict = dict(s)
                finalresult.append(rowdict)
        sql.close()
        return finalresult

    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        return {'success': False, 'message': e },400
    
    finally:
        db.session.close()
    


def get_adjustments_by_type(arg):
    if arg is not None:
        type_ = arg.get("type")
        entity_account = arg.get("entity_account")
        
        try:
            query = """ 
                SELECT ad.id as adjustments_id, ad.type, ad.description, ad.recipient_id, ad.status ,th.nit, th.description AS third_parties_description, th.id as third_parties_id  
                FROM `"""+ DATABASE_NAME +"""`.adjustments as ad 
                LEFT JOIN `"""+ DATABASE_NAME +"""`.third_parties as th
                ON th.id = ad.recipient_id
                WHERE ad.type = """ + type_ + """ and ad.entity_account = """ + entity_account + """ order by recipient_id desc;
            """
            query+=';'
            sql = db.engine.execute(query)
            result = [(dict(row)) for row in sql.fetchall()]
            return {"success": True, "result": result},200
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            logging.error(exc_obj)
            logging.error(exc_type)
            logging.error(fname + ': ' + str(exc_tb.tb_lineno))
            return {'success': False, 'message': e },400
        finally:
            db.session.close()
    else:
        return {"success": False, "message": "No se está enviando el tipo de deducciones." }, 400

def adjustment_information(id):
    adjustment_i = """
        SELECT * FROM `"""+ DATABASE_NAME +"""`.adjustment_subscriptions where id = {};
    """.format(id)
    sql = db.engine.execute(adjustment_i)
    cont_row = sql.rowcount
    query = sql.fetchone()
    
    if cont_row != 0:
        result = dict(query)
        return {"success": True, "result": result}, 200
    else:
        return {"success": False, "message": "El descuento o libranza no está disponible"}, 400
    

def update_adjustment_subscription(data, id):
    data_update = ""
    update_adjustment_information = ""
    
    # Get contract information
    info_adjustment = adjustment_information(id)
    
    try:
        if not id:
            return {"success": False, "message": "Este descuento no está disponible."}, 400
        
        if not data:
            return {"success": False, "message": "Es necesario diligenciar la información para actualizar el descuento."}, 400
        
        if not info_adjustment[0]["success"]:
            return {"success": False, "message": "Este descuento no existe"}, 400
        
        update_adjustment_information = 'UPDATE `'+ DATABASE_NAME +'`.`adjustment_subscriptions` SET '
        
        if data.get("support_file"):
            if data_update == "":
                data_update = ' `support_file` = '+ "'" + data.get("support_file")+ "'"
            else:
                data_update = data_update+' , `support_file` ='+ "'" + data.get("support_file")+ "'"
        
        # where 
        update_adjustment_information = update_adjustment_information + data_update + " WHERE id = "+ id + ";"
        update_adjustment = db.session.execute(update_adjustment_information)
        db.session.commit()
        update_adjustment.close()
        
        return {"success": True, "message": "Descuento actualizado correctamente", "adjustment": id}, 200
        
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        return {'success': False, 'message': e },400
    
    finally:
        db.session.close()

def create_adjustment_subscription(data):
    employee = data.get("employee")
    adjustment = data.get("adjustment")
    type = data.get("type")
    start_date = data.get("start_date")
    installments = data.get("installments")
    fee_value = data.get("fee_value")
    description = data.get("description")
    entity_account = data.get("entity_account")
    created_by = data.get("created_by")
    
    if not entity_account:
        return {"success": False, "message": "La cuenta es un campo obligatorio"}, 400
    
    if not adjustment:
        return {"success": False, "message": "La deducción es un campo obligatorio"}, 400
    
    if not employee:
        return {"success": False, "message": "El empleado es un campo obligatorio"}, 400
    
    if not type:
        return {"success": False, "message": "El tipo de descuento o libranza es un campo obligatorio"}, 400
    
    if not start_date:
        return {"success": False, "message": "La fecha de inicio es un campo obligatorio"}, 400
    
    if not fee_value:
        return {"success": False, "message": "El valor de la cuota es un campo obligatorio"}, 400
    
    if not installments:
        return {"success": False, "message": "El número de cuotas es un campo obligatorio"}, 400
    
    # validations 
    if int(installments) < 0:
        return {"success": False, "message": "El número de cuotas debe ser mayor a 0"}, 400

    
    now = datetime.datetime.now()
    date_st = datetime.datetime.strptime(start_date,"%Y-%m-%d")
   
    
    if  date_st >= now:
        return {"success": False, "message": "La fecha de inicio debe ser mayor o igual al día en curso."}, 400
    
    # End validations ########
    try:
        query = """ INSERT INTO `"""+ DATABASE_NAME +"""`.`adjustment_subscriptions`
            (`employee`,`adjustment`,`type`,`start_date`,`installments`,`fee_value`,`description`,`status`,`entity_account`,`created_at`,`created_by`)
            VALUES (:employee, :adjustment, :type, :start_date, :installments, :fee_value, :description, :status, :entity_account, :created_at, :created_by);
        """
        
        params = {
            "employee": employee,
            "adjustment": adjustment,
            "type" : type,
            "start_date" : start_date,
            "installments" : installments,
            "fee_value" : fee_value,
            "description" : description,
            "status": "current",
            "entity_account" : entity_account,
            "created_at": datetime.datetime.utcnow(),
            "created_by": created_by
        }
    
        result = db.session.execute(query, params)
        db.session.commit()
        lastInsert = result.lastrowid
        result.close()
        
        return {'success': True, 'employee': data.get("employee"), 'message': "Descuento o libranza creada exitosamente", 'id_inserted': lastInsert},200
    
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        return {'success': False, 'message': e },400
    finally:
        db.session.close()
#END      Payroll > get adjustments ####################################

PAYROLL_DBNAME = os.environ['PAYROLL_DBNAME']
SECURIT_DBNAME= os.environ['SECURIT_DBNAME']
# S T A R T     Payroll > get Payroll Sheet
def Accounting__payrollSheet_get(data):
    if not data.get("eaccount", type = int):
        return responseError("error",400,"Cuenta en sesión no enviada")
    payrollDBName = os.environ['PAYROLL_DBNAME']
    securityDBName = os.environ['SECURIT_DBNAME']
    try :
        params = {}
        if data.get("slim", type = int) == 1:
            return {"error":"Not ready"}
        else:
            fieldsCount = "SELECT COUNT(1) "
            fields = """ 
            SELECT
                p.id,
                p.details,
                p.payment_year year,
                p.payment_month month,
                p.total_amount totalAmount,
                p.payment_date date,
                p.range,
                p.status,
                p.created_at createdAt,
                p.created_by createdBy,
                CONCAT(u.first_name,' ',u.first_surname) createdByName,
                st.value statusName,
                COALESCE(cont.employeeCount,0) employeeCount,
                m.name monthES """
            tables = """ FROM """+ payrollDBName +""".payroll_sheet p
            INNER JOIN """+ payrollDBName +""".status st ON (st.name = p.status AND st.prefix='payroll')
            INNER JOIN """+ payrollDBName +""".month_ES m ON (m.id = MONTH(p.payment_date))
            INNER JOIN """+ securityDBName +""".users u ON (u.id = p.created_by)
            LEFT JOIN (SELECT payroll_template id, COUNT(*) employeeCount FROM """+ payrollDBName +""".payroll_employee_detail GROUP BY payroll_template) AS cont ON (cont.id = p.id) """  
            filters = " WHERE p.entity_account = :eaccount "
        params["eaccount"] = data.get("eaccount", type = int)
            
        if data.get("id", type = int):
            filters +=" AND p.id = :id "
            params['id'] = data.get("id", type = int)

        if data.get("status", type = str):
            filters +=" AND p.status = :status "
            params['status'] = data.get("status", type = str)

        if data.get("module", type = str) == "payrollAuth":
            filters +=" AND p.status IN ('review','approved','rejected') "
                    
        if data.get("dateFrom", type=str):
            filters +=" AND DATE(p.payment_date) >= :dateFrom "
            params['dateFrom'] = data.get("dateFrom", type=str)
                    
        if data.get("dateUntil", type=str):
            filters +=" AND DATE(p.payment_date) <= :dateUntil "
            params['dateUntil'] = data.get("dateUntil", type=str)
            
        group = "  "
        
        pagination = ""
        if data.get("page", type = int) is not None and data.get("perpage", type = int) is not None:
            offset = data.get("perpage", type = int) * (data.get("page", type = int) - 1)
            pagination =" LIMIT :perpage OFFSET :offset "
            params['offset'] = offset
            params['perpage'] = data.get("perpage", type = int)

        resultcount = 0
        resultcount = db.session.execute(fieldsCount + tables + filters + pagination + group, params).scalar()
        result = db.session.execute(fields + tables + filters + pagination + group, params)
        finalresult = []
        
        for row in result.fetchall():
            dictrow = dict(row)
            myrow = {}
            for i in dictrow:
                myrow[i]=json_serial(dictrow[i])
                
            finalresult.append(myrow)
        if data.get("id", type=int):
            finalresult = finalresult[0]
            finalresult["detail"] = Accounting__payrollSheet_getOne(data.get("id", type=int))

        result.close()
        return {'success': True, 'results':finalresult  , 'row_total': resultcount},200
    except Exception as e:
        return {'error': str(e)}, 400
# END     Payroll > get Payroll Sheet
# START         Payroll > get ONe Payroll Sheet
def Accounting__payrollSheet_getOne(id):
    payrollDBName = os.environ['PAYROLL_DBNAME']
    administrationDBName = os.environ['ADMINIS_DBNAME']
    try :
        params = {}
        query = """ 
            SELECT
                c.id employeeId,
                c.full_name employeeName,
                n.complete_account niifAccountNbr,
                ct.base_salary salary,
                0 discount,
                0 received,
                d.total_amount totalAmount
        FROM """+ payrollDBName +""".payroll_employee_detail d
            INNER JOIN """+ payrollDBName +""".candidates c ON (c.id = d.employee)
            INNER JOIN """+ payrollDBName +""".contracts ct ON (ct.candidate = c.id AND d.contract_id = ct.id)
            INNER JOIN """+ administrationDBName +""".niifAccount n ON (n.id = d.niff_account) 
        WHERE d.payroll_template = :id """
        params["id"] = id
        
        result = db.session.execute(query, params)
        finalresult = []
        
        for row in result.fetchall():
            dictrow = dict(row)
            myrow = {}
            for i in dictrow:
                myrow[i]=json_serial(dictrow[i])
                
            finalresult.append(myrow)
        return finalresult
    except Exception as e:
        return []
# END         Payroll > get One Payroll Sheet
# S T A R T     Payroll > auth
def Accounting__payrollSheet_auth(data):
    #Parameters validation
    if not data.get('eaccount'):
        return responseError("error",400,"Cuenta en sesión no enviada")
    if not data.get("payroll"):
        return responseError("error",400,"No se recibió una nómina para procesar")
    if not data.get("status"):
        return responseError("error",400,"Revisar estado")
    if not data.get("user"):
        return responseError("error",400,"Usuario no enviado")
    
    #status validation
    status = data.get("status")
    if status!="approved" and status!="rejected":
        return responseError("error",400,"Revisar estado")
    if status=="rejected":
        if not data.get("rejected"):
            return responseError("error",400,"Enviar justificación")

    payrollDBName = os.environ['PAYROLL_DBNAME']
    try :
        params = {}
        query = """ 
        SELECT
            m.name monthES,
            p.payment_year year,
            DAY(p.payment_date) day
        FROM """+ payrollDBName +""".payroll_sheet p
        INNER JOIN """+ payrollDBName +""".month_ES m ON (m.id = MONTH(p.payment_date))
        WHERE 
            p.entity_account = :eaccount 
            AND p.id = :id """
        params["eaccount"] = data.get("eaccount")
        params["id"] = data.get("payroll")

        finalresult = []
        result = db.session.execute(query, params)
        for row in result.fetchall():
            dictrow = dict(row)
            myrow = {}
            for i in dictrow:
                myrow[i]=json_serial(dictrow[i])
                
            finalresult.append(myrow)
        if len(finalresult)<=0:
            return responseError("error",400,"Nómina no encontrada")        
        
        statusMessage = "aprobada"
        if status == "rejected":
            statusMessage="denegada"

        paramsUpdate = {}
        rejectComment = ""
        if status=="rejected":
            rejectComment = data.get("rejected")
        queryupdate = """
            UPDATE """+ payrollDBName +""".payroll_sheet 
            SET status = :status,
            auth_at = LOCALTIMESTAMP(),
            auth_by = :user,
            reject_description = :reject
            WHERE id = :id
            AND entity_account = :eaccount
        """
        paramsUpdate["status"] = data.get("status")
        paramsUpdate["user"] = data.get("user")
        paramsUpdate["id"] = data.get("payroll")
        paramsUpdate["eaccount"] = data.get("eaccount")
        paramsUpdate["reject"] = rejectComment

        db.session.execute(queryupdate, paramsUpdate)
        db.session.commit()
        
        return {'success': True,"message":"Planilla "+statusMessage,'results':finalresult},200
    except Exception as e:
        return {'error': str(e)}, 400
# END     Payroll > auth


def medical_exams_detail(id):
    try:
        medical_exam_detail = """
            SELECT * FROM `"""+ DATABASE_NAME +"""`.medical_exam_details where id = {};
        """.format(id)
        sql = db.engine.execute(medical_exam_detail)
        cont_row = sql.rowcount
        query = sql.fetchone()
        
        if cont_row != 0:
            result = dict(query)
            return {"success": True, "result": result}, 200
        else:
            return {"success": False, "message": "El detalle del exámen médico no está disponible"}, 400
        
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        db.session.rollback()
        return {'success': False, 'message': e },400
    finally:
        db.session.close()
            
def update_resume_medical_exams_detail(data, id):
    data_update = ""
    update_medical_exam_detail_information = ""
    # Get contract information
    info_medical_exams_detail = medical_exams_detail(id)
    
    if not id:
        return {"success": False, "message": "El detalle del examen médico no está disponible."}, 400
    
    if not data:
        return {"success": False, "message": "Es necesario diligenciar la información para actualizar el detalle del exámen médico."}, 400
    
    if not info_medical_exams_detail[0]["success"]:
        return {"success": False, "message": "Este detalle de exámen médico no existe"}, 400
    
    update_medical_exam_detail_information = """UPDATE `"""+ DATABASE_NAME +"""`.`medical_exam_details` SET """
    
    if data.get("result_location"):
        if data_update == "":
            data_update = ' `result_location` = '+ "'" + data.get("result_location")+ "'"
        else:
            data_update = data_update+' , `result_location` ='+ "'" + data.get("result_location")+ "'"
    
    # where 
    update_medical_exam_detail_information = update_medical_exam_detail_information + data_update + " WHERE id = "+ id + ";"
    update_medical_detail = db.session.execute(update_medical_exam_detail_information)
    db.session.commit()
    update_medical_detail.close()
    return {"success": True, "message": "Exámen médico actualizado correctamente", "medical_Exam_detail": id}, 200
    
def get_candidate_area_info(candidate):
    try:
        query = """ SELECT jb.job_description, jb.job_title, jb.area_id, jb.area_name 
                    FROM `"""+ DATABASE_NAME +"""`.contracts as ct 
                    INNER JOIN job_positions as jb
                    ON jb.id = ct.job_position
                    WHERE ct.candidate = {0} and ct.status = "signed"
                    group by ct.id
                    limit 1
                """.format(candidate)
        query+=';'
        sql = db.engine.execute(query)
        area = sql.fetchone()
        return {"job_description": area["job_description"], "job_title": area["job_title"], "area_id":area["area_id"] if area["area_id"] is not None else None , "area_name": area["area_name"] if area["area_name"] is not None else None}
    
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        return responseError(e, 500)
    finally:
        db.session.close()
        
def create_resume_medical_exams(data):
    entity_account = data.get("entity_account")
    candidate = data.get("candidate")
    title = data.get("title")
    prog_date = data.get("prog_date")
    done_date = data.get("done_date")
    created_by = data.get("created_by")
    result_loaded_by = data.get("result_loaded_by")
    result_loaded_by_name = data.get("result_loaded_by_name")
    
    if not entity_account:
        return {"success": False, "message": "La cuenta es un campo obligatorio"}, 400

    if not candidate:
        return {"success": False, "message": "El candidato es un campo obligatorio"}, 400
    
    if not title:
        return {"success": False, "message": "El título es un campo obligatorio"}, 400
    
    if not prog_date:
        return {"success": False, "message": "La fecha de programación de exámen es un campo obligatorio"}, 400
    
    if not done_date:
        return {"success": False, "message": "La fecha del exámen es un campo obligatorio"}, 400
    
    if not created_by:
        return {"success": False, "message": "El usuario que crea el registro es un campo obligatorio"}, 400
    
    if not result_loaded_by:
        return {"success": False, "message": "El usuario que carga el resultado del exámen es un campo obligatorio"}, 400
    
    if not result_loaded_by_name:
        return {"success": False, "message": "El nombre del usuario que carga el resultado del exámen es un campo obligatorio"}, 400
    
    try:
        area_info = get_candidate_area_info(candidate)
    
        insert_medical_exams = """
            INSERT INTO `"""+ DATABASE_NAME +"""`.`medical_exams`
            (`title`,`area_id`,`area_name`,`prog_date`,`done_date`,`created_by`,`created_at`,`result_loaded_by`,
            `result_loaded_by_name`,`entity_account`)
            VALUES (:title,:area_id, :area_name, :prog_date, :done_date, :created_by, :created_at, :result_loaded_by,
            :result_loaded_by_name, :entity_account);
        """
        
        params = {
            "title" : title,
            "prog_date" : prog_date,
            "done_date" : done_date,
            "created_by" : created_by,
            "created_at" : datetime.datetime.utcnow(),
            "result_loaded_by" : result_loaded_by,
            "result_loaded_by_name" : result_loaded_by_name,
            "entity_account" : entity_account,
            "area_id": area_info.get('area_id') if area_info.get('area_id') is not None else None,
            "area_name": area_info.get('area_name') if area_info.get('area_name') is not None else None,
        }
        
        result_m = db.session.execute(insert_medical_exams, params)
        db.session.commit()
        result_m.close()
        medical_exam = result_m.lastrowid
        
        if medical_exam is not None and medical_exam != 0:
            insert_medical_exams_detail = """
                INSERT INTO `"""+ DATABASE_NAME +"""`.`medical_exam_details`
                (`medical_exam`, `employee`,`result_loaded_at`,`status`)
                VALUES(:medical_exam, :employee, :result_loaded_at, :status);
            """
            params_ = {
                "medical_exam" : medical_exam,
                "employee" : candidate,
                "result_loaded_at" : datetime.datetime.utcnow(),
                "status": "done"
            }
            
            result_mex = db.session.execute(insert_medical_exams_detail, params_)
            db.session.commit()
            result_mex.close()
            medical_exam_detail = result_mex.lastrowid
        
        return {"success": True, "message": "Registro exitoso de exámen médico", "candidate": candidate,"medical_exam": medical_exam, "medical_exam_detail": medical_exam_detail}, 200
        
        
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        db.session.rollback()
        return {'success': False, 'message': e },400
    
    finally:
        db.session.close()

def get_resume_medical_exams(args):
    candidate = args.get("candidate")
    entity_account = args.get("entity_account")
    
    if not candidate or not entity_account:
        return {"success": False, "message": "Los atributos candidatos y cuenta en sesión son obligatorios"}, 400
    
    try:
        result_info_medical = []
        result_info_historial = []
        next_exam_r = []
        
        # Medical information
        medical_info = """
            SELECT cdt.disabilities, cdt.blood_type, cdt.rh_factor, concat(cdt.weight_kg, " kg") as "weight_kg",  
            concat(cdt.heigth_cm, " cm") as "heigth_cm", cdt.known_diseases as known_diseases,
            cdt.drug_treatment as drug_treatment
            FROM `mod-payroll-core-dev`.candidates as cdt
            WHERE cdt.id = {0} and cdt.entity_account = {1};
        """.format(candidate, entity_account)
        sql = db.engine.execute(medical_info)
        query = sql.fetchone()
        result_info_medical = dict(query)
        
        # history exam information
        historial = """
            SELECT md_e.result_location, med.title, DATE_FORMAT(med.done_date, '%%d/%%m/%%Y') AS done_date,
            med.result_loaded_by_name
            FROM `mod-payroll-core-dev`.medical_exams as med
            INNER JOIN medical_exam_details as md_e
            ON med.id = md_e.medical_exam
            WHERE md_e.employee = {0} and med.entity_account = {1} and med.done_date is not null;
        """.format(candidate, entity_account)
        sql_1 = db.engine.execute(historial)
        result_info_historial = [(dict(row)) for row in sql_1.fetchall()]
        
        # Date of next exams
        next_exam = """
            SELECT DATE_FORMAT(med.prog_date, '%%d/%%m/%%Y') as next_exam
            FROM `mod-payroll-core-dev`.medical_exams as med
            INNER JOIN medical_exam_details as md_e
            ON med.id = md_e.medical_exam
            where md_e.employee = {} and med.entity_account = {} and 
            med.prog_date is not null and med.done_date is null limit 1;
        """.format(candidate, entity_account)
        sql_2 = db.engine.execute(next_exam)
        next_exam_r = sql_2.fetchone()
            
        return {'success': True, 'results':{
            "next_exam": next_exam_r["next_exam"] if next_exam_r is not None else "",
            "historial": result_info_historial,
            "medical_info": result_info_medical
        }}, 200
        
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        return {'success': False, 'message': e },400
    
    finally:
        db.session.close()
        

# This method receives a params array [] 🤞 
def responseSuccess(results,message,row_total):
    return jsonify({
        'success':True,
        'message':message,
        'results':results,
        'row_total':row_total
    })

def json_serial(obj):
    """JSON serializer for objects not serializable by default json code"""

    if isinstance(obj, (datetime.datetime, datetime.date)):
        return obj.isoformat()
    elif isinstance(obj, Decimal):
        return float(obj) 
    elif isinstance(obj, Enum):
        return str(obj)
    return obj

def consecutive_get(id_consec):
    db.session.execute("CALL getconsec ("+str(id_consec)+",7,@seq);")
    aux = db.session.execute("SELECT @seq;")
    consec = aux.fetchone()
    db.session.commit()
    return consec[0] if len(consec) > 0 else ""


def return_payment_data(period, eaccount):
    payrollDBName = os.environ['PAYROLL_DBNAME']
    administrationDBName = os.environ['ADMINIS_DBNAME']
    try :
        params = {}
        query = """ 
            SELECT * FROM (
                SELECT 
                `a`.`id` AS `id`,
                `a`.`details` AS `details`,
                `a`.`total_amount` AS `total_amount`,
                `a`.`entity_account` AS `entity_account`,
                CONCAT(`a`.`payment_year`,
                        ' ', `m`.`name`, ' (', `a`.`range`, ')') AS `period`,
                json_arrayagg(
                    json_object(
                        "niff_account",b.account_nbr,
                        "full_name", c.full_name,
                        "debit_money", b.total_amount,
                        "credit_money", "0" )
                    ) as detail
            FROM """+ payrollDBName +""".payroll_sheet a
            INNER JOIN """+ payrollDBName +""".payroll_employee_detail b ON (a.id = b.payroll_template)
            INNER JOIN """+ payrollDBName +""".candidates c ON (b.employee = c.id)
            INNER JOIN """+ payrollDBName +""".month_ES m ON (m.id = a.payment_month) 
            ) as det where true 
        """ 

        if period is not None:
            query = query + " AND period =:period" 
            params["period"] = period
        
        if eaccount is not None:
            query = query + " AND entity_account =:eaccount" 
            params["eaccount"] = eaccount
        
        result = db.session.execute(query, params)
        finalresult = []
        
        for row in result.fetchall():
            dictrow = dict(row)
            myrow = {}
            for i in dictrow:
                if i != "detail":
                    myrow[i]=json_serial(dictrow[i])
                else:
                    myrow[i]=json.loads( dictrow[i]) 
                
            finalresult.append(myrow)           

        result.close()
        return {'success': True, 'results': finalresult},200
   
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        return {'success': False, 'results': []},500


# S T A R T  Payroll > Periods in Payroll payment
def optionsOfPeriod(eaccount):
    payrollDBName = os.environ['PAYROLL_DBNAME']
    try :
        params = {}
        query = """ 
            SELECT 
            `a`.`id` AS `id`,
             `a`.`entity_account` AS `entity_account`,
            CONCAT(`a`.`payment_year`,
                ' ', `m`.`name`, ' (', `a`.`range`, ')') AS `period`
            FROM """+ payrollDBName +""".payroll_sheet a
            INNER JOIN """+ payrollDBName +""".month_ES m ON (m.id = a.payment_month) 
            WHERE `a`.`status`= "approved"
        """ 
        if eaccount is not None:
            query = query + " AND entity_account =:eaccount" 
            params["eaccount"] = eaccount
        
        result = db.session.execute(query, params)
        finalresult = []
        for row in result.fetchall():
            dictrow = dict(row)
            myrow = {}
            for i in dictrow:
                myrow[i]=json_serial(dictrow[i])
                
            finalresult.append(myrow)   
        result.close()
        return {'success': True, 'results': finalresult},200
        
    except Exception as e:
        return {'success': False, 'results': []},401

def get_job_positions(data):
    try :
        params = {}
        if data.get("slim", type = int) == 1:
            query = """ SELECT DISTINCT a.id, a.job_title, b.id as contract_id, a.area_id FROM """+PAYROLL_DBNAME+""".job_positions a
            LEFT JOIN """+PAYROLL_DBNAME+""".contracts AS b ON b.job_position = a.id
            LEFT JOIN """+PAYROLL_DBNAME+""".candidates AS c ON b.candidate = c.id
            WHERE job_title!='' """
            querycount = """ SELECT COUNT(1) FROM """+PAYROLL_DBNAME+""".job_positions a 
            LEFT JOIN """+PAYROLL_DBNAME+""".contracts AS b ON b.job_position = a.id
            LEFT JOIN """+PAYROLL_DBNAME+""".candidates AS c ON b.candidate = c.id
            WHERE job_title!=''  """
        else:
            current = ""
            if data.get("onlyCurrent", type = str):
                current +=" AND b.status = '{}'".format(data.get("onlyCurrent", type = str))
            query = """ 
            SELECT a.* ,
            COUNT(c.id) employees_count,
            b.status as status_contract,
            JSON_ARRAYAGG(JSON_OBJECT("id",c.id,"name",c.full_name,"date",b.start_date,"doc", c.doc_number)) employees,
            (SELECT JSON_ARRAYAGG(comp_doc) FROM """+PAYROLL_DBNAME+""".rel_jselect ovp.id from overtime_params ovpobpos_compdoc WHERE job_position = a.id) as documents
            FROM """+PAYROLL_DBNAME+""".job_positions  a
            LEFT JOIN """+PAYROLL_DBNAME+""".contracts AS b ON b.job_position = a.id """+current+"""
            LEFT JOIN """+PAYROLL_DBNAME+""".candidates AS c ON b.candidate = c.id
            WHERE 1=1 """
            querycount = """ SELECT count(distinct a.id),  a.*,  b.status as status_contract  FROM """+PAYROLL_DBNAME+""".job_positions  a
            LEFT JOIN """+PAYROLL_DBNAME+""".contracts AS b ON b.job_position = a.id
            LEFT JOIN """+PAYROLL_DBNAME+""".candidates AS c ON b.candidate = c.id
            WHERE 1=1 """            
            
        if data.get("id", type = int):
            query +=" AND a.id = :id "
            querycount += " AND a.id = :id "
            params['id'] = data.get("id", type = int)

        if data.get("area_id", type = int):
            query +=" AND a.area_id = :area_id "
            querycount += " AND a.area_id = :area_id "
            params['area_id'] = data.get("area_id", type = int)
        
        if data.get("job_title", type = str):
            query +=" AND a.job_title LIKE ('%%"+ data.get("job_title", type = str)+"%%')"
            querycount += " AND a.job_title LIKE ('%%"+ data.get("job_title", type = str)+"%%')"
            params['job_title'] = data.get("job_title", type = str)
        
        if data.get("area_name", type = str):
            query +=" AND a.area_name LIKE ('%%"+ data.get("area_name", type = str)+"%%')"
            querycount += " AND a.area_name LIKE ('%%"+ data.get("area_name", type = str)+"%%')"
            params['area_name'] = data.get("area_name", type = str)
        

        if data.get("area_leader", type = int) is not None:
            query +=" AND a.area_leader = :area_leader "
            querycount += " AND a.area_leader = :area_leader "
            params['area_leader'] = data.get("area_leader", type = int)
            
        if data.get("supervisor_pos", type = int) is not None:
            query +=" AND a.supervisor_pos = :supervisor_pos "
            querycount += " AND a.supervisor_pos = :supervisor_pos "
            params['supervisor_pos'] = data.get("supervisor_pos", type = int)

        if data.get("extra_hours", type = str):
            query +=" AND a.extra_hours = :extra_hours "
            querycount += " AND a.extra_hours = :extra_hours "
            params['extra_hours'] = data.get("extra_hours", type = str)

        if data.get("work_time", type = str):
            query +=" AND a.work_time = :work_time "
            querycount += " AND a.work_time = :work_time "
            params['work_time'] = data.get("work_time", type = str)

        if data.get("entity_account", type = int):
            query +=" AND a.entity_account = :entity_account "
            querycount += " AND a.entity_account = :entity_account "
            params['entity_account'] = data.get("entity_account", type = int)

        if data.get("active", type = str):
            query +=" AND a.active = :active "
            querycount += " AND a.active = :active "
            params['active'] = data.get("active", type = str)
    
        if data.get("created_by", type = int):
            query +=" AND a.created_by = :created_by "
            querycount += " AND a.created_by = :created_by "
            params['created_by'] = data.get("created_by", type = int)
                    
        if data.get("status", type = int):
            query +=" AND a.status = :status "
            querycount += " AND a.status = :status "
            params['status'] = data.get("status", type = int)

        if data.get("created_at", type=str):
            parsedate = datetime.datetime.strptime(data.get("created_at", type=str), '%Y-%m-%d')
            query +=" AND DATE(a.created_at) = :created_at "
            querycount +=" AND DATE(a.created_at) = :created_at "
            params['created_at'] = parsedate
        
        if data.get("search", type = str):
            query +=" AND (a.job_title LIKE ('%%"+ data.get("search", type = str)+"%%') OR a.id = :search )"
            querycount += " AND (a.job_title LIKE ('%%"+ data.get("search", type = str)+"%%') OR a.id = :search )"
            params['search'] = data.get("search", type = str)

        if data.get("id_candidate", type = int):
            query +=" AND c.id = :id_candidate "
            querycount += " AND c.id = :id_candidate "
            params['id_candidate'] = data.get("id_candidate", type = int)
            
        query += " GROUP BY a.id"
        resultcount = db.session.execute(querycount, params).scalar()
        
        if data.get("page", type = int) is not None and data.get("perpage", type = int) is not None:
            offset = data.get("perpage", type = int) * (data.get("page", type = int) - 1)
            query +=" LIMIT :perpage OFFSET :offset "
            params['offset'] = offset
            params['perpage'] = data.get("perpage", type = int)

        result = db.session.execute(query, params)
        finalresult = []  
        for row in result.fetchall():
            dictrow = dict(row)
            myrow = {}
            for i in dictrow:
                if i != "employees":
                    myrow[i]=json_serial(dictrow[i])
                else:
                    myrow[i]=json.loads( dictrow[i]) # esto sirve para convertir el array si viene una respuesta en  
                
            finalresult.append(myrow)           

        result.close()
        return {'success': True, 'results':finalresult  , 'row_total': resultcount},200
    except Exception as e:
        return {'error': str(e)}, 400
       
def list_filters(type, params):
    # If vacancy type is direct:
    if type:
        if type == "2":
            # Get all job positions
            jobPositions = get_job_position_simple(params)
            
            # Get all contract types 
            contractTypes = get_contrat_types_simple()
            
            # Get all candidates 
            candidates = list_candidates(params, type, None)
        
        # if the vacancy type is announcement
        elif type == "1":
            
            # Get position from announcement
            jobPositions = get_position_from_announcement(params)
            
            # Get contract type from announcement
            contractTypes = get_contract_type_from_announcement(params)
            
            # Get candidates from announcement
            candidates = list_candidates(params, type, jobPositions)
        
        
        if jobPositions["success"] and contractTypes["success"] and candidates["success"]:
            return {"success": True, "jobPositions": jobPositions["results"], "contractTypes": contractTypes["results"], "candidates": candidates["results"]}
        else:
            return {"success": False, "jobPositions": [], "contractTypes": [], "candidates": []}
    
    else:
        return {"success": False, "message": "Debe escoger el proceso de contratación (vacancy Type) para listar la información"}, 400

def get_position_from_announcement(data):
    announcement = data.get("announcement")
    entity_account = data.get("entity_account")
    try:
        if announcement and entity_account:
            query = """ 
                SELECT jb.id, jb.job_title, jb.job_description FROM `mod-payroll-core-dev`.announcements as an 
                INNER JOIN `mod-payroll-core-dev`.job_positions as jb
                ON an.job_position = jb.id
                
                where an.id ={0}  and an.entity_account = {1}
            """.format(announcement, entity_account)
            
            query+=';'
            
            res = db.session.execute(query)
            result = [(dict(row)) for row in res.fetchall()]
            res.close()
            return {'success': True, 'results':result}
        else:
            return {'success': False, 'message': "El atributo de convocatoria y la cuenta en sesión son obligatorios" },400
        
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        return responseError(e, 500)
    finally:
        db.session.close()

def get_contract_type_from_announcement(data):
    announcement = data.get("announcement")
    entity_account = data.get("entity_account")
    try:
        if announcement and entity_account:
            query = """ 
                SELECT ct.id, ct.description FROM `mod-payroll-core-dev`.announcements as an 
                INNER JOIN `mod-payroll-core-dev`.contract_types as ct
                ON an.contract_type = ct.id
                
                where an.id ={0}  and an.entity_account = {1}
            """.format(announcement, entity_account)
            
            query+=';'
            res = db.session.execute(query)
            result = [(dict(row)) for row in res.fetchall()]
            res.close()
            return {'success': True, 'results':result}
        else:
            return {'success': False, 'message': "El atributo de convocatoria y la cuenta en sesión son obligatorios" },400
        
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        return responseError(e, 500)
    finally:
        db.session.close()
        
def get_contrat_types_simple():
    try:
        query = """ SELECT id, description FROM `mod-payroll-core-dev`.contract_types where active = 1"""
        query+=';'
        res = db.session.execute(query)
        result = [(dict(row)) for row in res.fetchall()]
        res.close()
        return {'success': True, 'results':result}
    
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        return responseError(e, 500)
    finally:
        db.session.close()

def get_job_position_simple(data):
    account = data.get("entity_account")
    
    try:
        if account:
            query = ' SELECT id, job_title, job_description FROM `mod-payroll-core-dev`.job_positions where active = 1 and entity_account =  ' + str(account) + ''
            query+=';'
            res = db.session.execute(query)
            result = [(dict(row)) for row in res.fetchall()]
            res.close()
            return {'success': True, 'results':result}
        else:
            return {'success': False, 'message': "El atributo cuenta en sesión es obligatorio" },400
    
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        return responseError(e, 500)
    finally:
        db.session.close()

def get_announcements(data):
    account = data.get("entity_account")
    
    try:
        if account:
            query = """ SELECT id, title
                        FROM announcements
                        WHERE status = 2 and entity_account = {}
                    """.format(account)
            query+=';'
            res = db.session.execute(query)
            result = [(dict(row)) for row in res.fetchall()]
            res.close()
            return {'success': True, 'results':result}
        else:
            return {'success': False, 'message': "El atributo cuenta en sesión es obligatorio" },400 
    
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        return responseError(e, 500)
    finally:
        db.session.close()

def get_vacancy_types():
    try:
        query = """ SELECT id, description FROM `mod-payroll-core-dev`.vacancy_types where active = 1"""
        query+=';'
        sql = db.engine.execute(query)
        return jsonify([dict(row) for row in sql])
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        return responseError(e, 500)
    finally:
        db.session.close()
        
def update_job_position(data):
    try :
        if data.get('id') is None:
                return {'success': False, 'message': "Se requiere id"}, 400
        else :    
            params = {}
            x = 2
            query = """ UPDATE `mod-payroll-core-dev`.job_positions SET """
            
            if data.get('job_title') is not None:
                query += " job_title = :job_title "
                params['job_title'] = data.get('job_title')
                if len(data) > 1 and x<len(data):
                    query += ","
                    x +=1
            
            if data.get('job_description') is not None:
                query += " job_description = :job_description "
                params['job_description'] = data.get('job_description')
                if len(data) > 1 and x<len(data):
                    query += ","
                    x +=1

            if data.get('area_id') is not None:
                query += " area_id = :area_id "
                params['area_id'] = data.get('area_id')
                if len(data) > 1 and x<len(data):
                    query += ","
                    x +=1

            if data.get('area_name') is not None:
                query += " area_name = :area_name "
                params['area_name'] = data.get('area_name')
                if len(data) > 1 and x<len(data):
                    query += ","
                    x +=1

            if data.get('area_leader') is not None:
                query += " area_leader = :area_leader "
                params['area_leader'] = data.get('area_leader')
                if len(data) > 1 and x<len(data):
                    query += ","
                    x +=1

            if data.get('supervisor_pos') is not None:
                query += " supervisor_pos = :supervisor_pos "
                params['supervisor_pos'] = data.get('supervisor_pos')
                if len(data) > 1 and x<len(data):
                    query += ","
                    x +=1

            if data.get('extra_hours') is not None:
                query += " extra_hours = :extra_hours "
                params['extra_hours'] = data.get('extra_hours')
                if len(data) > 1 and x<len(data):
                    query += ","
                    x +=1
                    
            if data.get('work_time') is not None:
                query += " work_time = :work_time "
                params['work_time'] = data.get('work_time')
                if len(data) > 1 and x<len(data):
                    query += ","
                    x +=1
                    
            if data.get('entity_account') is not None:
                query += " entity_account = :entity_account "
                params['entity_account'] = data.get('entity_account')
                if len(data) > 1 and x<len(data):
                    query += ","
                    x +=1
                    
            if data.get('active') is not None:
                query += " active = :active "
                params['active'] = data.get('active')
                if len(data) > 1 and x<len(data):
                    query += ","
                    x +=1

            query += " WHERE id = :id"
            params['id'] = data.get('id')
            result = db.session.execute(query, params)
            db.session.commit()
            result.close()
            return {'success': True, 'message': "Cargo actualizado exitosamente"},200
    except Exception as e:
        return responseError(e,400, "Error al momento de actualizar el cargo")
    
def create_job_positions(data):
    try :
        query = """INSERT INTO `mod-payroll-core-dev`.job_positions 
        (job_title,job_description,area_id ,area_name,area_leader,supervisor_pos, extra_hours, work_time, entity_account , active, created_by, created_at)
        VALUE(:job_title,:job_description,:area_id ,:area_name,:area_leader,:supervisor_pos, :extra_hours, :work_time, :entity_account , :active, :created_by, :created_at)"""
        params = {
            "job_title" : data.get('job_title'),
            "job_description" : data.get('job_description'),
            "area_id" : data.get('area_id'),
            "area_name" : data.get('area_name'),
            "area_leader" : data.get('area_leader'),
            "supervisor_pos" : data.get('supervisor_pos'),
            "extra_hours" : data.get('extra_hours'),
            "work_time" : data.get('work_time'),
            "entity_account" : data.get('entity_account'),
            "active" : True,
            "created_by" : data.get('created_by'),
            "created_at" : datetime.datetime.utcnow(),
        }
        result = db.session.execute(query, params)

        db.session.flush()
        just_inserted_id= result.lastrowid

        query_2 = """INSERT INTO `mod-payroll-core-dev`.rel_jobpos_compdoc
        (comp_doc, job_position)
        VALUE
        """
        count = 0
        if isinstance(data.get("comp_doc_ids"), list):
            for comp_doc in data.get("comp_doc_ids"):
                if count !=0:
                    query_2 +=", "
                query_2 += "("+str(comp_doc)+", "+str(just_inserted_id)+")"
                count += 1

            if len(data.get("comp_doc_ids")) >0:
                result_2 = db.session.execute(query_2)


        db.session.commit()
        result.close()
        lastInsert = result.lastrowid
        return {'success': True, 'message': "Cargo creado exitosamente", 'id_inserted': lastInsert, "ammount of comp_doc": count},200
    except Exception as e:
        return responseError(e,400, "Error al momento de crear el cargo")

def get_work_times(data):
    params = {}
    query = """ SELECT * from `mod-payroll-core-dev`.work_times WHERE 1=1 """
    
    if data.get("id", type = int):
        query +=" AND id = :id "
        params['id'] = data.get("id", type = int)
        
    if data.get("description", type = str):
        query +=" AND description LIKE ('%%"+ data.get("description", type = str)+"%%')"
        params['description'] = data.get("description", type = str)

    if data.get("active", type = int):
        query +=" AND active = :active "
        params['active'] = data.get("active", type = int)

    result = db.session.execute(query, params)
    finalresult = []
    for row in result.fetchall():
        rowdict = dict(row)
        finalresult.append(rowdict)     
    result.close()
    return {'success': True, 'results':finalresult },200

def get_adjustments(data):
    try:
        query =  """SELECT ad.*, at.description as type_name , ar.description as recipient_name,
        st.value as value_status, st.color_id , sc.background, sc.fontcolor        
        FROM `mod-payroll-core-dev`.adjustments ad
        join `mod-payroll-core-dev`.adjustment_type at ON at.id = ad.type
        join `mod-payroll-core-dev`.adjustment_recipient ar ON ar.id = ad.recipient_type
        join `mod-payroll-core-dev`.status st ON st.name = ad.status and st.prefix = 'adjustment'
        join `mod-payroll-core-dev`.status_color sc ON sc.id = st.color_id 
        WHERE 1=1 """
        querycount = """SELECT COUNT(1) FROM `mod-payroll-core-dev`.adjustments ad
        join `mod-payroll-core-dev`.adjustment_type at ON at.id = ad.type
        join `mod-payroll-core-dev`.adjustment_recipient ar ON ar.id = ad.recipient_type 
        join `mod-payroll-core-dev`.status st ON st.name = ad.status and st.prefix = 'adjustment'
        join `mod-payroll-core-dev`.status_color sc ON sc.id = st.color_id 
        WHERE 1=1 """
        params = {}
        if data.get("id") is not None:
            query +=" AND ad.id = :id "
            querycount += " AND ad.id = :id "
            params['id'] = data.get("id", type = int)
    
        if data.get("type") is not None:
            query +=" AND ad.type = :type "
            querycount += " AND ad.type = :type "
            params['type'] = data.get("type", type = int)

        if data.get("description", type = str):
            query +=" AND ad.description LIKE ('%%"+data.get("description", type=str)+"%%')"
            querycount +=" AND ad.description LIKE ('%%"+data.get("description", type=str)+"%%')"
            params['description'] =  data.get("description", type = str)

        if data.get("recipient_type") is not None:
            query +=" AND ad.recipient_type = :recipient_type "
            querycount += " AND ad.recipient_type = :recipient_type "
            params['recipient_type'] = data.get("recipient_type", type = int)

        if data.get("recipient_id") is not None:
            query +=" AND ad.recipient_id = :recipient_id "
            querycount += " AND ad.recipient_id = :recipient_id "
            params['recipient_id'] = data.get("recipient_id", type = int)
    
        if data.get("niifAccount_id") is not None:
            query +=" AND ad.niifAccount_id = :niifAccount_id "
            querycount += " AND ad.niifAccount_id = :niifAccount_id "
            params['niifAccount_id'] = data.get("niifAccount_id", type = int)

        if data.get("niifAccount_name", type = str):
            query +=" AND ad.niifAccount_name LIKE ('%%"+data.get("niifAccount_name", type=str)+"%%')"
            querycount +=" AND ad.niifAccount_name LIKE ('%%"+data.get("niifAccount_name", type=str)+"%%')"
            params['niifAccount_name'] =  data.get("niifAccount_name", type = str)    

        if data.get("affects_socialSecurity") is not None:
            query +=" AND ad.affects_socialSecurity = :affects_socialSecurity "
            querycount += " AND ad.affects_socialSecurity = :affects_socialSecurity "
            params['affects_socialSecurity'] = data.get("affects_socialSecurity", type = str)

        if data.get("affects_socialBenefit") is not None:
            query +=" AND ad.affects_socialBenefit = :affects_socialBenefit "
            querycount += " AND ad.affects_socialBenefit = :affects_socialBenefit "
            params['affects_socialBenefit'] = data.get("affects_socialBenefit", type = str)

        if data.get("entity_account") is not None:
            query +=" AND ad.entity_account = :entity_account "
            querycount += " AND ad.entity_account = :entity_account "
            params['entity_account'] = data.get("entity_account", type = int)

        if data.get("status") is not None:
            query +=" AND ad.status = :status "
            querycount += " AND ad.status = :status "
            params['status'] = data.get("status", type = str)
    
        querycount = db.session.execute(querycount, params).scalar()
        
        if data.get("page", type = int) is not None and data.get("perpage", type = int) is not None:
            offset = data.get("perpage", type = int) * (data.get("page", type = int) - 1)
            query +=" LIMIT :perpage OFFSET :offset "
            params['offset'] = offset
            params['perpage'] = data.get("perpage", type = int)
        
        result = db.session.execute(query, params)
        finalresult = []
        for row in result.fetchall():
            dictrow = dict(row)
            myrow = {}
            for i in dictrow:
                myrow[i]=json_serial(dictrow[i])
            finalresult.append(myrow)    
        return {'success': True, 'results' : finalresult,  'row_total' :querycount }, 200
    except Exception as e:
        return responseError(str(e), 400, "Error")
    finally :
        result.close()


def update_adjustments(data):
    try:
        if data.get('id') is None:
                return {'success': False, 'message': "Se requiere id"}, 400
        else :
            recipient_id = data.get('recipient_id')
            if (data.get('id_third_party') is None and data.get('type') == 3 ):
                third_party_data = {
                    'nit' : data.get('nit_third_party'),
                    }
                res_third_party = get_third_parties(third_party_data)
                if (res_third_party[0].get('lenght') > 0):
                    return responseError("El nit digitado ya existe", 400)
                else:
                    third_party_data['description'] =  data.get('nit_description')
                    res_third_party = create_third_parties(third_party_data)
                    if res_third_party[1] == 400:
                        return res_third_party
                    else:
                        recipient_id = res_third_party[0].get('inserted_id')         
                        
            params = {}
            x = 2
            query = """ UPDATE `mod-payroll-core-dev`.adjustments ad SET """

            if data.get('type') is not None:
                query += " ad.type = :type "
                params['type'] = data.get('type')
                if len(data) > 1 and x<len(data):
                    query += ","
                    x +=1

            if data.get('description') is not None:
                query += " ad.description = :description "
                params['description'] = data.get('description')
                if len(data) > 1 and x<len(data):
                    query += ","
                    x +=1

            if data.get('recipient_type') is not None:
                query += " ad.recipient_type = :recipient_type "
                params['recipient_type'] = data.get('recipient_type')
                if len(data) > 1 and x<len(data):
                    query += ","
                    x +=1

            if recipient_id is not None:
                query += " ad.recipient_id = :recipient_id "
                params['recipient_id'] = recipient_id
                if len(data) > 1 and x<len(data):
                    query += ","
                    x +=1
                    
            if data.get('niifAccount_id') is not None:
                query += " ad.niifAccount_id = :niifAccount_id "
                params['niifAccount_id'] = data.get('niifAccount_id')
                if len(data) > 1 and x<len(data):
                    query += ","
                    x +=1

            if data.get('niifAccount_name') is not None:
                query += " ad.niifAccount_name = :niifAccount_name "
                params['niifAccount_name'] = data.get('niifAccount_name')
                if len(data) > 1 and x<len(data):
                    query += ","
                    x +=1

            if data.get('affects_socialSecurity') is not None:
                query += " ad.affects_socialSecurity = :affects_socialSecurity "
                params['affects_socialSecurity'] = data.get('affects_socialSecurity')
                if len(data) > 1 and x<len(data):
                    query += ","
                    x +=1
            if data.get('affects_socialBenefit') is not None:
                query += " ad.affects_socialBenefit = :affects_socialBenefit "
                params['affects_socialBenefit'] = data.get('affects_socialBenefit')
                if len(data) > 1 and x<len(data):
                    query += ","
                    x +=1
            if data.get('entity_account') is not None:
                query += " ad.entity_account = :entity_account "
                params['entity_account'] = data.get('entity_account')
                if len(data) > 1 and x<len(data):
                    query += ","
                    x +=1

            if data.get('status') is not None:
                query += " ad.status = :status "
                params['status'] = data.get('status')
            
            query += " WHERE ad.id = :id"
            params['id'] = data.get('id')
            result = db.session.execute(query, params)
            db.session.commit()
            result.close()
        return {'success': True, 'message' : "Registro devengado o deducción actualizado correctamente" }, 200
    except Exception as e:
        return responseError(str(e), 400, "Error")
    finally:
        db.session.close()

def create_adjustments(data):
    try:
        query = """ INSERT INTO `mod-payroll-core-dev`.adjustments 
        (`type`, `description`, `recipient_type`, `recipient_id`, `niifAccount_id`
        , `niifAccount_name`, `affects_socialSecurity`, `affects_socialBenefit`, `entity_account`
        , `status`)
        VALUES (  :type,  :description,  :recipient_type,  :recipient_id,  :niifAccount_id
        ,  :niifAccount_name,  :affects_socialSecurity,  :affects_socialBenefit,  :entity_account
        ,  :status)
        
        """
        params = {
            "type" : data.get('type'),
            "description" : data.get('description'),
            "recipient_type" : data.get('recipient_type'),
            "recipient_id" : data.get('recipient_id'),
            "niifAccount_id" : data.get('niifAccount_id'),
            "niifAccount_name" : data.get('niifAccount_name'),
            "affects_socialSecurity" : data.get('affects_socialSecurity'),
            "affects_socialBenefit" : data.get('affects_socialBenefit'),
            "entity_account" : data.get('entity_account'),
            "status" : data.get('status')
        }
        result = db.session.execute(query, params)
        db.session.commit()
        lastInsert = result.lastrowid
        return {"success":True, "message":"Registro devengado o deducción creado correctamente", "inserted_id":lastInsert}, 200
    except Exception as e:
        return responseError(str(e), 400, "Error")
    finally:
        db.session.close()
        
def get_third_parties(data):
    try: 
        query = """SELECT * FROM `mod-payroll-core-dev`.third_parties WHERE 1=1 """
        params = {}
        if data.get("id") is not None:
            query +=" AND ad.id = :id "
            params['id'] = data.get("id", type = int)
            
        
        if data.get("description") is not None:
            query +=" AND description = :description "
            params['description'] = data.get("description", type = str)
            
        if data.get("nit") is not None:
            nit =  data.get("nit")
            query +=" AND nit = :nit "
            params['nit'] =nit
        result = db.session.execute(query, params)
        finalresult = []
        for row in result.fetchall():
            dictrow = dict(row)
            myrow = {}
            for i in dictrow:
                myrow[i]=json_serial(dictrow[i])
            finalresult.append(myrow)    
        return {'success': True, 'results' : finalresult , 'lenght' : len(finalresult) }, 200
    except Exception as e:
        return responseError(str(e), 400, "Error")
    finally :
        db.session.close()

def create_third_parties(data):
    try:
        query = """ INSERT INTO `mod-payroll-core-dev`.third_parties (description, nit)
        VALUES (:description, :nit)
        """
        params = {
            "description" : data.get('description'),
            "nit" : data.get('nit'),
        }
        result = db.session.execute(query, params)
        db.session.commit()
        lastInsert = result.lastrowid
        return {"success":True, "message":"Tercero creado correctamente", "inserted_id":lastInsert}, 200
    except Exception as e:
        return responseError(str(e), 400, "Error")
    finally:
        db.session.close()

def create_Deductions_Accruals(data):
    try :
        id_third_party = 0
        create_adjustments_data = {
                "type" : data.get('type'),  
                "description" : data.get('description'),
                "recipient_type" : data.get('recipient_type'),
                "recipient_id" : id_third_party ,
                "niifAccount_id" : data.get('niifAccount_id'),
                "niifAccount_name" : data.get('niifAccount_name'),
                "affects_socialSecurity" : data.get('affects_socialSecurity'),
                "affects_socialBenefit" : data.get('affects_socialBenefit'),
                "entity_account" : data.get('entity_account'),
                "status" : "enabled",
            }
        if data.get('type') == 1 and data.get('recipient_type')==3 : #si tiene tercero, es decir es una deducción
            if (data.get('recipient_id') is None or data.get('recipient_id') == 0 or data.get('recipient_id') == "" ): #si están creando un nuevo tercero 
                third_party_data = {
                    'nit' : data.get('nit_third_party'),
                    }
                res_third_party = get_third_parties(third_party_data) #se busca el tercero a ver si ya está creado
                
                if (res_third_party[0].get('lenght') > 0):
                    return responseError("El nit digitado ya existe", 400)
                else:
                    third_party_data['description'] =  data.get('nit_description')
                    res_third_party = create_third_parties(third_party_data)
                    if res_third_party[1] == 400: #si la creación del tercero salió mal
                        return res_third_party 
                    else:
                        id_third_party = res_third_party[0].get('inserted_id')
                        create_adjustments_data['recipient_id'] = id_third_party
                return create_adjustments(create_adjustments_data)
            else:
                create_adjustments_data['recipient_id'] = data.get('recipient_id')
                return create_adjustments(create_adjustments_data)
        else:
            if (data.get('recipient_id') is None or data.get('recipient_id') == ""):
                create_adjustments_data['recipient_id'] = None
            else:
                create_adjustments_data['recipient_id'] = data.get('recipient_id')
            return create_adjustments(create_adjustments_data)
    except Exception as e:
        return responseError(str(e), 400, "Error al crear")
    finally:
        db.session.close()
    
def list_candidates(data, type, jobPosition):
    account = data.get("entity_account")
    status = data.get("status")
    
    try:
        if account and status:
            if type == "2":
                query = ' SELECT DISTINCT id ,full_name, doc_type, doc_number FROM `mod-payroll-core-dev`.candidates where entity_account = ' + str(account) + ' and status = '+ '"' + str(status) +'";'
            
            elif type == "1":
                if jobPosition["success"]:
                    jb_p = jobPosition["results"][0]["id"]
                    query = """
                        SELECT DISTINCT a.id, full_name, doc_type, doc_number
                        FROM candidates AS a
                        INNER JOIN applicants AS b ON b.candidate = a.id
                        INNER JOIN announcements AS c ON c.id = b.announcement
                        LEFT JOIN contracts AS d ON d.job_position = c.job_position AND d.candidate = a.id
                        WHERE c.job_position = {0} and a.entity_account = {1}
                    """.format(jb_p, account)
                else:
                    return {'success': False, 'message': "El atributo Job positions es obligatorio" },400 
        else:
            return {'success': False, 'message': "El atributo cuenta en sesión y estado del candidato es obligatorio" },400
        
        res = db.session.execute(query)
        result = [(dict(row)) for row in res.fetchall()]
        res.close()
        return {'success': True, 'results':result}
    
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        return {'success': False, 'message': e },400
    
    finally:
        db.session.close() 

def get_contrat_types():
    try:
        query ="""
            SELECT a.id, a.description AS description ,SUM(IF(b.`status` = 2,1,0)) AS collaborators, a.active AS status
            FROM contract_types AS a
            LEFT JOIN contracts AS b ON b.contract_type = a.id
            LEFT JOIN candidates AS c ON b.candidate = c.id
            GROUP BY a.id;
            """
        res = db.session.execute(query)
        content = []
        for row in res.fetchall():
            query_benefits = """
                select bt.id as ben_id, bt.description as description, bt.active
                from benefit_types bt
                INNER JOIN rel_contract_benefit rl on rl.benefit_type = bt.id
                INNER JOIN contract_types ct on ct.id = rl.contract_type
                WHERE 1=1 AND ct.id = {}; 
                """.format(row.id)
            res_benefits = db.session.execute(query_benefits)
            item = {
                'id':row.id,
                'description':row.description,
                'status':row.status,
                'collaborators':int(row.collaborators),
                'benefits':[dict({'id':r.ben_id, 'name':r.description, 'checked':bool(r.active)}) for r in res_benefits.fetchall()],
            }
            content.append(item)
        return {'success':True, "results":content}
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        return responseError(e, 500)
    finally:
        db.session.close()


def update_contract_type(data):
    id_contract = data.get('id')
    status = data.get('active')
    description = data.get('description')
    template_body = data.get('template_body')
    statement = "UPDATE contract_types "
    where_clause = " WHERE id = :id_contract;"
    if id_contract is None:
        return responseError("id",400, "Debe seleccionar el tipo de contrato")
    if status is not None:
        if status == True:
            statement += "SET active = 1"
        else:
            statement += "SET active = 0"
    if description is not None:
        statement += "SET description = :description"
    if template_body is not None:
        statement += "SET template_body = :template_body"
    if status is None and description is None and template_body is None and statement:
        return responseError("empty data", 400, "Debe enviar el campo que desea editar")
    statement += where_clause    
    try:
        params = {
            'id_contract': id_contract,
            'active':status,
            'description':description,
            'template_body':template_body
            }
        db.session.execute(statement, params)
        db.session.commit()
        objEdited = db.session.execute("SELECT description from contract_types WHERE id = "+str(id_contract))
        resObj = objEdited.fetchone()
        resObj = resObj[0]
        message = "El contrato "+str(resObj)+" ha sido actualizado"
        return responseSuccess(id_contract, message, 1)
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        db.session.rollback()
        return responseError(e, 500, "Ocurrió un error al actualizar el tipo de contrato")
    finally:
        db.session.close()
        
def list_status(data):
    prefix = data.get("prefix")
    try:
        #query = 'SELECT id, name AS key_, value AS value_ FROM `mod-payroll-core-dev`.status '
        query = 'SELECT * FROM `mod-payroll-core-dev`.status '
        if prefix:
            query += ' where prefix = ' + '"' + str(prefix) + '" '

        sql = db.engine.execute(query)
            
        return jsonify({'results': [dict(row) for row in sql]})
        
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        return responseError(e, 400)
    finally:
        db.session.close()

def list_CandidateStatus(data):
    entity_account = data.get("entity_account")
    status = data.get("status")
    
    try:
        query = 'select id, full_name, doc_number  FROM `mod-payroll-core-dev`.candidates where entity_account = "'+str(entity_account)+'" and status = "'+str(status)+'";'
        result = db.session.execute(query)
        return jsonify({'results': [dict(row) for row in result]})
    
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        return {'success': False, 'message': e },400
    finally:
        db.session.close()
        

def validateDuplicity_docCandidate(number, account):
    query = 'SELECT * FROM `mod-payroll-core-dev`.candidates where doc_number = "'+str(number)+'" and entity_account = "'+str(account)+'" ;'
    res = db.session.execute(query)
    cont_row = res.rowcount
    
    if cont_row != 0:
        return {"success": False, "message": "Ya existe un candidato registrado con este número de documento"}, 400
    else:
        return {"success": True, "message": "Documento no registrado"}, 200


def updateCandidateDocuments(candidate, documents):
    try:
        for doc in documents:
            prefix = doc.get('support_type')
            prefix = "cv" if prefix == "s3_cv" else prefix
            url = doc.get('url')
            consult_cv = """
                SELECT * FROM complementary_docs a
                LEFT JOIN rel_candidate_compdoc b on a.id = b.comp_doc AND candidate = :candidate
                WHERE prefix = :prefix;
            """
            param_cv = {'candidate': candidate, 'prefix':prefix}
            verify_cv = db.session.execute(consult_cv, param_cv)

            verify_cv = verify_cv.fetchone()                                  
            if verify_cv is not None:
                if verify_cv.candidate is None and verify_cv.prefix is not None:
                    # update curriculum vitae                     
                    query1 = """                            
                        INSERT INTO rel_candidate_compdoc
                        VALUES (
                            null,
                            :candidate,
                            (SELECT id from complementary_docs WHERE prefix = :prefix ),
                            :s3_location
                        )
                    """

                    params = {
                        "candidate": candidate,
                        "s3_location": url,
                        "prefix":verify_cv.prefix
                    }
                    result1 = db.session.execute(query1, params)
                    result1.close()
                elif verify_cv.candidate is not None and verify_cv.prefix is not None:
                    update_cv = """ 
                        UPDATE rel_candidate_compdoc a
                        INNER JOIN complementary_docs b on b.id = a.comp_doc
                        SET s3_location = :url
                        WHERE prefix = :prefix AND candidate = :candidate;
                    """                    
                    params_update_cv = {'url':doc.get('url'), 'candidate':candidate, 'prefix':prefix}
                    db.session.execute(update_cv, params_update_cv)
                    db.session.commit()
            else:
                verifyDoc = "SELECT id from complementary_docs WHERE prefix = '"+str(prefix)+"'"
                resVerDoc = db.session.execute(verifyDoc)
                if resVerDoc.fetchone() is not None:
                    query1 = """                            
                            INSERT INTO rel_candidate_compdoc
                            VALUES (
                                null,
                                :candidate,
                                (SELECT id from complementary_docs WHERE prefix = :prefix ),
                                :s3_location
                            )
                        """

                    params = {
                        "candidate": candidate,
                        "s3_location": url,
                        "prefix":prefix
                    }
                    result1 = db.session.execute(query1, params)
                    result1.close()
                    db.session.commit()
        return responseSuccess("OK", "OK", 1)


    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        return responseError(str(e), 500, "Ocurrió un error al guardar los documentos del candidato")
    finally:
        db.session.close()


def updateCandidateEducation(candidate, education):
    try:
        del_stm = "DELETE FROM candidate_educlevel where candidate = :candidate"
        db.session.execute(del_stm, {'candidate':candidate})
        db.session.commit()
        stm = """
            INSERT INTO candidate_educlevel values
        """
        paramsArray = []
        for item in education:
            paramsArray.append(" (null, "+ str(candidate) +", '"+ str(item.get('education_level'))+"' , '"+ item.get('graduation_date')+ "' , '"+ item.get('institution_name')+"' , '"+ item.get('description') +"' )")
        stm += ",".join(paramsArray)
        res = db.session.execute(stm)
        db.session.commit()
        return responseSuccess(res.lastrowid, "Se guardó correctamente la información del candidato (Educación)", 1)
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        return responseError(str(e), 500, "Ocurrió un error al guardar la información del candidato (Educación)")
    finally:
        db.session.close()

def updateCandidateWorkExp(candidate, workExperience):
    try:
        del_stm = "DELETE FROM candidate_workexp where candidate = :candidate"
        db.session.execute(del_stm, {'candidate':candidate})
        db.session.commit()
        stm = """
            INSERT INTO candidate_workexp values
        """
        paramsArray = []
        for item in workExperience:
            paramsArray.append(" (null, "+ str(candidate) +", '"+ str(item.get('company_name'))+"' , '"+ item.get('start_date')+ "' , '"+ item.get('end_date')+"' , '"+ item.get('description') +"' )")
        stm += ",".join(paramsArray)
        res = db.session.execute(stm)
        db.session.commit()
        return responseSuccess(res.lastrowid, "Se guardó correctamente la información del candidato (Experiencia laboral)", 1)
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        return responseError(str(e), 500, "Ocurrió un error al guardar la información del candidato (Experiencia laboral)")
    finally:
        db.session.close()

def modify_candidate(data):
    candidate = None    
    s3_photo = None
    id_contract = data.get('id_contract')
    contract_status = data.get('contract_status')
    full_name = data.get('full_name')
    doc_type = data.get('doc_type')
    doc_number = data.get('doc_number')
    doc_issued = data.get('doc_issued')
    gender = data.get('gender')
    birthdate = data.get('birthdate')
    email = data.get('email')
    mobile = data.get('mobile')
    home_address = data.get('home_address')
    province_id = data.get('province_id')
    province_name = data.get('province_name')
    citizenship = data.get('citizenship')
    city_id = data.get('city_id')
    city_name = data.get('city_name')
    occupation = data.get('occupation')
    marital_status = data.get('marital_status')
    children_qty = data.get('children_qty')
    emergency_contact = data.get('emergency_contact')
    emercont_relationship = data.get('emercont_relationship')
    emercont_mobile = data.get('emercont_mobile')
    blood_type = data.get('blood_type')
    rh_factor = data.get('rh_factor')
    heigth_cm = data.get('heigth_cm')
    weight_kg = data.get('weight_kg')
    shirt_size = data.get('shirt_size')
    pant_size = data.get('pant_size')
    shoe_size = data.get('shoe_size')
    healthcare_entity = data.get('healthcare_entity')
    pension_fund_entity = data.get('pension_fund_entity')
    occupational_risk_entity = data.get('occupational_risk_entity')
    bank_account_nbr = data.get('bank_account_nbr')
    bank_account_type = data.get('bank_account_type')
    bankingEntity = data.get('bankingEntity')
    description = data.get('description')        
    status = data.get('status')
    endowmentGroups = data.get('endowmentGroups')
    education = data.get('education')
    workExperience = data.get('workExperience')
    percentage = data.get('percentage')
    # files
    documents = data.get('document')
    s3_bank_certificate_doc = None
    s3_data_verification = None
    s3_clinic_history = None
    
    if documents is not None and len(documents) > 0:
        for d in data["document"]:
            candidate = d["id"]
            if d["support_type"] == "s3_photo":
                s3_photo = d["url"] if d["url"] != "" else None

            if d["support_type"] == "s3_bank_certificate_doc":
                s3_bank_certificate_doc = d["url"] if d["url"] != "" else None

            if d["support_type"] == "s3_data_verification":
                s3_data_verification = d["url"] if d["url"] != "" else None

            if d["support_type"] == "s3_clinic_history":
                s3_clinic_history = d["url"] if d["url"] != "" else None
    try:
        if candidate is None and data.get('id_candidate'):
            candidate = data.get('id_candidate')
        if candidate is not None:                  

            paramArray = []
            if s3_photo is not None:
                paramArray.append('`s3_photo` = "'+str(s3_photo)+'"') 
            if full_name is not None:
                paramArray.append("full_name = '"+str(full_name)+"'") 
            if doc_type is not None:
                paramArray.append("doc_type = '"+str(doc_type)+"'") 
            if doc_number is not None:
                paramArray.append("doc_number = '"+str(doc_number)+"'") 
            if doc_issued is not None:
                paramArray.append("doc_issued = '"+str(doc_issued)+"'") 
            if gender is not None:
                paramArray.append("gender = '"+str(gender)+"'") 
            if birthdate is not None:
                paramArray.append("birthdate = '"+str(birthdate)+"'") 
            if email is not None:
                paramArray.append("email = '"+str(email)+"'") 
            if mobile is not None:
                paramArray.append("mobile = '"+str(mobile)+"'") 
            if home_address is not None:
                paramArray.append("home_address = '"+str(home_address)+"'") 
            if province_id is not None:
                paramArray.append("province_id = '"+str(province_id)+"'") 
            if province_name is not None:
                paramArray.append("province_name = '"+str(province_name)+"'") 
            if citizenship is not None:
                paramArray.append("citizenship = '"+str(citizenship)+"'") 
            if city_id is not None:
                paramArray.append("city_id = '"+str(city_id)+"'") 
            if city_name is not None:
                paramArray.append("city_name = '"+str(city_name)+"'") 
            if occupation is not None:
                paramArray.append("occupation = '"+str(occupation)+"'") 
            if marital_status is not None:
                paramArray.append("marital_status = '"+str(marital_status)+"'") 
            if children_qty is not None:
                paramArray.append("children_qty = '"+str(children_qty)+"'") 
            if emergency_contact is not None:
                paramArray.append("emergency_contact = '"+str(emergency_contact)+"'") 
            if emercont_relationship is not None:
                paramArray.append("emercont_relationship = '"+str(emercont_relationship)+"'") 
            if emercont_mobile is not None:
                paramArray.append("emercont_mobile = '"+str(emercont_mobile)+"'") 
            if blood_type is not None:
                paramArray.append("blood_type = '"+str(blood_type)+"'") 
            if rh_factor is not None:
                paramArray.append("rh_factor = '"+str(rh_factor)+"'") 
            if heigth_cm is not None:
                paramArray.append("heigth_cm = '"+str(heigth_cm)+"'") 
            if weight_kg is not None:
                paramArray.append("weight_kg = '"+str(weight_kg)+"'") 
            if shirt_size is not None:
                paramArray.append("shirt_size = '"+str(shirt_size)+"'") 
            if pant_size is not None:
                paramArray.append("pant_size = '"+str(pant_size)+"'") 
            if shoe_size is not None:
                paramArray.append("shoe_size = '"+str(shoe_size)+"'") 
            if healthcare_entity is not None:
                paramArray.append("healthcare_entity = '"+str(healthcare_entity)+"'") 
            if pension_fund_entity is not None:
                paramArray.append("pension_fund_entity = '"+str(pension_fund_entity)+"'") 
            if occupational_risk_entity is not None:
                paramArray.append("occupational_risk_entity = '"+str(occupational_risk_entity)+"'") 
            if bank_account_nbr is not None:
                paramArray.append("bank_account_nbr = '"+str(bank_account_nbr)+"'") 
            if bank_account_type is not None:
                paramArray.append("bank_account_type = '"+str(bank_account_type)+"'") 
            if bankingEntity is not None:
                paramArray.append("bankingEntity = '"+str(bankingEntity)+"'") 
            if description is not None:
                paramArray.append("description = '"+str(description)+"'") 
            if status is not None:
                paramArray.append("status = '"+str(status)+"'") 
            if s3_bank_certificate_doc is not None:
                paramArray.append("s3_bank_certificate_doc = '"+str(s3_bank_certificate_doc+"'"))
            if s3_data_verification is not None:
                paramArray.append("s3_data_verification = '"+str(s3_data_verification+"'"))
            if s3_clinic_history is not None:
                paramArray.append("s3_clinic_history = '"+str(s3_clinic_history+"'"))
            if percentage is not None:
                paramArray.append("percentage = '"+str(percentage)+"'")
            if len(paramArray) > 0:
                query = 'UPDATE candidates SET '
                query += ','.join(paramArray)
                query += " WHERE `id` =" +str(candidate)+';'
                db.session.execute(query)
            if documents is not None and len(documents) >0:
                res_documents = updateCandidateDocuments(candidate, documents).json                
                if res_documents.get('success') == False:
                    db.session.rollback()
                    return responseError(res_documents['error'], 500, res_documents['message'])
                

            if endowmentGroups is not None and len(endowmentGroups) > 0:
                res_groups = updateEndowmentGroups(endowmentGroups, candidate, id_contract).json
                if res_groups.get('success') == False:
                    db.session.rollback()
                    return responseError(res_groups['error'], 500, "Ocurrió un error al guardar los grupos dotacionales")

            if education is not None and len(education) > 0:
                res_education = updateCandidateEducation(candidate, education).json
                if res_education.get('success') == False:
                    db.session.rollback()
                    return responseError(res_education['error'], 500, res_education['message'])

            if workExperience is not None and len(workExperience) > 0:
                res_workexp = updateCandidateWorkExp(candidate, workExperience).json
                if res_workexp.get('success') == False:
                    db.session.rollback()
                    return responseError(res_workexp['error'], 500, res_workexp['message'])

            if contract_status is not None:
                update_contract = """
                    UPDATE contracts set `status` = :contract_status WHERE id = :id_contract
                """
                paramUpdate = {'contract_status': contract_status, 'id_contract':id_contract}
                db.session.execute(update_contract, paramUpdate)
            db.session.commit()
            db.session.close()
            return {'success': True, 'message': "Candidato actualizado exitosamente", 'results': candidate},200
        else:
            return responseError("id_candidate", 400, "Debe enviar el id del candidato")    
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        db.session.rollback()
        return responseError(str(e), 500, "Ocurrió un error al actualizar el candidato")
    finally:
        db.session.close()
            

def create_candidate(data):
    entity_account = data.get("entity_account")
    full_name = data.get("full_name")
    doc_type = data.get("doc_type")
    doc_number = data.get("doc_number")
    created_by = data.get("created_by")
    birthdate = data.get("birthdate")
    doc_issued = data.get("doc_issued")
    email = data.get("email")
    valid = bool(re.search(r"(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])", email))
    occupation = data.get("occupation")
    province_id = data.get("province_id")
    province_name = data.get("province_name")
    city_id = data.get("city_id")
    city_name = data.get("city_name")
    home_address = data.get("home_address")
    description = data.get("description")
    mobile = data.get("mobile")
    valid_doc = validateDuplicity_docCandidate(doc_number, entity_account)
    
    try:
        
        if not entity_account:
            return {"success": False, "message": "La cuenta es un campo obligatorio"}, 400

        if not full_name:
            return {"success": False, "message": "El nombre completo es un campo obligatorio"}, 400
        
        if not doc_type:
            return {"success": False, "message": "El tipo de documento es un campo obligatorio"}, 400
        
        if not doc_number:
            return {"success": False, "message": "El número de documento es un campo obligatorio"}, 400
        
        if not valid_doc[0]["success"]:
            return {"success": False, "message": valid_doc[0]["message"]}, 400
        
        if not birthdate:
            return {"success": False, "message": "La fecha de nacimiento del candidato es un campo obligatorio"}, 400
        
        if not doc_issued:
            return {"success": False, "message": "El lugar de expedición del documento es un campo obligatorio"}, 400
        
        # Email validations!! ################################
        
        if not email:
            return {"success": False, "message": "El correo electrónico es un campo obligatorio"}, 400
        
        if not valid:
            return {"success": False, "message": "El formato del correo no es correcto."}, 400
        
        ######################################################
        
        if not occupation:
            return {"success": False, "message": "La profesión es un campo obligatorio"}, 400
        
        if not province_id:
            return {"success": False, "message": "El departamento es un campo obligatorio"}, 400
        
        if not city_id:
            return {"success": False, "message": "La ciudad es un campo obligatorio"}, 400
        
        if not home_address:
            return {"success": False, "message": "La dirección es un campo obligatorio"}, 400
        
        if not mobile:
            return {"success": False, "message": "El teléfono de contacto es un campo obligatorio"}, 400
        
        # create candidate
        query = """INSERT INTO `mod-payroll-core-dev`.`candidates`
        (full_name, doc_type, doc_number, doc_issued, birthdate, email, mobile, home_address, province_id, province_name,
            city_id, city_name, occupation ,description, entity_account, status, created_by, created_at)
            
        VALUE(:full_name,:doc_type, :doc_number , :doc_issued, :birthdate, :email, :mobile, :home_address, :province_id , :province_name, :city_id, 
        :city_name,  :occupation, :description, :entity_account, :status, :created_by, :created_at)"""
        
        params = {
            "full_name" : data.get('full_name'),
            "doc_type" : data.get('doc_type'),
            "doc_number" : data.get('doc_number'),
            "doc_issued" : data.get('doc_issued'),
            "birthdate" : data.get('birthdate'),
            "email" : data.get('email'),
            "mobile" : data.get('mobile'),
            "home_address" : data.get('home_address'),
            "province_id" : data.get('province_id'),
            "province_name" : data.get('province_name'),
            "city_id" : data.get('city_id'),
            "city_name" : data.get('city_name'),
            "occupation" : data.get('occupation'),
            "description" : data.get('description'),
            "entity_account" : data.get('entity_account'),
            "status" : "verified",
            "created_by" : data.get('created_by'),
            "created_at" : datetime.datetime.utcnow(),
        }
        result = db.session.execute(query, params)
        db.session.commit()
        result.close()
        lastInsert = result.lastrowid
        return {'success': True, 'message': "Candidato creado exitosamente", 'id_inserted': lastInsert},200
        
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        return {'success': False, 'message': e },400
    finally:
        db.session.close()

def create_contract(data):
    vacancy_type = data.get("vacancy_type")
    job_position = data.get("job_position")
    contract_type = data.get("contract_type")
    start_date = data.get("start_date")
    site_id = data.get("site_id")
    site_name = data.get("site_name")
    site_address = data.get("site_address")
    base_salary = data.get("base_salary")
    candidate = data.get("candidate")
    entity_account = data.get("entity_account")
    created_by = data.get("created_by")
    
    status = data.get("status")
    end_date = data.get("end_date")
    non_const_income = data.get("non_const_income")
    
    try:
        if not entity_account:
            return {"success": False, "message": "La cuenta es un campo obligatorio"}, 400
        
        if not candidate:
            return {"success": False, "message": "El candidato es un campo obligatorio"}, 400
        
        if not base_salary:
            return {"success": False, "message": "El salario base es un campo obligatorio"}, 400
        
        if not site_id:
            return {"success": False, "message": "La sede es un campo obligatorio"}, 400
        
        if not start_date:
            return {"success": False, "message": "La fecha es un campo obligatorio"}, 400
        
        if not contract_type:
            return {"success": False, "message": "El tipo de contrato es un campo obligatorio"}, 400
        
        if not job_position:
            return {"success": False, "message": "El cargo es un campo obligatorio"}, 400
        
        if not vacancy_type:
            return {"success": False, "message": "El proceso de contratación es un campo obligatorio"}, 400
        
        # Depending on the type of hiring process, an additional validation must be done
        if str(vacancy_type) == "2":
            return save_contract(data)
        
        elif str(vacancy_type) == "1":
            # valid endDate
            valid = """ 
                SELECT d.end_date as end_date
                FROM candidates AS a
                INNER JOIN applicants AS b ON b.candidate = a.id
                INNER JOIN announcements AS c ON c.id = b.announcement
                LEFT JOIN contracts AS d ON d.job_position = c.job_position AND d.candidate = a.id
                WHERE c.job_position = {0} and a.id = {1}
            """.format(job_position, candidate)
            sql = db.engine.execute(valid)
            cont_row = sql.rowcount
            row = sql.fetchall()
            # Si de este query llega a resultar != NULL el índice end_date, al hacer click en Generar contrato se debe validar 
            # adicionalmente que la fecha ingresada en el campo  Fecha (start_date)  >=  end_date.
            if cont_row != 0:
                sw = True
                startDate = datetime.datetime.strptime(start_date,'%Y-%m-%d').date()
                for r in row:
                    if r["end_date"] is not None:
                        if startDate < r["end_date"]:
                            sw = False
                
                if sw:
                    return save_contract(data)
                else:
                    return {"success": False, "message": "El candidato seleccionado tiene un contrato vigente para este cargo"}, 400
                      
            else:
                return save_contract(data)
                
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        return {'success': False, 'message': e },400
    
    finally:
        db.session.close()

def save_contract(data):
    query = """ INSERT INTO `mod-payroll-core-dev`.`contracts`
    (vacancy_type, job_position, contract_type, start_date, end_date, site_id, site_name, site_address, base_salary, 
    non_const_income, candidate, entity_account, created_by, created_at, status, announcement)
    
    VALUE(:vacancy_type,:job_position, :contract_type , :start_date, :end_date, :site_id, :site_name, :site_address, :base_salary , :non_const_income, :candidate, 
    :entity_account,  :created_by, :created_at, :status, :announcement)"""
    
    params = {
        "vacancy_type" : data.get('vacancy_type'),
        "job_position" : data.get('job_position'),
        "contract_type" : data.get('contract_type'),
        "start_date" : data.get('start_date'),
        "end_date" : data.get('end_date'),
        "site_id" : data.get('site_id'),
        "site_name" : data.get('site_name'),
        "site_address" : data.get('site_address'),
        "base_salary" : data.get('base_salary'),
        "non_const_income" : data.get('non_const_income'),
        "candidate" : data.get('candidate'),
        "entity_account" : data.get('entity_account'),
        "created_by" : data.get('created_by'),
        "created_at" : datetime.datetime.utcnow(),
        "status" : "pending",
        "announcement": data.get("announcement") if int(data.get('vacancy_type')) == 1 else None
    }
    
    result = db.session.execute(query, params)
    db.session.commit()
    result.close()
    lastInsert = result.lastrowid
    
    # Generation of contract in PDF
    pdf_contract = generation_pdf_contract(lastInsert, False, None)
        
    return {'success': True, 'message': "Contrato creado exitosamente", 'contract': lastInsert, "pdf_contract": pdf_contract},200

def sign_contract(data):
    
    contract = data.get("contract")
    base64 = data.get("base64")
    
    if contract and base64:
        signed_contract = generation_pdf_contract(contract, True, base64)
        return signed_contract
    else:
        return {"success": False, "message": "No es posible generar un contrato, la firma no se generó correctamente"}, 400

def replace_variables_contracts(variables, contract, signed, signature):
    
    if len(variables):
        var_values = {}
        contract_information = """
            SELECT * FROM `mod-payroll-core-dev`.contracts WHERE id = {}
        """.format(contract)
        sql = db.engine.execute(contract_information)
        contract_info = sql.fetchone()
        
        if contract_info["id"] is not None:
            for v in variables:
                find_var = """ 
                    SELECT * FROM `mod-payroll-core-dev`.contract_templates_vars where var_name = '{0}' AND contract_type = {1};
                """.format(v, contract_info["contract_type"])
                sql = db.engine.execute(find_var)
                row = sql.fetchone()
                if row["id"] is not None:
                    if row["contract_param"] is not None:
                        # attribute in contract
                        contract_attr = contract_info[row["contract_param"]]
                    
                    if row["fixed_value"] is not None:
                        var_values['{{'+row["var_name"]+'}}'] = str(row["fixed_value"])
                    
                    if row["fixed_value"] is None and row["SELECT_field"] is not None and row["INNER_joins"] is None and row["location_table"] is not None and row["location_field"] is not None:
                        # query 
                        exec_query = 'SELECT ' + row["SELECT_field"] + ' FROM ' + row["location_table"] + ' WHERE id = '+ str(contract_attr)+';'
                        sql = db.engine.execute(exec_query)
                        query = sql.fetchone()
                        
                        if query[row["location_field"]] is not None:
                            var_values['{{'+row["var_name"]+'}}'] = str(query[row["location_field"]])
                    
                    if row["fixed_value"] is None and row["SELECT_field"] is not None and row["INNER_joins"] is not None and row["location_table"] is not None and row["location_field"] is not None:
                        # query
                        exec_query = 'SELECT ' + row["SELECT_field"] + ' FROM ' + row["location_table"] + ' ' + row["INNER_joins"] + ' WHERE '+ row["location_table"]+'.id = '+ str(contract_attr)+';'
                        sql = db.engine.execute(exec_query)
                        query = sql.fetchone()
                        
                        if query[row["location_field"]] is not None:
                            var_values['{{'+row["var_name"]+'}}'] = str(query[row["location_field"]])
                    
                       
            
            # signature validation
            if signed:
                var_values["{{"+"SIGNATURE"+"}}"] = "data:image/png;base64,"+signature
                
            return {"success": True, "variables": var_values}, 200
                    
        else:
            return {"success": False, "message": "No es posible generar un contrato en pdf, error en la plantilla."}, 400        
    else:
        return {"success": False, "message": "No es posible generar un contrato en pdf, error en la plantilla."}, 400

def generation_pdf_contract(contract, signed, signature):
    
    if not contract:
        return {"success": False, "message": "El atributo de contrato es campo obligatorio"}, 400
    
    # contract information 
    info = """
            SELECT cty.template_body FROM `mod-payroll-core-dev`.contracts as ct
            INNER JOIN contract_types as cty
            ON ct.contract_type = cty.id
            where ct.id = {0};
    """.format(contract)
    
    sql = db.engine.execute(info)
    cont_row = sql.rowcount
    old = sql.fetchone()
    
    if cont_row != 0:
        if old["template_body"] is not None:
            # Find all variables in template body
            variables = re.findall('{{(.+?)}}', old["template_body"])
            template = old["template_body"]
            
            if len(variables) != 0:
                # get and replace variables 
                data = replace_variables_contracts(variables, contract, signed, signature)
            
                if data[0]["success"]:
                    # update values of variables
                    data = data[0]["variables"]
                    for key, value in data.items():
                        template = template.replace(str(key), str(value))
                else:
                    {"success": False, "message": "No es posible generar un contrato en pdf, error en la plantilla."}, 400
            
                
            #render_template is used to generate output from a string that is passed in rather than from a file in the templates folder
            rendered = render_template_string(template)
            file = render_pdf(HTML(string=rendered))
            
            # convert file to base64
            data_file_convert = base64.b64encode(file.data)
            
            return json.loads('{"key":"%s"}' % (data_file_convert)), 200
        else:
            {"success": False, "message": "No es posible generar un contrato en pdf, error en la plantilla."}, 400
    else:
        return {"success": False, "message": "No es posible generar un contrato en pdf, no existe una plantilla para este tipo de contrato"}, 400
        

def list_contracts(data):
    account = data.get("account")
    status = data.get("status")
    area = data.get("area")
    search = data.get("search")
    page = data.get("page")
    perpage = data.get("perpage")

    try:
        records = """ SELECT con.entity_account as account , con.id as contract_id, vt.id as contract_process, vt.description AS contract_process_description, ann.id as annoncement_id, ann.title as annoncement,
        job.id AS job_positionid, job.job_title AS position, job.job_description AS job_description, ctt.id as contract_typeid, ctt.description as contract_typename,
        job.area_name AS area, DATE_FORMAT(con.start_date, '%%d/%%m/%%Y') AS star_date, 
        DATE_FORMAT(con.end_date, '%%d/%%m/%%Y') AS end_date, con.site_id as site_id, 
        con.site_name as site_name, con.site_address as site_address,
        cand.id as cand_id, cand.full_name AS candidate, cand.doc_number as cand_docnumber,
        ct.value AS status, cc.background AS color , cc.fontcolor,
        CAST(con.base_salary as char) as base_salary, CAST(con.non_const_income as char) as non_const_income
        FROM `mod-payroll-core-dev`.contracts AS con """
        
        records = records + """
            inner join `mod-payroll-core-dev`.vacancy_types AS vt 
            on con.vacancy_type = vt.id 
            inner join `mod-payroll-core-dev`.status AS ct 
            on con.status = ct.name AND ct.prefix = 'contracts'
            inner join `mod-payroll-core-dev`.status_color AS cc 
            on cc.id = ct.color_id
            inner join `mod-payroll-core-dev`.job_positions AS job 
            on con.job_position = job.id 
            inner join `mod-payroll-core-dev`.candidates AS cand 
            on con.candidate = cand.id
            inner join `mod-payroll-core-dev`.contract_types AS ctt 
            on con.contract_type = ctt.id
            left join `mod-payroll-core-dev`.announcements AS ann 
            on con.announcement = ann.id
        """  

        records = records + 'WHERE con.entity_account = '+ account
        
        if status:
            records = records + ' AND con.status = ' + '"' + str(status) + '"'
        
        if area:
            records = records + ' AND job.area_id =' + area
        
        if search:
            records = records + " AND (job.job_title LIKE ('%%"+ str(search) +"%%')" 
            records = records + " OR cand.full_name LIKE ('%%"+ str(search) +"%%'))"
            
        records = records + ' ORDER BY FIELD (ct.value, "Pendiente", "Firmado", "Vigente" , "Finalizado", "Cancelado")'
        cont = db.engine.execute(records)
        cont_row = cont.rowcount
        
        if page and perpage:
            page = int(page)
            perpage = int(perpage)
            offset = (perpage*page)-perpage
            records = records + ' LIMIT '+ str(perpage) + ' OFFSET ' + str(offset)
            
        records+=';'
        sql = db.engine.execute(records)
        
        # {
        #     empleado: "Kevin Castillo",
        #     nombreDeCargo: "desarrollador FullStack",
        #     area: "Tecnologia",
        #     fecha: "23-09-2021",
        #     firma: true,
        #     estado: "1"
        # }
        
        return jsonify({'results': [(dict(row)) for row in sql.fetchall()], 'row_total': cont_row})
    
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        return responseError(e, 400)
    finally:
        db.session.close()

############################################################    
def get_document_types():
    query = """SELECT * FROM `mod-payroll-core-dev`.document_types where active = 1"""
    result = db.session.execute(query)
    finalresult = []
    for row in result.fetchall():
        rowdict = dict(row)
        finalresult.append(rowdict)     
    result.close()
    return {'success': True, 'results':finalresult },200
    
def complementary_docs_get_sections():
    query_section = "SELECT DISTINCT section FROM complementary_docs"
    query_main = "SELECT * from complementary_docs"
    
    result_section = db.session.execute(query_section)
    result_main = db.session.execute(query_main)

    finalsections = []
    for row_s in result_section.fetchall():
        rowdict_s = dict(row_s)
        finalsections.append(rowdict_s)     
    result_section.close()

    finalresult = []
    for row in result_main.fetchall():
        rowdict = dict(row)
        finalresult.append(rowdict)     
    result_main.close()
    
    return {'success': True, 'results':finalresult, 'sections':finalsections },200

def job_position_areas():
    query_main = "SELECT DISTINCT area_id, area_name FROM job_positions where area_id != ''"

    result_main = db.session.execute(query_main)

    finalresult = []
    for row in result_main.fetchall():
        rowdict = dict(row)
        finalresult.append(rowdict)     
    result_main.close()
    
    return {'success': True, 'results':finalresult},200

def create_endowment_group(data):
    # required name and articles
    description = data.get('description')
    articles = data.get('articles')
    if not description:
        return responseError("description : "+ data.get('description'), 400, "Debe ingresar un nombre de grupo")
    if not articles:
        return responseError("articles :" + data.get('articles'), 400, "Debe relacionar artículos al grupo")
    articles = data.get('articles')
    if len(articles) == 0:
        return responseError("articles", 400, "Debe agregar artículos al grupo")
    try:
        st = """INSERT INTO endowment_group (description, active) values ('{}', 1)""".format(description)
        result = db.session.execute(st)
        db.session.commit()
        lastInsert = result.lastrowid
        # db.session.flush()
        for article in articles:
            if not article.get('id_article'):
                return responseError("id_article", 400, "Artículo inválido")
            if not article.get('min_quantity'):
                return responseError("min_quantity", 400, "Artículo inválido")
            insert = """
                INSERT INTO endowment_articles (description, active, min_quantity, id_article, id_end_group)
                values (:description,:active,:min_quantity, :id_article, :id_end_group)
            """
            params = {
                'description': article.get('description'),
                'active': 1,
                'min_quantity': article.get('min_quantity'),
                'id_article': article.get('id_article'),
                'id_end_group': lastInsert,
            }
            db.session.execute(insert, params)
            db.session.commit()
        return {'success': True, 'results': description, 'message':"Grupo dotacional creado"}
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        db.session.rollback()
        return responseError(e, 500)
    finally:
        db.session.close()

def get_endownment_groups(params):
    try:
        newparams = {}
        query = "SELECT * FROM endowment_group WHERE id is not NULL"
        querycount = "SELECT COUNT(1) FROM endowment_group WHERE id is not NULL"
        
        if params.get('name'):
            query += " AND description LIKE '%{}%'".format(params.get('name'))
            querycount += " AND description LIKE '%{}%'".format(params.get('name'))
            newparams['name'] = params.get('name', type = str)

        if params.get('active'):
            query += " AND active = :active"
            querycount += " AND active = :active"
            newparams['active'] = params.get('active', type = int)

        
        finaltotal = db.session.execute(querycount, newparams).scalar()

        if params.get("page", type = int) is not None and params.get("perpage", type = int) is not None:
            query +=" LIMIT :perpage OFFSET :offset "
            newparams['offset'] = params.get("perpage", type = int) * (params.get("page", type = int) - 1)
            newparams['perpage'] = params.get('perpage', type = int)

        res = db.session.execute(query, newparams)

        finalresult = []
        for row in res.fetchall():
            dictrow = dict(row)
            myrow = {}
            for i in dictrow:
                myrow[i]=json_serial(dictrow[i])
            myrow['articles'] = get_articles_endownment_group(row.id)
            finalresult.append(myrow)

        return {"results": finalresult, "row_count":finaltotal}, 200
        # res = db.session.execute(query, params)
        # results = ([dict({'id':row.id, 'description':row.description, 'active':row.active,'articles':get_articles_endownment_group(row.id)}) for row in res.fetchall()])
        # return responseSuccess(results, "OK", len(results))
    except Exception as e:
        return responseError(e, 500)
    finally:
        db.session.close()

def get_articles_endownment_group(id_group):
    articles_query = """SELECT * FROM endowment_articles WHERE id_end_group = {}""".format(id_group)
    res_articles = db.session.execute(articles_query).fetchall()    

    return [dict(row) for row in res_articles]


def edit_endownment_groups(data):
    try:
        groupId = data.get('id')
        articles = data.get('articles')

        query = """ UPDATE endowment_group SET """
        
        params={
            'id': groupId
        }

        setarraytemp = []

        if data.get('active') is not None:
            setarraytemp.append("  active = :active ")
            params['active']=data.get('active')
        
        if data.get('description') is not None:
            setarraytemp.append("  description = :description ")
            params['description']=data.get('description')

        query = query + ",".join(setarraytemp) + " WHERE id = :id "

        result = db.session.execute(query, params)



        if  isinstance(articles, list):

            second_query = "DELETE FROM endowment_articles WHERE id_end_group = :id"
            second_result = db.session.execute(second_query, params)

            thirdquery = "INSERT INTO endowment_articles"  +\
            " (description, active, min_quantity, id_article, id_end_group, renewal_days)" +\
            " VALUES "
            first = True
            for art in articles:
                if first:
                    first = False
                else:
                    thirdquery += ", "

                thirdquery += "(" +\
                    "'"+art.get("description")+"'"  +"," +\
                    xstr( 1 )                            +"," +\
                    xstr( art.get("min_quantity") )      +"," +\
                    xstr( art.get("id_article") )        +"," +\
                    xstr( groupId )                      +"," +\
                    xstr( art.get("renewal_days") )      +\
                ")"

            third_result = db.session.execute(thirdquery)


        db.session.commit()
        return {"success": True}, 200
    except Exception as e:
        return responseError(e, 500)
    finally:
        db.session.close()
       
def getCandidateList(params):
    status = params.get('status')
    eaccount = params.get('eaccount')
    full = params.get('full') 
    area = params.get('area_id')
    search = params.get('search')
    filtered = params.get('filtered')
    job_position = params.get('job_position')
    area_leader = params.get('area_leader')
    area_id = params.get('area_id')
    try:
        if(full is None):
            statement = """
                        SELECT a.id as id_candidate, b.id as id_contract, a.full_name, a.doc_number, c.job_title
                        FROM candidates a
                        INNER JOIN contracts b ON b.candidate = a.id
                        INNER JOIN job_positions c ON c.id = b.job_position
                        WHERE 1=1
                        """
            if status is not None:
                statement += " AND b.status = :status"

            if eaccount is not None:
                statement += " AND a.entity_account = :eaccount"
                
            if job_position is not None:
                statement  += " AND c.id = :job_position"
            
            if area_leader is not None:
                statement += " AND c.area_leader = :area_leader"

            if area_id is not None:
                statement += " AND c.area_id = :area_id"
                
            if filtered is not None:
                statement += " GROUP BY a.id"    

            res = db.session.execute(statement, {'status':status, 'eaccount':eaccount, 'job_position':job_position, 'area_leader' : area_leader, 'area_id' : area_id } )
            results = [dict(row) for row in res.fetchall()]
            return responseSuccess(results, "OK", len(results))
        else:
            params = {}
            statement = """
                        SELECT cd.id, cd.full_name, jp.job_title, us.doc_num, jp.area_id, jp.area_name, ct.status,
                        cd.entity_account, jp.id as job_position_id
                        FROM `mod-payroll-core-dev`.candidates cd
                        inner join `mod-payroll-core-dev`.contracts ct on ct.candidate = cd.id 
                        inner join `mod-payroll-core-dev`.job_positions jp on jp.id = ct.job_position
                        left join `ttn-security-core-dev`.users us on us.doc_num = cd.doc_number
                        WHERE 1=1 """
            if status is not None:
                statement += " AND ct.status = :status"
                params['status'] = status

            if eaccount is not None:
                statement += " AND cd.entity_account = :eaccount"
                params['eaccount'] = eaccount
            if area is not None:
                statement += " AND jp.area_id = :area"
                params['area'] = area
            if search is not None:
                statement += " AND (cd.full_name LIKE '%{}%' or jp.job_title  LIKE '%{}%' )".format(search,search)
            if filtered is not None:
                statement += " GROUP BY cd.id"    
            statement += " order by cd.full_name, cd.id"
            result = db.session.execute(statement, params)
            finalresult = []
            for row in result.fetchall():
                dictrow = dict(row)
                myrow = {}
                for i in dictrow:
                    myrow[i]=json_serial(dictrow[i])
                finalresult.append(myrow)    
            return {'success': True, 'results' : finalresult , 'lenght' : len(finalresult) }, 200
        
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        return responseError(e,500, "Ocurrió un error al traer el listado de contratados")
    finally:
        db.session.close()
    

def calculateAge(birthDate):
    today = datetime.date.today()
    age = today.year - birthDate.year - \
    ((today.month, today.day) < (birthDate.month, birthDate.day))
    
    return age


def getCandidateWorkExperience(id_candidate):
    try:
        sql = """
            SELECT a.*, json_arrayagg(
                json_object(
                    "id",b.id,
                    "company_name", b.company_name,
                    "start_date", b.start_date,
                    "end_date",b.end_date,
                    "description", b.description
                    ) 
                ) as workExperience FROM `mod-payroll-core-dev`.candidates a 
            LEFT JOIN candidate_workexp b on b.candidate = a.id
            WHERE a.id = :id_candidate
            ORDER BY b.id
            ;
        """
        param = {'id_candidate':id_candidate}
        response = db.session.execute(sql, param)
        res = response.fetchone()
        dictrow = dict(res)
        myrow = {}
        for i in dictrow:
            if i != "workExperience":
                myrow[i]=json_serial(dictrow[i])
            else:
                if dictrow[i] is not None:
                    myrow[i]=json.loads( dictrow[i])
        return responseSuccess(myrow, "OK", 1)

    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        return responseError(str(e), 500, "Ocurrió un error al traer la experiencia laboral del candidato")
    finally:
        db.session.close()


def getCandidateEducationLevel(id_candidate):
    try:
        stm = """
            SELECT a.*, json_arrayagg(
                json_object(
                    "id",c.id,
                    "education_type", c.description,
                    "graduation_date", b.graduation_date,
                    "institution_name",b.institution_name,
                    "description", b.description
                    ) 
                ) as education FROM `mod-payroll-core-dev`.candidates a 
            LEFT JOIN candidate_educlevel b on b.candidate = a.id
            LEFT JOIN education_level c on c.id = b.education_level
            WHERE a.id = :id_candidate
            ORDER BY c.id;
        """
        param = {'id_candidate': id_candidate}
        response = db.session.execute(stm, param)
        res = response.fetchone()
        dictrow = dict(res)
        myrow = {}
        for i in dictrow:
            if i != "education":
                myrow[i]=json_serial(dictrow[i])
            else:
                if dictrow[i] is not None:
                    myrow[i]=json.loads(dictrow[i])
        return responseSuccess(myrow, "OK", 1)

    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        return responseError(str(e), 500, "Ocurrió un error al traer la información de educación del candidato")
    finally:
        db.session.close()


def getCandidateGeneralInfo(candidate, contract):
    try:
        stm = """
            SELECT aaa.*
                FROM
                    (SELECT 
                        a.id AS id_contract,
                            b.id AS id_candidate,
                            b.full_name,
                            b.doc_number,
                            b.doc_type,
                            b.doc_issued,
                            j.description document_type,
                            b.birthdate,
                            CASE b.gender 
                                WHEN 'male'THEN 'Masculino'
                                WHEN 'female' THEN 'Femenino'
                                WHEN 'other' THEN 'Otro'
                                END AS gender,
                            CASE b.marital_status
                                WHEN 'married' THEN 'Casado'
                                WHEN 'single' THEN 'Soltero'
                                WHEN 'free_union' THEN 'Uninón libre' ELSE '' END as marital_status,
                            b.children_qty,
                            b.mobile,
                            b.email,
                            b.home_address,
                            b.emergency_contact,
                            b.emercont_relationship,
                            b.emercont_mobile,
                            b.blood_type,
                            b.rh_factor,
                            b.heigth_cm,
                            b.weight_kg,
                            b.shirt_size,
                            b.pant_size,
                            b.shoe_size,
                            pp.id AS province_id,
                            pp.name AS province_name,
                            b.id AS city_id,
                            cities.name AS city_name,
                            cc.id AS country_id,
                            cc.name AS country_name,
                            c.description,
                            d.id job_position,
                            d.job_title,
                            d.area_id,
                            b.s3_photo AS photo,
                            e.description AS consumption_center,
                            e.functional_unit_id,
                            f.description AS functional_unit_description,
                            a.start_date,
                            a.site_id,
                            a.site_name,
                            a.site_address,
                            a.base_salary,
                            a.non_const_income,
                            g.id AS hce_id,
                            g.description AS hce_description,
                            b.pension_fund_entity,
                            h.description AS pension_description,
                            b.occupational_risk_entity,
                            i.description AS occupational_risk_description,
                            b.compensation_center as compensation_center_id,
                            q.description as compensation_center,
                            b.bank_account_nbr,
                            b.bank_account_type,
                            b.bankingEntity,
                            b.s3_bank_certificate_doc,
                            b.s3_data_verification,
                            b.s3_clinic_history,
                            (SELECT 
                                    JSON_ARRAYAGG(
                                        JSON_OBJECT(
                                            'id', c1.id,
                                            'education_type', c1.description,
                                            'graduation_date', b1.graduation_date,
                                            'institution_name', b1.institution_name, 'description', b1.description
                                            )
                                            )
                                FROM
                                    `mod-payroll-core-dev`.candidates a1
                                LEFT JOIN candidate_educlevel b1 ON b1.candidate = a1.id
                                LEFT JOIN education_level c1 ON c1.id = b1.education_level
                                WHERE
                                    a1.id = :candidate) AS education,
                            (
                                SELECT json_arrayagg(
                                    json_object(
                                        "id",b2.id,
                                        "company_name", b2.company_name,
                                        "start_date", b2.start_date,
                                        "end_date",b2.end_date,
                                        "description", b2.description
                                        ) 
                                    ) as workExperience FROM candidates a2
                                LEFT JOIN candidate_workexp b2 on b2.candidate = a2.id
                                WHERE a2.id = :candidate
                            ) as workExperience
                    FROM
                        contracts a
                    INNER JOIN candidates b ON b.id = a.candidate
                    INNER JOIN contract_types c ON a.contract_type = c.id
                    INNER JOIN job_positions d ON d.id = a.job_position
                    LEFT JOIN `ttn-inventory-core-dev`.consumption_center e ON e.id = d.area_id
                    LEFT JOIN `ttn-inventory-core-dev`.functional_units f ON f.id = e.functional_unit_id
                    LEFT JOIN health_care_entities g ON g.id = b.healthcare_entity
                    LEFT JOIN `pension_fund_entities` h ON h.id = b.pension_fund_entity
                    LEFT JOIN `occupational_risk_entities` i ON i.id = b.occupational_risk_entity
                    LEFT JOIN document_types j ON j.id = b.doc_type
                    LEFT JOIN rel_candidate_endowgroup m ON m.candidate = b.id AND contract = a.id
                    INNER JOIN `ttn-administration-core-dev`.provinces pp ON pp.id = b.province_id
                    INNER JOIN `ttn-administration-core-dev`.countries cc ON cc.id = pp.country
                    INNER JOIN `ttn-administration-core-dev`.cities ON cities.id = b.city_id
                    LEFT JOIN compensation_center_entities q on q.id = b.compensation_center
                    LEFT JOIN endowment_group n ON n.id = m.endowment_group    
                    WHERE
                        b.id = :candidate AND a.id = :contract
                    GROUP BY b.id) AS aaa
        """
        param = {'candidate': candidate, 'contract':contract}
        response = db.session.execute(stm, param)
        res = response.fetchone()
        dictrow = dict(res)
        myrow = {}
        for i in dictrow:
            if i != "education" and i != "workExperience":
                myrow[i]=json_serial(dictrow[i])
            else:
                if dictrow[i] is not None:
                    myrow[i]=json.loads(dictrow[i])
        return responseSuccess(myrow, "OK", 1)

    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        return responseError(str(e), 500, "Ocurrió un error al traer la información del candidato")
    finally:
        db.session.close()
        

def getCandidateInfo(params):
    id_contract = params.get('id_contract')    
    id_candidate = params.get('id_candidate')
    if id_candidate is None:
        return responseError("id_candidate", 400, "Debe seleccionar un candidato")
    if id_contract is None:
        return responseError("id_contract", 400, "Debe seleccionar un contrato")
    if params.get('education') and params.get('education') == "1":
        return getCandidateEducationLevel(id_candidate)
    if params.get('workExperience') and params.get('workExperience') == "1":
        return getCandidateWorkExperience(id_candidate)
    if params.get('generalInfo') and params.get('generalInfo') == "1":
        return getCandidateGeneralInfo(id_candidate,id_contract )
    if params.get('annotations') and params.get('annotations') == "1":
        return getAnnotations({'employee':id_candidate})
    try:
        stm = """
            SELECT 
            bbb.*,
            JSON_ARRAYAGG(JSON_OBJECT('section', r.section)) AS sections
        FROM
            (SELECT 
                aaa.*,
                    JSON_ARRAYAGG(JSON_OBJECT('s3_location', k1.s3_location, 'id', k1.comp_doc, 'section', k1.section, 'doc_description', k1.description, 'prefix', k1.prefix)) AS documents
            FROM
                (SELECT 
                a.id AS id_contract,
                    b.id AS id_candidate,
                    b.full_name,
                    b.doc_number,
                    b.doc_type,
                    b.doc_issued,
                    j.description document_type,
                    b.birthdate as age,
                    b.birthdate,
                    CASE b.gender 
                        WHEN 'male'THEN 'Masculino'
                        WHEN 'female' THEN 'Femenino'
                        WHEN 'other' THEN 'Otro'
                        END AS gender,
                    CASE b.marital_status
                        WHEN 'married' THEN 'Casado'
                        WHEN 'single' THEN 'Soltero'
                        WHEN 'free_union' THEN 'Uninón libre' ELSE '' END as marital_status,
                    b.children_qty,
                    b.mobile,
                    b.email,
                    b.home_address,
                    b.emergency_contact,
                    b.emercont_relationship,
                    b.emercont_mobile,
                    b.blood_type,
                    b.rh_factor,
                    b.heigth_cm,
                    b.weight_kg,
                    b.shirt_size,
                    b.pant_size,
                    b.shoe_size,
                    pp.id AS province_id,
                    pp.name AS province_name,
                    b.id AS city_id,
                    cities.name AS city_name,
                    cc.id AS country_id,
                    cc.name AS country_name,
                    c.description,
                    d.id job_position,
                    d.job_title,
                    d.area_id,
                    b.s3_photo AS photo,
                    e.description AS consumption_center,
                    e.functional_unit_id,
                    f.description AS functional_unit_description,
                    a.start_date,
                    a.site_id,
                    a.site_name,
                    a.site_address,
                    a.base_salary,
                    a.non_const_income,
                    g.id AS hce_id,
                    g.description AS hce_description,
                    b.pension_fund_entity,
                    h.description AS pension_description,
                    b.occupational_risk_entity,
                    i.description AS occupational_risk_description,
                    b.bank_account_nbr,
                    b.bank_account_type,
                    b.bankingEntity,
                    b.s3_bank_certificate_doc,
                    b.s3_data_verification,
                    b.s3_clinic_history,
                    a.signed_doc,
                    IF(COUNT(DISTINCT n.id) = 0, NULL, JSON_ARRAYAGG(JSON_OBJECT('id', n.id, 'description', n.description))) AS endowmentGroups
            FROM
                contracts a
            INNER JOIN candidates b ON b.id = a.candidate
            INNER JOIN contract_types c ON a.contract_type = c.id
            INNER JOIN job_positions d ON d.id = a.job_position
            LEFT JOIN `ttn-inventory-core-dev`.consumption_center e ON e.id = d.area_id
            LEFT JOIN `ttn-inventory-core-dev`.functional_units f ON f.id = e.functional_unit_id
            LEFT JOIN health_care_entities g ON g.id = b.healthcare_entity
            LEFT JOIN `pension_fund_entities` h ON h.id = b.pension_fund_entity
            LEFT JOIN `occupational_risk_entities` i ON i.id = b.occupational_risk_entity
            LEFT JOIN document_types j ON j.id = b.doc_type
            LEFT JOIN rel_candidate_endowgroup m ON m.candidate = b.id AND contract = a.id
            INNER JOIN `ttn-administration-core-dev`.provinces pp ON pp.id = b.province_id
            INNER JOIN `ttn-administration-core-dev`.countries cc ON cc.id = pp.country
            INNER JOIN `ttn-administration-core-dev`.cities ON cities.id = b.city_id
            LEFT JOIN endowment_group n ON n.id = m.endowment_group
            WHERE
                b.id = :id_candidate AND a.id = :id_contract
            GROUP BY b.id) AS aaa
            LEFT JOIN (SELECT DISTINCT
                m.comp_doc,
                    k.s3_location,
                    k.candidate,
                    l.section,
                    l.description,
                    l.prefix,
                    m.job_position
            FROM
                rel_jobpos_compdoc m
            INNER JOIN complementary_docs l ON (l.id = m.comp_doc)
            LEFT JOIN rel_candidate_compdoc k ON (k.comp_doc = l.id AND k.candidate = :id_candidate)
            ORDER BY l.id , m.id) k1 ON k1.job_position = aaa.job_position) AS bbb
                LEFT JOIN
            (SELECT DISTINCT
                l.section, k.candidate
            FROM
                rel_jobpos_compdoc m
            INNER JOIN complementary_docs l ON (l.id = m.comp_doc)
            INNER JOIN contracts k ON (k.job_position = m.job_position AND k.candidate = :id_candidate)
            ORDER BY l.id , m.id) r ON r.candidate = bbb.id_candidate
            GROUP BY bbb.id_candidate; 	
            """

        params = {'id_candidate':id_candidate, 'id_contract':id_contract}
        res = db.session.execute(stm, params)
        res = res.fetchone()
        if res is not None:
            row = res
            dictrow = dict(row)
            myrow = {}
            for i in dictrow:
                if i != "documents" and i != "endowmentGroups" and i != "sections":
                    myrow[i]=json_serial(dictrow[i])
                    if i == "age":
                        if dictrow[i] is not None:
                            myrow[i]=json_serial(calculateAge(dictrow[i]))
                        else:
                            myrow[i]=json_serial({'age':None})
                        
                else:
                    if dictrow[i] is not None:
                        myrow[i]=json.loads( dictrow[i])
            return responseSuccess(myrow, "OK", 1)
        else:
            return responseSuccess(None, "No se encontró un contrato para el candidato",0)

    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        return responseError(str(e), 500, "Ocurrió un error al traer la información del candidato")
    finally:
        db.session.close()

def get_recipient_type():
    query = """SELECT * FROM `mod-payroll-core-dev`.adjustment_recipient"""
    result = db.session.execute(query)
    finalresult = []
    for row in result.fetchall():
        rowdict = dict(row)
        finalresult.append(rowdict)     
    result.close()
    return {'success': True, 'results':finalresult },200


def get_eps(params):
    try:
        stm = "SELECT * FROM `mod-payroll-core-dev`.health_care_entities WHERE 1=1 "
        paramArray = []
        if params.get('id'):
            paramArray.append('AND id = "'+str(params.get('id'))+'"') 
        if params.get('description'):
            paramArray.append('AND description LIKE "'+str(params.get('description'))+'"') 
        if params.get('alias'):
            paramArray.append('AND alias LIKE "'+str(params.get('alias'))+'"') 
        if params.get('enabled'):
            paramArray.append('AND enabled = "'+str(params.get('enabled'))+'"')
        if params.get('page') and params.get('perpage'):
            offset = params.get("perpage", type = int) * (params.get("page", type = int) - 1)
            paramArray.append('LIMIT = "'+str(params.get('perpage'))+'"'+ "OFFSET = "+offset)
        stm += ",".join(paramArray)
        res = db.session.execute(stm)
        results = [dict(row) for row in res.fetchall()]
        return responseSuccess(results, "OK", res.rowcount)
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        return responseError(e,500, "Ocurrió un error al traer el listado de EPS" )
    finally:
        db.session.close()


def get_occupational_risk_entities(params):
    try:
        stm = "SELECT * FROM occupational_risk_entities WHERE 1=1 "
        paramArray = []
        if params.get('id'):
            paramArray.append('AND id = "'+str(params.get('id'))+'"') 
        if params.get('description'):
            paramArray.append('AND description LIKE "'+str(params.get('description'))+'"') 
        if params.get('alias'):
            paramArray.append('AND alias LIKE "'+str(params.get('alias'))+'"') 
        if params.get('enabled'):
            paramArray.append('AND enabled = "'+str(params.get('enabled'))+'"')
        if params.get('page') and params.get('perpage'):
            offset = params.get("perpage", type = int) * (params.get("page", type = int) - 1)
            paramArray.append(' LIMIT = '+str(params.get('perpage'))+'OFFSET = ' +offset)
        stm += ",".join(paramArray)
        res = db.session.execute(stm)
        results = [dict(row) for row in res.fetchall()]
        return responseSuccess(results, "OK", res.rowcount)
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        return responseError(e,500, "Ocurrió un error al traer el listado de ARL" )
    finally:
        db.session.close()
    
############################################################
def rearranged_contract(contract, data):
    job_position = data.get("job_position") 
    base_salary = data.get("base_salary") 
    end_date = data.get("end_date") 
    entity_account = data.get("entity_account")
    non_const_income = data.get("non_const_income")
    created_by = data.get("created_by")
    start_date = data.get("start_date")
    
    if not job_position:
        return {"success": False, "message": "El cargo es un campo obligatorio"}, 400
    
    if not base_salary:
        return {"success": False, "message": "El salario base es un campo obligatorio"}, 400
        
    if not end_date:
        return {"success": False, "message": "La vigencia es un campo obligatorio"}, 400
    
    if not start_date:
        return {"success": False, "message": "La vigencia es un campo obligatorio"}, 400
    
    if not entity_account:
        return {"success": False, "message": "La cuenta es un campo obligatorio"}, 400
    
    if not created_by:
        return {"success": False, "message": "El atributo de creado por es obligatorio"}, 400
    
    if contract:
        info_contract = """
            SELECT * FROM `mod-payroll-core-dev`.contracts where id = {0} and entity_account = {1} and status = "signed"
        """.format(contract,entity_account)
        
        sql = db.engine.execute(info_contract)
        cont_row = sql.rowcount
        old = sql.fetchone()
        
        if cont_row != 0:
            
            # old info contract
            old_contract = {
                "contract": contract,
                "job_position" : old["job_position"],
                "base_salary" : old["base_salary"],
                "non_const_income" : old["non_const_income"] if old["non_const_income"] is not None else 0,
                #"start_date" : old["start_date"],
                "end_date" : old["end_date"],
                "signed_doc": old["signed_doc"],
                "created_by" : created_by,
                "created_at" : datetime.datetime.utcnow(),
            }
            
            # new info contract
            new_contract = {
                "contract": contract,
                "job_position" : job_position,
                "base_salary" : base_salary,
                "non_const_income" : non_const_income if non_const_income is not None else 0,
                "end_date" : end_date,
                "start_date" : start_date,
                #"signed_doc": old["signed_doc"],
            }
            
            # 1. Create record in contract rearranged with the information of the old contract
            create_contract_rearranged = """ INSERT INTO `mod-payroll-core-dev`.`contract_rearranged`
                (`contract`,`job_position`,`base_salary`,`non_const_income`,`end_date`,`signed_doc`,`created_by`,`created_at`)
                VALUES(:contract,:job_position,:base_salary,:non_const_income,:end_date,:signed_doc,:created_by,:created_at)
            """
            
            # 2. Update contract information with new contract
            update_contract_information = """ UPDATE `mod-payroll-core-dev`.`contracts` SET
                `job_position` = :job_position,
                `start_date` = :start_date,
                `end_date` = :end_date,
                `base_salary` = :base_salary,
                `non_const_income` = :non_const_income
                
                WHERE `id` = :contract ;
            """
            
            update_contract = db.session.execute(update_contract_information, new_contract)
            db.session.commit()
            update_contract.close()
            
            contract_rearranged = db.session.execute(create_contract_rearranged, old_contract)
            db.session.commit()
            contract_rearranged.close()
            lastInsert = contract_rearranged.lastrowid
            
            return {'success': True, 'message': "Otro sí creado exitosamente", 'contract_annex': lastInsert , 'contract': contract},200
        else:
            return {"success": False, "message": "Este contrato no está disponible para otro sí"}, 400
    else:
        return {"success": False, "message": "El atributo de contrato es obligatorio"}, 400

def changeStatusContract(status, contract):
    if contract and status:
        # validate status in contract
        exec_query = """
            SELECT id FROM `mod-payroll-core-dev`.status where prefix = '{0}' and name = '{1}';
        """.format("contracts", status)
        sql = db.engine.execute(exec_query)
        query = sql.fetchone()
        
        if query is not None:
            data = {
                "status": status
            }
            
            return update_contract(data, contract)
            
        else:
            return {"success": False, "message": "El estado seleccionado no existe en la tabla contratos"}, 400
    else:
        return {"success": False, "message": "No es posible modificar el estado del contrato, el atributo estado y contracto son obligatorios"}, 400

def contract_information(contract):
    contract_i = """
        SELECT * FROM `mod-payroll-core-dev`.contracts where id = {};
    """.format(contract)
    sql = db.engine.execute(contract_i)
    cont_row = sql.rowcount
    query = sql.fetchone()
    
    if cont_row != 0:
        result = dict(query)
        return {"success": True, "result": result}, 200
    else:
        return {"success": False, "message": "El contracto no está disponible"}, 400

def get_contract_url(id):
    if not id:
        return {"success": False, "message": "Este contrato no está disponible."}, 400
    
    try:
        query = """
            SELECT signed_doc FROM `mod-payroll-core-dev`.contracts where id = {}
        """.format(id)
        sql = db.engine.execute(query)
        cont_row = sql.rowcount
        query = sql.fetchone()
        
        if cont_row != 0:
            result = dict(query)
            return {"success": True, "result": result}, 200
        else:
            return {"success": False, "result": ""}, 400
    
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        return responseError(e, 500)
    finally:
        db.session.close()
        
def update_contract(id, data):
    try:
        data_update = ""
        update_contract_information = ""
        # Get contract information
        info_contract = contract_information(id)
        if not id:
            return {"success": False, "message": "Este contrato no está disponible."}, 400
        
        if not data:
            return {"success": False, "message": "Es necesario diligenciar la información para actualizar el contrato."}, 400
        
        if not info_contract[0]["success"]:
            return {"success": False, "message": "Este contrato no existe"}, 400
        
        update_contract_information = 'UPDATE `mod-payroll-core-dev`.`contracts` SET '
        
        if data.get("signed_doc"):
            if data_update == "":
                data_update = ' `signed_doc` = '+ "'" + data.get("signed_doc")+ "'"
            else:
                data_update = data_update+' , `signed_doc` ='+ "'" + data.get("signed_doc")+ "'"
        
        if data.get("status"):
            if data_update == "":
                data_update = ' `status` = '+ "'" + data.get("status") + "'"
            else:
                data_update = data_update+' , `status` ='+ "'" + data.get("status") + "'"
        
        if data.get("vacancy_type"):
            old_vacancy = int(info_contract[0]["result"]["vacancy_type"])
            new_vacancy = int(data.get("vacancy_type"))
            if old_vacancy != new_vacancy:
                if info_contract[0]["result"]["status"] == "pending":
                    if data_update == "":
                        data_update = ' `vacancy_type` = '+ str(data.get("vacancy_type"))
                    else:
                        data_update = data_update+' , `vacancy_type` = '+ str(data.get("vacancy_type"))
                else:
                    return {"success": False, "message": "El contracto no se encuentra en estado pendiente, no puede actualizar el tipo de contratación"}, 400
            
        if data.get("announcement"):
            if data_update == "":
                data_update = ' `announcement` = '+ str(data.get("announcement"))
            else:
                data_update = data_update+' , `announcement` = '+ str(data.get("announcement"))
                        
        if data.get("job_position"):
            if data_update == "":
                data_update = ' `job_position` = '+ str(data.get("job_position"))
            else:
                data_update = data_update+' , `job_position` = '+ str(data.get("job_position"))
        
        if data.get("contract_type"):
            if data_update == "":
                data_update = ' `contract_type` = '+ str(data.get("contract_type"))
            else:
                data_update = data_update+' , `contract_type` = '+ str(data.get("contract_type"))
        
        if data.get("start_date"):
            if data_update == "":
                data_update = ' `start_date` = '+ "'" + data.get("start_date") + "'"
            else:
                data_update = data_update+' , `start_date` ='+ "'" + data.get("start_date") + "'"
        
        if data.get("end_date"):
            if data_update == "":
                data_update = ' `end_date` = '+ "'" + data.get("end_date") + "'"
            else:
                data_update = data_update+' , `end_date` = '+ "'" + data.get("end_date") + "'"
        
        if data.get("site_id"):
            if data_update == "":
                data_update = ' `site_id` = '+ str(data.get("site_id"))
            else:
                data_update = data_update+' , `site_id` = '+ str(data.get("site_id"))
        
        if data.get("site_name"):
            if data_update == "":
                data_update = ' `site_name` = '+ "'" + data.get("site_name") + "'"
            else:
                data_update = data_update+' , `site_name` ='+ "'" + data.get("site_name") + "'"
        
        if data.get("site_address"):
            if data_update == "":
                data_update = ' `site_address` = '+ "'" + data.get("site_address") + "'"
            else:
                data_update = data_update+' , `site_address` ='+ "'" + data.get("site_address") + "'"
        
        if data.get("base_salary"):
            if data_update == "":
                data_update = ' `base_salary` = '+ str(data.get("base_salary"))
            else:
                data_update = data_update+' , `base_salary` ='+ str(data.get("base_salary"))
        
        if data.get("non_const_income"):
            if data_update == "":
                data_update = ' `non_const_income` = '+ str(data.get("non_const_income"))
            else:
                data_update = data_update+' , `non_const_income` ='+ str(data.get("non_const_income"))
        
        if data.get("candidate"):
            old_candidate = int(info_contract[0]["result"]["candidate"])
            new_candidate = int(data.get("candidate"))
            if old_candidate != new_candidate:
                if data_update == "":
                    data_update = ' `candidate` = '+ str(data.get("candidate"))
                else:
                    data_update = data_update+' , `candidate` ='+ str(data.get("candidate"))
                    
        # where 
        update_contract_information = update_contract_information + data_update + " WHERE id = "+ id + ";"
        update_contract = db.session.execute(update_contract_information)
        db.session.commit()
        update_contract.close()
        return {"success": True, "message": "Contracto actualizado correctamente", "contract": id}, 200
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        return {'success': False, 'message': e },400
    finally:
        db.session.close()


def get_pension_fund_entities(params):
    try:
        stm = "SELECT * FROM pension_fund_entities WHERE 1=1 "
        paramArray = []
        if params.get('id'):
            paramArray.append('AND id = "'+str(params.get('id'))+'"') 
        if params.get('description'):
            paramArray.append('AND description LIKE "'+str(params.get('description'))+'"') 
        if params.get('alias'):
            paramArray.append('AND alias LIKE "'+str(params.get('alias'))+'"') 
        if params.get('enabled'):
            paramArray.append('AND enabled = "'+str(params.get('enabled'))+'"')
        if params.get('page') and params.get('perpage'):
            offset = params.get("perpage", type = int) * (params.get("page", type = int) - 1)
            paramArray.append('LIMIT = "'+str(params.get('perpage'))+'"'+ "OFFSET = "+offset)
        stm += ",".join(paramArray)
        res = db.session.execute(stm)        
        results = [dict(row) for row in res.fetchall()]
        return responseSuccess(results, "OK", res.rowcount)
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        return responseError(e,500, "Ocurrió un error al traer el listado de ARL" )
    finally:
        db.session.close()



# update endowment groups by candidate
# this method receives an objects array 
def updateEndowmentGroups(groups, id_candidate, id_contract):
    if groups is not None:    
        try:
            delete_stm = "DELETE FROM `mod-payroll-core-dev`.rel_candidate_endowgroup WHERE candidate = :id_candidate AND contract = :id_contract"
            params = {'id_candidate':id_candidate, 'id_contract':id_contract}
            db.session.execute(delete_stm, params)
            db.session.commit()
            insert_stm = "INSERT INTO `mod-payroll-core-dev`.rel_candidate_endowgroup VALUES "
            paramGroups = []
            for group in groups:
                paramGroups.append(" (null, {0}, {1}, {2})".format(id_candidate, group, id_contract))                            
            insert_stm += ",".join(paramGroups)
            db.session.execute(insert_stm)            
            db.session.commit()
            return responseSuccess(results=201, message="Grupo guardados correctamente", row_total=len(groups))
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            logging.error(exc_obj)
            logging.error(exc_type)
            logging.error(fname + ': ' + str(exc_tb.tb_lineno))
            return responseError(str(e), 500, "Ocurrió un error al guardar los grupos de dotación")
        finally:
            db.session.close()


# Get education level
def get_education_level():
    try:        
        res = db.session.execute("SELECT * FROM education_level;")
        response = [dict(row) for row in res.fetchall()]
        return responseSuccess(response, "Listado de tipos de educación", len(response))

    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        return responseError(str(e), 500, "Ocurrió un error al traer la información de tipo de educación")
    finally:
        db.session.close()
# end get education_level
    
def get_personal(data):
    try :
        query = """ SELECT cd.full_name,  cd.id as id_candidate, ct.id as id_contract , jp.job_title, us.doc_num, jp.area_id
        , jp.area_name, cd.entity_account , cd.created_by, cd.percentage
        from """+PAYROLL_DBNAME+""".candidates cd
        inner join """+PAYROLL_DBNAME+""".contracts ct on ct.candidate = cd.id and ct.status = "current"
        inner join """+PAYROLL_DBNAME+""".job_positions jp on jp.id = ct.job_position
        left join """+SECURIT_DBNAME+""".users us on us.doc_num = cd.doc_number
        WHERE 1=1 """
        querycount = """ SELECT COUNT(1)
        from """+PAYROLL_DBNAME+""".candidates cd
        inner join """+PAYROLL_DBNAME+""".contracts ct on ct.candidate = cd.id and ct.status = "current"
        inner join """+PAYROLL_DBNAME+""".job_positions jp on jp.id = ct.job_position
        left join """+SECURIT_DBNAME+""".users us on us.doc_num = cd.doc_number
        WHERE 1=1 """
        params = {}
        if data.get("area_id", type = int):
            query +=" AND jp.area_id = :area_id "
            querycount += " AND jp.area_id = :area_id "
            params['area_id'] = data.get("area_id", type = int)

        if data.get("search", type = str):
            query +=" AND (cd.full_name LIKE ('%%"+ data.get("search", type = str)+"%%') or jp.job_title LIKE ('%%"+ data.get("search", type = str)+"%%')) "
            querycount += " AND (cd.full_name LIKE ('%%"+ data.get("search", type = str)+"%%') or jp.job_title LIKE ('%%"+ data.get("search", type = str)+"%%')) "
            params['search'] = data.get("search", type = str)

        if data.get("entity_account", type = int):
            query +=" AND cd.entity_account = :entity_account "
            querycount += " AND cd.entity_account = :entity_account "
            params['entity_account'] = data.get("entity_account", type = int)

        if data.get("created_by", type = int):
            query +=" AND cd.created_by = :created_by "
            querycount += " AND cd.created_by = :created_by "
            params['created_by'] = data.get("created_by", type = int)
        
        resultcount = db.session.execute(querycount, params).scalar()
        
        query += " order by cd.full_name, cd.id "
        if data.get("page", type = int) is not None and data.get("perpage", type = int) is not None:
            offset = data.get("perpage", type = int) * (data.get("page", type = int) - 1)
            query +=" LIMIT :perpage OFFSET :offset "
            params['offset'] = offset
            params['perpage'] = data.get("perpage", type = int)
            
        result = db.session.execute(query, params)
        finalresult = []    
        current = ""                
        for row in result.fetchall():
            dictrow = dict(row)
            myrow = {}
            if(current != dictrow["id_candidate"]):
                current = dictrow["id_candidate"]
            else:
                dictrow["full_name"] = ""
            for i in dictrow:
                myrow[i]=json_serial(dictrow[i])
                
            finalresult.append(myrow)           

        result.close()
        return {'success': True, 'results':finalresult  , 'row_total': resultcount},200
    except Exception as e:
        return {'error': str(e)}, 400

# Create work accdient
def create_work_accdient(data):
    try:
        title = data.get('title')
        if not title:
            return  responseError('title', 400, "El campo title es obligatorio")
        description_event = data.get('description_event')
        if not description_event:
            return  responseError('description_event', 400, "El campo description_event es obligatorio")
        accident_date = data.get('accident_date')
        if not accident_date:
            return  responseError('accident_date', 400, "El campo accident_date es obligatorio")
        reporter = data.get('reporter')
        if not reporter:
            return  responseError('reporter', 400, "El campo reporter es obligatorio")
        involved = data.get('involved')
        if not involved:
            return  responseError('involved', 400, "El campo involved es obligatorio")        
        description_injury = data.get('description_injury')
        if not description_injury:
            return  responseError('description_injury', 400, "El campo description_injury es obligatorio")        
        entity_account = data.get('entity_account')
        if not entity_account:
            return  responseError('entity_account', 400, "El campo entity_account es obligatorio")
        created_by = data.get('created_by')
        if not created_by:
            return  responseError('created_by', 400, "El campo created_by es obligatorio")
        job_position = data.get('job_position')
        if not job_position:
            return  responseError('job_position', 400, "El campo job_position es obligatorio")                
        insertQuery = """
            INSERT INTO work_accidents values (
                null,
                :title,
                :description_event,
                :accident_date,
                :reporter,
                :involved,
                :laboral_inhability,
                :inhability_doc,
                :description_injury,
                :furat,
                :other_docs,
                :entity_account,
                :created_by,
                :job_position,
                :created_at,
                :result,
                :result_file,
                :reviewed_by,
                :reviewed_at,
                :status
            )
        """
        params = {
            'title':data.get('title'),
            'description_event':data.get('description_event'),
            'accident_date':data.get('accident_date'),
            'reporter':data.get('reporter'),
            'involved':data.get('involved'),
            'laboral_inhability':data.get('laboral_inhability'),
            'inhability_doc':data.get('inhability_doc'),
            'description_injury':data.get('description_injury'),
            'furat':data.get('furat'),
            'other_docs':data.get('other_docs'),
            'entity_account':data.get('entity_account'),
            'created_by':data.get('created_by'),
            'job_position':data.get('job_position'),
            'created_at':datetime.datetime.utcnow(),
            'result':data.get('result'),
            'result_file':data.get('result_file'),
            'reviewed_by':data.get('reviewed_by'),
            'reviewed_at':data.get('reviewed_at'),
            'status':data.get('status')
        }
        res = db.session.execute(insertQuery, params)
        db.session.commit()
        return responseSuccess(res.lastrowid, 200, "Se guardó correctamente el accidente")
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        return responseError(str(e), 500, "Ocurrió un error al guardar el accidente")
    finally:
        db.session.close()
# end create work accident


# get work accidents details
def getWorkAccidentDetails(id):
    try:
        if id:
            stm = """SELECT w.*, c.full_name, s.value as statusValue, sc.fontcolor, jp.job_title, jp.area_name,
                c.full_name as user_reviewed FROM work_accidents w
                                INNER JOIN candidates c on c.id = w.involved
                                INNER JOIN contracts con on c.id = con.candidate AND con.status = 'current'
                                INNER JOIN status s on s.name = w.status and s.prefix = 'work_accident'
                                INNER JOIN status_color sc on sc.id = s.color_id
                                INNER JOIN job_positions jp on jp.id = w.job_position                                
                                WHERE 1=1 AND w.id = :id;"""
            res = db.session.execute(stm, {'id':id}).fetchone()

            return responseSuccess({
                'id':res.id,
                'title':res.title,
                'description_event':res.description_event,
                'accident_date':json_serial(res.accident_date),
                'reporter':res.reporter,
                'employee':res.involved,
                'laboral_inhability':res.laboral_inhability,
                'inhability_doc':res.inhability_doc,
                'description_injury':res.description_injury,
                'furat':res.furat,
                'other_docs':res.other_docs,
                'entity_account':res.entity_account,
                'created_by':res.created_by,
                'job_position':res.job_position,
                'created_at':json_serial(res.created_at),
                'result':res.result,
                'result_file':res.result_file,
                'reviewed_by':res.reviewed_by,
                'reviewed_at':json_serial(res.reviewed_at),
                'status':res.status,
                'full_name':res.full_name,
                'statusValue':res.statusValue,
                'statusBackground':res.fontcolor,
                'job_title':res.job_title,
                'area_name':res.area_name,
                'user_reviewed':res.user_reviewed
            }, 200, 1)
        else:
            return responseError("id no valid",400, "ID inválido" )
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        return responseError(str(e), 500, "Ocurrió un error al traer la información del accidente")
    finally:
        db.session.close()
# end 

# get work accidetns
def get_work_accidents(params):
    try:
        stm = """
            SELECT w.*, c.full_name, s.value as statusValue, sc.fontcolor FROM work_accidents w
            INNER JOIN candidates c on c.id = w.involved            
            INNER JOIN status s on s.name = w.status and s.prefix = 'work_accident'
            INNER JOIN status_color sc on sc.id = s.color_id
            WHERE 1=1
        """
        paramArray = {}        
        if params.get('id'):
            return getWorkAccidentDetails(params.get('id'))
            # stm +=" AND w.id = " + str(params.get('id'))            
        if params.get('title'):
            stm +=" AND w.title = '" + str(params.get('search'))+"'"
        if params.get('entity_account'):
            stm += " AND w.entity_account = " + str(params.get('entity_account'))
        if params.get('status'):
            stm += " AND w.status = '" + str(params.get('status')) + "'"
        if params.get('employee'):
            stm += " AND w.involved = " + str(params.get('search'))
        if params.get('search'):
            stm += " AND (full_name LIKE '%" + str(params.get('search')) + "%'\
                OR w.title LIKE '%" + str(params.get('search'))+ "%' OR w.description_event LIKE '%"+ str(params.get('search'))+ "%'\
                OR w.description_injury LIKE '%" + str(params.get('search'))+ "%')"
            paramArray['search'] = str(params.get('search'))

        stm += " ORDER BY accident_date DESC "
        
        rowTotal = db.session.execute(stm).rowcount
        
        if params.get('page') and params.get('perpage'):
            offset = params.get("perpage", type = int) * (params.get("page", type = int) - 1)
            stm += (' LIMIT '+str(params.get('perpage'))+' '+ "OFFSET "+str(offset))
        
        res = db.session.execute(stm)
        results = [dict({
            'id':row.id,
            'title':row.title,
            'description_event':row.description_event,
            'accident_date':json_serial(row.accident_date),
            'employee':row.involved,
            'laboral_inhability':row.laboral_inhability,
            'inhability_doc':row.inhability_doc,
            'description_injury':row.description_injury,
            'furat':row.furat,
            'other_docs':row.other_docs,
            'entity_account':row.entity_account,
            'created_by':row.created_by,
            'job_position':row.job_position,
            'created_at':json_serial(row.created_at),
            'result':row.result,
            'result_file':row.result_file,
            'reviewed_by':row.reviewed_by,
            'reviewed_at':json_serial(row.reviewed_at),
            'status':row.status,
            'full_name':row.full_name,
            'statusValue':row.statusValue,
            'statusBackground':row.fontcolor,
            }) for row in res.fetchall()]

        return responseSuccess(results, "OK", rowTotal)
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        return responseError(str(e), 500, "Ocurrió un error al traer la información de los accidentes")
    finally:
        db.session.close()
# end get work accidetns


def xstr(s):
    return 'Null' if s is None else str(s)

def get_endowment_deliveries(params):
    try:
        newparams = {}
        query = """
        SELECT ed.*,
        (SELECT JSON_ARRAYAGG(
            JSON_OBJECT(
                "id", ea.id,
                "description", ea.description,
                "active", ea.active,
                "min_quantity", ea.min_quantity,
                "id_article", ea.id_article,
                "id_end_group", ea.id_end_group,
                "renewal_days", ea.renewal_days
                )
            ) 
            FROM endowment_articles ea
            LEFT JOIN endowment_dlv_details edd ON edd.endowment_article = ea.id
            WHERE edd.endowment_dlv = ed.id
        ) AS articles
        FROM endowment_deliveries ed
        WHERE 1=1 
        """
        querycount = """
        SELECT COUNT(1) 
        FROM endowment_deliveries
        WHERE 1=1 
        """
        
        if params.get('id'):
            query += " AND ed.id = :id "
            querycount += " AND id = :id "
            newparams['id'] = params.get('id', type = int)

        if params.get('candidate'):
            query += " AND ed.candidate = :candidate "
            querycount += " AND candidate = :candidate "
            newparams['candidate'] = params.get('candidate', type = int)

        finaltotal = db.session.execute(querycount, newparams).scalar()

        if params.get("page", type = int) is not None and params.get("perpage", type = int) is not None:
            query +=" LIMIT :perpage OFFSET :offset "
            newparams['offset'] = params.get("perpage", type = int) * (params.get("page", type = int) - 1)
            newparams['perpage'] = params.get('perpage', type = int)

        res = db.session.execute(query, newparams)

        finalresult = []
        for row in res.fetchall():
            dictrow = dict(row)
            myrow = {}
            for i in dictrow:
                if i == "articles":
                    subarray = []
                    if dictrow[i] is not None:
                        myrow[i]=json.loads(dictrow[i])
                    else:
                        myrow[i]=None
                else:
                    myrow[i]=json_serial(dictrow[i])
            finalresult.append(myrow)
        return {"results": finalresult, "row_count":finaltotal}, 200
    except Exception as e:
        return responseError(e, 500)
    finally:
        db.session.close()


def insert_endowment_deliveries (data):
    try:

        query= """INSERT INTO endowment_deliveries 
            (candidate, delivery_date, delivered_by)
            VALUES 
            (:candidate, :delivery_date, :delivered_by)"""
            #delivery_signed
        params = {
            "candidate": data.get("candidate"),
            "delivery_date": data.get("delivery_date"),
            #"delivery_signed": data.get("delivery_signed"),
            "delivered_by": data.get("delivered_by"),
        }
        result = db.session.execute(query, params)
        lastInsert = result.lastrowid

        articles= data.get("articles")
        if  isinstance(articles, list) and len(articles) >0:
            thirdquery = """INSERT INTO endowment_dlv_details 
                (endowment_dlv, endowment_article)
                VALUES """
            first = True
            for art in articles:
                if first:
                    first = False
                else:
                    thirdquery += ", "

                thirdquery += "(" +\
                    xstr( lastInsert )      +"," +\
                    xstr( art.get("id") )   +\
                ")"
            third_result = db.session.execute(thirdquery)

        db.session.commit()
        return {"success":True, "id_inserted": lastInsert}, 200
    except Exception as e:
        return responseError(e, 500)
    finally:
        db.session.close()

def edit_endownment_deliveries(data):
    try:
        delId = data.get('id')
        if delId is None:
            return {"success":False, "message": "no hay ID para editar"}, 200

        query = """ UPDATE endowment_deliveries SET """
        
        params={
            'id': delId
        }

        setarraytemp = []

        if data.get('delivery_signed') is not None:
            setarraytemp.append("  delivery_signed = :delivery_signed ")
            params['delivery_signed']=data.get('delivery_signed')
        else:
            if data.get('signed_doc') is not None:
                signed_doc = data.get('signed_doc') 
                if len(signed_doc) >0:
                    setarraytemp.append("  delivery_signed = :delivery_signed ")
                    params['delivery_signed']=signed_doc[0].get("url")

        if len(setarraytemp) <= 0:
            return {"success":False, "message": "No hay info para actualizar, revisar data enviada"}, 200

        query = query + ",".join(setarraytemp) + " WHERE id = :id "

        result = db.session.execute(query, params)


        db.session.commit()
        return {"success":True, "result": str(delId)+" updated with file located at "+str(params.get('delivery_signed'))}, 200
    except Exception as e:
        return responseError(e, 500)
    finally:
        db.session.close()

def generar_acta_entrega_pdf(data): #(contract, signed, signature):
    try:
        if data.get("candidate", type=int) is None:
            return {"success": False, "message": "El atributo de candidato es obligatorio"}, 400

        query_user = """
        SELECT id, full_name, doc_number FROM candidates WHERE id = :id
        """
        result_user= db.session.execute(query_user, {"id": data.get("candidate", type=int)})
                    
        row = result_user.fetchone()
        today = datetime.date.today()
        myrow = {
            "{{ID_DOC}}": row.doc_number,
            "{{NAME}}": row.full_name,

            "{{YEAR}}": today.year,
            "{{MONTH}}": today.month,
            "{{DAY}}": today.day,
        }
                            

        info = """
                SELECT template_body 
                FROM contract_types
                where id = 7;
        """
        template = db.engine.execute(info).scalar()
        

        if template is not None:
            # Find all variables in template body
            # variables = re.findall('{{(.+?)}}', template)
            

            for key, value in myrow.items():
                template = template.replace(str(key), str(value))
            
                
            #render_template is used to generate output from a string that is passed in rather than from a file in the templates folder
            rendered = render_template_string(template)
            file = render_pdf(HTML(string=rendered))
            
            # convert file to base64
            data_file_convert = base64.b64encode(file.data)
            
            return json.loads('{"key":"%s"}' % (data_file_convert)), 200
        else:
            return {"success": False, "message": "No es posible generar un contrato en pdf, error en la plantilla."}, 400
    except Exception as e:
        return {"success": False, "message": "Error al generar pdf"}, 500


# update workAccident
def updateWorkAccident(data):
    try:
        id = data.get('id')
        title = data.get('title')
        description_event = data.get('description_event')
        accident_date = data.get('accident_date')
        employee = data.get('employee')
        laboral_inhability = data.get('laboral_inhability')
        inhability_doc = data.get('inhability_doc')
        description_injury = data.get('description_injury')
        furat = data.get('furat')
        other_docs = data.get('other_docs')
        entity_account = data.get('entity_account')
        created_by = data.get('created_by')
        job_position = data.get('job_position')
        result = data.get('result')
        result_file = data.get('result_file')
        reviewed_by = data.get('reviewed_by')
        reviewed_at = data.get('reviewed_at')
        status = data.get('status')        
        
        # validate id #️⃣ 
        if not id:
            return responseError("id: "+str(id), 400, "ID inválido")

        paramsArray = []
        stm = "UPDATE " + PAYROLL_DBNAME + ".work_accidents SET "

        if title:
            paramsArray.append("title = '"+ str(title)+ "'")
        if description_event:
            paramsArray.append("description_event = '"+str(description_event)+"'")
        if accident_date:
            paramsArray.append("accident_date = '"+str(accident_date)+"'")
        if employee:
            paramsArray.append("employee = '"+str(employee)+"'")
        if laboral_inhability:
            paramsArray.append("laboral_inhability = '"+str(laboral_inhability)+"'")
        if inhability_doc:
            paramsArray.append("inhability_doc = '"+str(inhability_doc)+"'")
        if description_injury:
            paramsArray.append("description_injury = '"+str(description_injury)+"'")
        if furat:
            paramsArray.append("furat = '"+str(furat)+"'")
        if other_docs:
            paramsArray.append("other_docs = '"+str(other_docs)+"'")
        if entity_account:
            paramsArray.append("entity_account = '"+str(entity_account)+"'")
        if created_by:
            paramsArray.append("created_by = '"+str(created_by)+"'")
        if job_position:
            paramsArray.append("job_position = '"+str(job_position)+"'")
        if result:
            paramsArray.append("result = '"+str(result)+"'")
        if result_file:
            paramsArray.append("result_file = '"+str(result_file)+"'")
        if reviewed_by:
            paramsArray.append("reviewed_by = '"+str(reviewed_by)+"'")
        if reviewed_at:
            paramsArray.append("reviewed_at = '"+str(reviewed_at)+"'")
        if status:
            paramsArray.append("status = '"+str(status)+"'")
        stm += ",".join(paramsArray)
        stm += " WHERE id = '"+str(id)+"'"
        db.session.execute(stm)
        db.session.commit()
        return responseSuccess(id, "resultado registrado", 1)

    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        return responseError(str(e), 500, "Ocurrió un error al actualizar la información" )
    finally:
        db.session.close()
# end update

def medical_exam_get(params):
    try:
        query = """
        SELECT 
            me.*,
            (
                SELECT JSON_ARRAYAGG(
                    JSON_OBJECT(
                        'id', med.id,
                        'employee', med.employee,
                        'result_location', med.result_location,
                        'result_loaded_at', med.result_loaded_at,
                        'status', med.status
                    )
                ) FROM medical_exam_details med
                WHERE med.medical_exam = me.id
            
            ) AS details
        FROM  medical_exams me
        WHERE 1=1 
        """

        querycount = "SELECT COUNT(1) FROM medical_exams me WHERE 1=1 "

        newparams = {}
        
        if params.get('title') and params.get('title')!="":
            query += " AND me.title LIKE '%{}%'".format(params.get('title'))
            querycount += " AND me.title LIKE '%{}%'".format(params.get('title'))
            newparams['title'] = params.get('title', type = str)

        if params.get('area_id') and params.get('area_id')!="":
            query += " AND me.area_id = :area_id "
            querycount += " AND me.area_id = :area_id "
            newparams['area_id'] = params.get('area_id', type = int)

        if params.get('year') and params.get('year')!="":
            query += " AND YEAR(me.prog_date) = :year "
            querycount += " AND YEAR(me.prog_date) = :year "
            newparams['year'] = params.get("year", type=int)

        if params.get('status'):
            if params.get('status', type=str) =="done":
                query += " AND done_date is not null "
                querycount += " AND done_date is not null "

        if params.get('from_date') and params.get('from_date')!="":
            fromDate = datetime.datetime.strptime( params.get('from_date', type=str) , "%Y-%m-%d")
            query += " AND prog_date >= :from_date "
            querycount += " AND prog_date >= :from_date "
            newparams['from_date'] = fromDate

        if params.get('to_date') and params.get('to_date')!="":
            ToDate = datetime.datetime.strptime( params.get('to_date', type=str) , "%Y-%m-%d")
            query += " AND prog_date <= :to_date "
            querycount += " AND prog_date <= :to_date "
            newparams['to_date'] = ToDate

        query += " GROUP BY me.prog_date, me.area_id"
        querycount += " GROUP BY me.prog_date, me.area_id"
        totalamount = db.session.execute(querycount, newparams).scalar()

        if params.get("page", type = int) is not None and params.get("perpage", type = int) is not None:
            query +=" LIMIT :perpage OFFSET :offset "
            newparams['offset'] = params.get("perpage", type = int) * (params.get("page", type = int) - 1)
            newparams['perpage'] = params.get('perpage', type = int)

        res = db.session.execute(query, newparams)

        finalresult = []
        for row in res.fetchall():
            dictrow = dict(row)
            myrow = {}
            for i in dictrow:
                if i == "details" and dictrow[i] is not None:
                    myrow[i]=json.loads( dictrow[i]) 
                else:
                    myrow[i]=json_serial(dictrow[i])
            finalresult.append(myrow)

        return {"results": finalresult, "total": totalamount}, 200
    except Exception as e:
        return responseError(e, 500)
    finally:
        db.session.close()

def medical_exam_area(params):
    try:
        if params.get("year", type=int) is None:
            return {"message": "Se requiere un año"}, 500

        newparams = {
            "year": params.get("year", type=int)
        }

        query = """
        SELECT 
        cc.id AS CC_ID,
        cc.description AS CC_DESC,
        me.*
        ,(
            SELECT JSON_ARRAYAGG(
                JSON_OBJECT(
                    'id', med.id,
                    'employee', med.employee,
                    'result_location', med.result_location,
                    'result_loaded_at', med.result_loaded_at,
                    'status', med.status
                )
            ) FROM medical_exam_details med
            WHERE med.medical_exam = me.id
        ) AS details
        ,(
            SELECT JSON_ARRAYAGG(
                JSON_OBJECT(
                        'candidate', candidate,
                        'job_position', job_position
                )
            ) FROM 
            (
                SELECT c.candidate, c.job_position
                FROM contracts c 
                JOIN job_positions jp ON jp.id=c.job_position  
                WHERE jp.area_id = cc.id
                GROUP BY c.candidate
            ) as subtable
        ) AS area_candidates
        FROM  `ttn-inventory-core-dev`.consumption_center cc
        LEFT JOIN (SELECT * FROM medical_exams WHERE YEAR(prog_date) = :year) me ON cc.id = me.area_id
        WHERE 1=1
        """
        
        if params.get('title'):
            query += " AND me.title LIKE '%{}%'".format(params.get('title'))
            newparams['title'] = params.get('title', type = str)

        if params.get('area_id'):
            query += " AND me.area_id = :area_id"
            newparams['area_id'] = params.get('area_id', type = int)

        if params.get("page", type = int) is not None and params.get("perpage", type = int) is not None:
            query +=" LIMIT :perpage OFFSET :offset "
            newparams['offset'] = params.get("perpage", type = int) * (params.get("page", type = int) - 1)
            newparams['perpage'] = params.get('perpage', type = int)

        query += " GROUP BY cc.id ORDER BY me.prog_date desc"
        res = db.session.execute(query, newparams)

        finalresult = []
        for row in res.fetchall():
            dictrow = dict(row)
            myrow = {}
            for i in dictrow:
                if (i == "details" or i == "area_candidates") and dictrow[i] is not None:
                    myrow[i]=json.loads( dictrow[i]) 
                else:
                    myrow[i]=json_serial(dictrow[i])
            finalresult.append(myrow)

        return {"results": finalresult}, 200
    except Exception as e:
        return responseError(e, 500)
    finally:
        db.session.close()

def medical_exam_put(data):
    try:
        ID = data.get('id')
        USER_IDS = data.get('userids')

        query = """ UPDATE medical_exams SET """
        
        params={
            'id': ID
        }

        setarraytemp = []

        if data.get('title') is not None:
            setarraytemp.append("  title = :title ")
            params['title']=data.get('title')
        
        if data.get('area_id') is not None:
            setarraytemp.append("  area_id = :area_id ")
            params['area_id']=data.get('area_id')
        
        if data.get('area_name') is not None:
            setarraytemp.append("  area_name = :area_name ")
            params['area_name']=data.get('area_name')


        if data.get('prog_date') is not None:
            setarraytemp.append("  prog_date = :prog_date ")
            params['prog_date']=data.get('prog_date_new') 
        
        if data.get('done_date') is not None:
            setarraytemp.append("  done_date = :done_date ")
            params['done_date']=data.get('done_date')

        
        if data.get('requested') is not None:
            setarraytemp.append("  requested = :requested ")
            params['requested']=data.get('requested')

        
        if data.get('cancelled_by') is not None:
            setarraytemp.append("  cancelled_by = :cancelled_by ")
            params['cancelled_by']=data.get('cancelled_by')

        if data.get('cancelled_at') is not None:
            setarraytemp.append("  cancelled_at = :cancelled_at ")
            params['cancelled_at']=data.get('cancelled_at')

        query = query + ",".join(setarraytemp) + " WHERE id = :id "
        result = db.session.execute(query, params)



        if  isinstance(USER_IDS, list):

            params2 = {
                'id': ID,
                'userids': USER_IDS
            }


            query2 = """ 
            UPDATE medical_exam_details 
            SET status = 'canceled' 
            WHERE medical_exam = :id 
            AND status != 'done' 
            AND employee not in :userids
            """
            result2 = db.session.execute(query2, params2)


            # query3 = """ 
            # UPDATE medical_exam_details 
            # SET status = 'pending' 
            # WHERE medical_exam = :id 
            # AND status != 'done' 
            # AND employee in :userids
            # """
            # result3 = db.session.execute(query3, params2)

            for userid in USER_IDS:
                damn_query = """
                SELECT status FROM medical_exam_details 
                WHERE medical_exam = :id 
                AND employee = """+str(userid)
                #AND status != 'done' 
                damn_result= db.session.execute(damn_query, params2).scalar()
                #print(damn_result, flush=True)

                if damn_result is None:
                    damn_query2 = """
                    INSERT INTO medical_exam_details 
                    (status, medical_exam, employee) 
                    VALUES 
                    ('pending' , :id, """+str(userid)+""")"""
                    #print(damn_query2, flush=True)
                    damn_result2= db.session.execute(damn_query2, params2)
                else:
                    if damn_result != "done":
                        damn_query2 = """
                        UPDATE medical_exam_details 
                        SET status = 'pending' 
                        WHERE medical_exam = :id 
                        AND employee ="""+str(userid)
                        #print(damn_query2, flush=True)
                        damn_result2= db.session.execute(damn_query2, params2)
                    #else:
                        #print("is done, wont update", flush=True)

        db.session.commit()
        return {"success": True}, 200
    except Exception as e:
        return responseError(e, 500)
    finally:
        db.session.close()

def medical_exam_post(data):
    try:

        getquery = """ SELECT count(1) FROM  medical_exams WHERE id = :id"""
        ID = data.get('id')
        params={
            'id': data.get('id'),
            'title': data.get('title'),
            'area_id': data.get('area_id'),
            'area_name': data.get('area_name'),
            'prog_date': data.get('prog_date_new'),
            
            'created_by': data.get('created_by'),
            'entity_account': data.get('entity_account')
        }
        getresult = db.session.execute(getquery, params).scalar()

        if getresult:
            query = """ 
            UPDATE medical_exams 
            SET 
            prog_date = :prog_date
            WHERE id = :id
            """
            # title = :title
            # area_id = :area_id
            # area_name = :area_name
            
            result = db.session.execute(query, params)

        else:
            query = """ INSERT INTO medical_exams (
                title,
                area_id,
                area_name,
                prog_date,
                created_by,
                entity_account
            ) VALUES (
                :title,
                :area_id,
                :area_name,
                :prog_date,
                :created_by,
                :entity_account
            )"""
            result = db.session.execute(query, params)
            ID = result.lastrowid




        query4 = """
            SELECT c.id, c.full_name, jp.area_id
            FROM candidates c
            JOIN contracts con ON con.candidate = c.id
            JOIN job_positions jp ON jp.id = con.job_position
            WHERE jp.area_id = :area_id 
            GROUP BY c.id
        """
        params4 = {
            "area_id": data.get('area_id')
        }
        res4 = db.session.execute(query4, params4)

        queryDetail = """ INSERT INTO medical_exam_details (
            medical_exam,
            employee,
            status
        ) VALUES """

        first = True
        for row4 in res4.fetchall():

            if first:
                first = False
            else:
                queryDetail += ", "

            queryDetail += "(" +\
                str( ID )           +"," +\
                str( row4.id )      +"," +\
               "'pending'"    +\
            ")"

        if first == False:
            resultDetail = db.session.execute(queryDetail)



        db.session.commit()
        return {'success': True, 'results': "description", 'message':"Fecha seteada, inserte los detalles a todos los candidatos"}
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        db.session.rollback()
        return responseError(e, 500)
    finally:
        db.session.close()
    return {"success":False, "message":"To Do"}, 400

def medical_exam_candidates(params):
    try:
        query = """
        SELECT c.id, c.full_name, c.occupation, c.s3_photo, c.doc_number,
        med.employee, med.result_location, med.result_loaded_at, med.status
        FROM candidates c
        JOIN contracts con ON con.candidate = c.id
        JOIN job_positions jp ON jp.id=con.job_position
        LEFT JOIN medical_exam_details med ON c.id = med.employee
        WHERE 1=1
        """
        newparams = { }

        if  params.get("me_id", type=int):
            query += " AND med.medical_exam = :me_id  "
            newparams["me_id"]=params.get("me_id", type=int)

        if  params.get("area_id", type=int):
            query += " AND jp.area_id = :area_id  "
            newparams["area_id"]=params.get("area_id", type=int)

        
        if params.get('search') and params.get('search')!="":
            query += " AND c.full_name LIKE '%{}%'".format(params.get('search'))
            newparams['title'] = params.get('search', type = str)

        query += " GROUP BY c.id"
        querycount = "SELECT COUNT(1) FROM (" + query + ") as subtable"
        rescount = db.session.execute(querycount, newparams).scalar()

        if params.get("page", type = int) is not None and params.get("perpage", type = int) is not None:
            query +=" LIMIT :perpage OFFSET :offset "
            newparams['offset'] = params.get("perpage", type = int) * (params.get("page", type = int) - 1)
            newparams['perpage'] = params.get('perpage', type = int)

        res = db.session.execute(query, newparams)

        finalresult = []
        for row in res.fetchall():
            dictrow = dict(row)
            myrow = {}
            for i in dictrow:
                    myrow[i]=json_serial(dictrow[i])
            finalresult.append(myrow)

        return {"results": finalresult, "total":rescount}, 200
    except Exception as e:
        return responseError(e, 500)
    finally:
        db.session.close()

def get_candidates_in_list(params):
    try:
        newparams = {}
        query = """
        SELECT c.id, c.full_name, c.occupation, c.s3_photo, 
        med.employee, med.result_location, med.result_loaded_at, med.status
        FROM candidates c
        LEFT JOIN medical_exam_details med ON c.id = med.employee
        WHERE 1=1
        """
        
        if params.get('userids', type=str):
            userids_list= params.get('userids', type=str).split(",")
            query += " AND c.id IN :userids_list "
            newparams['userids_list'] = userids_list

        query += " GROUP BY c.id"
        res = db.session.execute(query, newparams)

        finalresult = []
        for row in res.fetchall():
            dictrow = dict(row)
            myrow = {}
            for i in dictrow:
                    myrow[i]=json_serial(dictrow[i])
            finalresult.append(myrow)

        return {"results": finalresult}, 200
    except Exception as e:
        return responseError(e, 500)
    finally:
        db.session.close()

#..............................................seccion calendario ....................................
def last_day_of_month(any_day):
    # this will never fail
    # get close to the end of the month for any day, and add 4 days 'over'
    next_month = any_day.replace(day=28) + datetime.timedelta(days=4)
    # subtract the number of remaining 'overage' days to get last day of current month, or said programattically said, the previous day of the first of next month
    return next_month - datetime.timedelta(days=next_month.day)

def getCalendar(year, _month):
    month = "0"+str(_month) if len(str(_month))<2  else str(_month)

    date = datetime.datetime.strptime(str(year)+"-"+month+"-01", "%Y-%m-%d")
    end_date = last_day_of_month(date)
    firstDow = (date.weekday() +1) %7
    #in python 0=monday
    dates = []

    for i in range(firstDow):
        dates.append({
            "date": "",
            "day": "",
            "dow": i
        })
    

    for j in range(end_date.day):
        myday = "0"+str(j+1) if len(str(j+1))<2  else str(j+1)
        #mydate = datetime.datetime.strptime(str(year)+"-"+month+"-"+myday, "%Y-%m-%d")
        dates.append({
            "date": str(year)+"-"+month+"-"+myday,
            "day": j+1,
            "dow": (firstDow+j) % 7
        })
    return dates
    

def medical_exam_calendar(args):

    if args.get("year", int) is None or args.get("month", int) is None:
        return {"success":False, "message":"Se requiere el año y el mes"}, 400

    query = "SELECT *, DAY(prog_date) AS day FROM medical_exams WHERE YEAR(prog_date) = :year AND MONTH(prog_date) = :month"
    params = {
        "year": args.get("year", int),
        "month": args.get("month", int)
    }
    result = db.session.execute(query, params)



    finalresult = []
    for row in result.fetchall():
        dictrow = dict(row)
        myrow = {}
        for i in dictrow:
            myrow[i]=json_serial(dictrow[i])
        finalresult.append(myrow)

    fullmonth= getCalendar(args.get("year", int), args.get("month", int))

    for fd in fullmonth:
        matches = [me for me in finalresult if me.get('day') == fd.get('day')]
        fd["data"] = matches

    
    return {"success":True, "message":finalresult, "fullmonth": fullmonth}, 200

#..............................................seccion calendario FIN....................................

# * get annotations * #
def getAnnotations(params):
    try:
        stm = """ SELECT * FROM """ + PAYROLL_DBNAME +""". develmot_annotations WHERE 1=1 """ 
        id = params.get('id')
        if id:
            stm += "AND id = :id"
        employee = params.get('employee')
        if employee:
            stm += "AND employee = :employee"
        title = params.get('title')
        if title:
            stm += "AND title = :title"
        ann_date = params.get('ann_date')
        if ann_date:
            stm += "AND ann_date = :ann_date"
        description = params.get('description')
        if description:
            stm += "AND description = :description"
        created_by = params.get('created_by')
        if created_by:
            stm += "AND created_by = :created_by"
        created_by_name = params.get('created_by_name')
        if created_by_name:
            stm += "AND created_by_name = :created_by_name"
        created_at = params.get('created_at')
        if created_at:
            stm += "AND created_at = :created_at"
        entity_account = params.get('entity_account')
        if entity_account:
            stm += "AND entity_account = :entity_account"
        stm += " ORDER BY id DESC "
        res = db.session.execute(stm, params)
        return responseSuccess([dict(({
            'id_annotation':row.id,
            'employee':row.employee,
            'title':row.title,
            'ann_date':json_serial(row.ann_date),
            'description':row.description,
            'created_by':row.created_by,
            'created_by_name':row.created_by_name,
            'created_at':json_serial(row.created_at),
            'entity_account':row.entity_account,
        })) for row in res.fetchall()], "OK", None)
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        return responseError(str(e), 500, "Ocurrió un error al traer las anotaciones")
    finally:
        db.session.close()

def createAnnotation(data):
    try:
        employee = data.get('employee')
        title = data.get('title')
        ann_date = data.get('ann_date')
        description = data.get('description')
        created_by = data.get('created_by')
        created_by_name = data.get('created_by_name')
        entity_account = data.get('entity_account')
        if not employee:
            return responseError(str(employee), 400, "El campo employee es obligatorio")
        if not title:
            return responseError(str(title), 400, "El campo title es obligatorio")
        if not ann_date:
            return responseError(str(ann_date), 400, "El campo ann_date es obligatorio")
        if not description:
            return responseError(str(description), 400, "El campo description es obligatorio")
        if not created_by:
            return responseError(str(created_by), 400, "El campo created_by es obligatorio")
        if not created_by_name:
            return responseError(str(created_by_name), 400, "El campo created_by_name es obligatorio")        
        if not entity_account:
            return responseError(str(entity_account), 400, "El campo entity_account es obligatorio")
        stm = """
            INSERT INTO `mod-payroll-core-dev`.develmot_annotations VALUES
                (
                    null,
                    :employee,
                    :title,
                    :ann_date,
                    :description,
                    :created_by,
                    :created_by_name,
                    NOW(),
                    :entity_account
                );
        """
        db.session.execute(stm, data)
        db.session.commit()
        return responseSuccess(title, "Anotación registrada", 1)
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        return responseError(str(e), 500, "Ocurrió un error al guardar la anotación")
    finally:
        db.session.close()
# * end annotations * #


# * get details for disciplinary-processes * #
def getDisciplinaryProcessesDetails(id):
    try:
        stm = """
            SELECT dp.*,
            (CASE
                WHEN dp.last_action="citation" THEN "Citación"
                WHEN dp.last_action="minute" THEN "Minuta"
                WHEN dp.last_action="memorandum" THEN "Memorando"
                WHEN dp.last_action="suspension" THEN "Suspensión"    
                WHEN dp.last_action="termination" THEN "Terminación"
                ELSE dp.last_action
                END
            ) as lastActionValue,
            can.full_name as reporter,
            st.value as statusValue,
            sc.background as statusBackground,
            sc.fontcolor as statusColor,
            can2.full_name as involvedName,
            com.area_id,
            com.area_name,
            jp.job_title,
            jp.job_description,
            jp.job_title,
            jp.job_description,
            disciplinary_process_detail
            FROM """+ PAYROLL_DBNAME +""".disciplinary_processes dp
            INNER JOIN status st on st.name = dp.status and PREFIX = 'disciplinary_process'
            INNER JOIN status_color sc on sc.id = st.color_id
            INNER JOIN complaints com on com.id = dp.complaint
            INNER JOIN candidates can on can.id = com.reporter
            INNER JOIN candidates can2 on can2.id = dp.involved
            INNER JOIN contracts co on co.candidate = can.id and co.status = 'current'
            INNER JOIN job_positions jp on jp.id = co.job_position
            LEFT JOIN (SELECT disciplinary_proc proc,
                json_arrayagg(
                    json_object(
                        "detail_id", dpd.id,
                        "title", dpd.title,
                        "date_detail", DATE_FORMAT (dpd.created_at, "%d/%m/%Y"),
                        "created_by", dpd.created_by_name)
                        ) AS disciplinary_process_detail
                    FROM disciplinary_proc_details dpd
                    GROUP BY disciplinary_proc) dpd ON (dpd.proc = dp.id)
            WHERE 1=1 AND dp.id = :id
            GROUP BY dp.id;
        """
        
        response = db.session.execute(stm, {'id':id})
        res = response.fetchone()
        dictrow = dict(res)
        myrow = {}
        for i in dictrow:
            if i != "disciplinary_process_detail":
                myrow[i]=json_serial(dictrow[i])
            else:
                if dictrow[i] is not None:
                    myrow[i]=json.loads( dictrow[i])
        return responseSuccess(myrow, "OK", 1)
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        return responseError(str(e), 500, "Ocurrió un error al traer la información del proceso disciplinario")
    finally:
        db.session.close()
# * end get details for disciplinary-processes * #

# * get getDisciplinaryProcesses * #
def getDisciplinaryProcesses(params):
    try:
        if not params.get('entity_account'):
            return responseError("entity_account: "+str(params.get('entity_account')), 400, "El parámetro entity_account es obligatorio")
        stm = """ 
            SELECT dp.*,
            (CASE
                WHEN dp.last_action="citation" THEN "Citación"
                WHEN dp.last_action="minute" THEN "Minuta"
                WHEN dp.last_action="memorandum" THEN "Memorando"
                WHEN dp.last_action="suspension" THEN "Suspensión"    
                WHEN dp.last_action="termination" THEN "Terminación"
                ELSE dp.last_action
                END
            ) as lastActionValue,
            can.full_name as reporter,
            st.value as statusValue,
            sc.background as statusBackground,
            sc.fontcolor as statusColor,
            can2.full_name as involved,
            com.area_id,
            com.area_name,
            dpd.type_date AS type_date,
            com.title,
            com.description,
            if (rcs.id is not null,
                    json_arrayagg(json_object('id', rcs.id, 'support_file', rcs.support_file)),
                null
            ) as support_files,
            contr.job_title
            FROM """ + PAYROLL_DBNAME +""".disciplinary_processes dp
            INNER JOIN status st on st.name = dp.status and PREFIX = 'disciplinary_process'
            INNER JOIN status_color sc on sc.id = st.color_id
            INNER JOIN complaints com on com.id = dp.complaint
            INNER JOIN candidates can on can.id = com.reporter
            INNER JOIN candidates can2 on can2.id = dp.involved
            LEFT JOIN disciplinary_proc_details dpd on dpd.disciplinary_proc = dp.id
            INNER JOIN (SELECT cont.candidate,TRIM(GROUP_CONCAT(" ",jp.job_title)) job_title FROM contracts cont
            INNER JOIN job_positions jp on jp.id = cont.job_position
                WHERE cont.status = 'current'
                GROUP BY cont.candidate) contr on contr.candidate = can.id
            LEFT JOIN rel_complaint_support rcs on rcs.complaint = com.id
            WHERE 1=1
        """        
        paramsArray = {
            'id' : params.get('id')                ,
            'complaint_type' : params.get('complaint_type'),
            'area_id' : params.get('area_id'),
            'area_name' : params.get('area_name'),
            'reporter' : params.get('reporter'),
            'report_date' : params.get('report_date'),
            'title' : params.get('title'),
            'description' : params.get('description'),
            'status' : params.get('status'),
            'created_at' : params.get('created_at'),
            'created_by' : params.get('created_by'),
            'result_type' : params.get('result_type'),
            'result_justification' : params.get('result_justification'),
            'result_by' : params.get('result_by'),
            'result_at' : params.get('result_at'),
            'entity_account' : params.get('entity_account'),
            'involved' : params.get('involved'),
        }
        search = params.get('search')
        if paramsArray.get('id'):
            return getDisciplinaryProcessesDetails(paramsArray.get('id'))
        if paramsArray.get('complaint_type'):
            stm += " AND `com.type` = :type"
        if paramsArray.get('area_id'):
            stm += " AND com.area_id = :area_id"
        if paramsArray.get('area_name'):
            stm += " AND com.area_name = :area_name"
        if paramsArray.get('reporter'):
            stm += " AND com.reporter = :reporter"
        if paramsArray.get('report_date'):
            stm += " AND com.report_date = :report_date"
        if paramsArray.get('title'):
            stm += " AND com.title = :title"
        if paramsArray.get('description'):
            stm += " AND com.description = :description"
        if paramsArray.get('status'):
            stm += " AND dp.status = :status"
        if paramsArray.get('created_at'):
            stm += " AND com.created_at = :created_at"
        if paramsArray.get('created_by'):
            stm += " AND com.created_by = :created_by"
        if paramsArray.get('result_type'):
            stm += " AND com.result_type = :result_type"
        if paramsArray.get('result_justification'):
            stm += " AND com.result_justification = :result_justification"
        if paramsArray.get('result_by'):
            stm += " AND com.result_by = :result_by"
        if paramsArray.get('result_at'):
            stm += " AND com.result_at = :result_at"
        if paramsArray.get('entity_account'):
            stm += " AND com.entity_account = :entity_account"
        if paramsArray.get('involved'):
            stm += " AND dp.involved = :involved"
        if search:
            stm += " AND (can.full_name LIKE '%%" + search + "%%' OR can2.full_name LIKE '%%"+ search +"%%')"
        stm += " GROUP BY dp.id "
        rowTotal = db.session.execute(stm, paramsArray).rowcount
        if params.get("page") is not None and params.get("perpage") is not None:
            offset = int(params.get("perpage")) * (int(params.get("page")) - 1)
            stm +=" LIMIT " + params.get('perpage') + " OFFSET " + str(offset)
        
        res = db.session.execute(stm, paramsArray)
        result = []
        for row in res.fetchall():
            dictrow = dict(row)
            myrow = {}
            for i in dictrow:
                if i != "support_files":
                    myrow[i]=json_serial(dictrow[i])
                elif dictrow[i] is not None:                    
                    myrow[i]=json.loads( dictrow[i])
                else:
                    myrow[i]=None
            result.append(myrow)    
        return responseSuccess(result, "OK", rowTotal)
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        return responseError(str(e), 500, "Ocurrió un error al traer el listado de procesos disciplinarios")
    finally:
        db.session.close()
# * end getDisciplinaryProcesses * #
        
    

def create_work_certificates(data):
    try:
        tableTemplate = "work_certif_template"
        table = "work_certificates"
        query = """ INSERT INTO `mod-payroll-core-dev`.work_certificates
        (employee, job_position, exp_date, addressed_to, include_salary, include_notconst,include_average, 
        certificate_file, entity_account, created_by, created_at , contract)
        VALUE(:employee,:job_position,:exp_date,:addressed_to,:include_salary,:include_notconst,:include_average,
        :certificate_file,:entity_account,:created_by,:created_at , :contract) """
        params = {
            "employee" : data.get('employee'),
            "job_position" : data.get('job_position'),
            "exp_date" : data.get('exp_date'),
            "contract" : data.get('contract'),
            "addressed_to" : data.get('addressed_to'),
            "include_salary" : data.get('include_salary'),
            "include_notconst" : data.get('include_notconst'),
            "include_average" : data.get('include_average'),
            "certificate_file" : data.get('certificate_file'),
            "entity_account" : data.get('entity_account'),
            "created_by" : data.get('created_by'),
            "created_at" : datetime.datetime.utcnow(),
        }
        result = db.session.execute(query, params)
        db.session.commit()
        result.close()
        lastInsert = result.lastrowid    
        pdf = pdf_generation(lastInsert, tableTemplate , table)
        return {'success': True, 'message': "Carta laboral creada exitosamente", 'id': lastInsert, "pdf_work_certificate": pdf},200
    except Exception as e:
        return {'success': False, 'message': "Error al crear certificado laboral", 'Error' : str(e) },400
def update_work_certificate(id, data):
    try:
        employee = data.get("employee")
        job_position = data.get("job_position")
        exp_date = data.get("exp_date")
        addressed_to = data.get("addressed_to")
        include_salary = data.get("include_salary")
        include_notconst = data.get("include_notconst")
        include_average = data.get("include_average")
        certificate_file = data.get("certificate_file")
        if not id:
            return responseError("id: "+str(id), 400, "ID inválido")
        paramsArray = []
        query = "UPDATE "+PAYROLL_DBNAME+".work_certificates SET "
        if employee:
            paramsArray.append("employee = '"+ str(employee)+ "'")
        if job_position:
            paramsArray.append("job_position = '"+ str(job_position) + "'")
        if exp_date:
            paramsArray.append("exp_date = '"+ str(exp_date) + "'")
        if addressed_to:
            paramsArray.append("addressed_to = '"+ str(addressed_to) + "'")
        if include_salary:
            paramsArray.append("include_salary = '"+ str(include_salary) + "'")
        if include_notconst:
            paramsArray.append("include_notconst = '"+ str(include_notconst) + "'")
        if include_average:
            paramsArray.append("include_average = '"+ str(include_average) + "'")
        if certificate_file:
            paramsArray.append("certificate_file = '"+ str(certificate_file) + "'")
        query += ",".join(paramsArray)
        query += " WHERE id ={}".format(id)
        db.session.execute(query)
        db.session.commit()
        return {'success': True, 'message': "Carta laboral actualizada exitosamente", "id" : id , "pdf" : certificate_file},200
    except Exception as e:
        return {'success': False, 'message': "Error al crear certificado laboral", 'Error' : str(e) },400
def pdf_generation(id, tableTemplate, table):   #id (the register id ), table (The table which contains the register) , 
    try:                                        #tableTemplate (Table that contain the document template which we will to generate)
        tableTemplateVars = "work_certif_template_vars" 
        param_name = "workcert_param"
        if not id:
            return {"success": False, "message": "El atributo de contrato es campo obligatorio"}, 400
        # contract information 
        info = """ SELECT a.template_body FROM `mod-payroll-core-dev`."""+tableTemplate+""" as a ;"""
        sql = db.engine.execute(info)
        cont_row = sql.rowcount
        old = sql.fetchone()
        if cont_row != 0:
            if old["template_body"] is not None:
                # Find all variables in template body
                variables = re.findall('{{(.+?)}}', old["template_body"])
                template = old["template_body"]
                if len(variables) != 0:
                    # get and replace variables 
                    data = replace_variables_template(variables,id,table, tableTemplateVars,param_name)
                    if data[0]["success"]:
                        # update values of variables
                        data = data[0]["variables"]
                        for key, value in data.items():
                            template = template.replace(str(key), str(value))
                    else:
                        {"success": False, "message": "No es posible generar un contrato en pdf, error en la plantilla."}, 400
                #render_template is used to generate output from a string that is passed in rather than from a file in the templates folder
                rendered = render_template_string(template)
                file = render_pdf(HTML(string=rendered))
                # convert file to base64
                data_file_convert = base64.b64encode(file.data)
                return json.loads('{"key":"%s"}' % (data_file_convert)), 200
            else:
                {"success": False, "message": "No es posible generar un contrato en pdf, error en la plantilla."}, 400
        else:
            return {"success": False, "message": "No es posible generar un contrato en pdf, no existe una plantilla para este tipo de contrato"}, 400
    except Exception as e:
        return {"success": False, "message":"Error al generar el pdf", "error": str(e)},400

def replace_variables_template(variables, id, table, tableTemplateVars, param_name): #id (the register id), table (table that contain the register) 
    try:                                                                             #TableTemplateVars(table that contains the template variables)
        if len(variables):                                                           #param_name (column name in template vars table that contain the name of the variable) 
            var_values = {}                                                               
            register_information = """
                SELECT * FROM `mod-payroll-core-dev`."""+table+""" WHERE id = {}
            """.format(id)
            sql = db.engine.execute(register_information)
            register_info = sql.fetchone()
            
            if register_info["id"] is not None:
                for v in variables:
                    find_var = """ 
                        SELECT * FROM `mod-payroll-core-dev`."""+tableTemplateVars+""" where var_name = '{0}';
                    """.format(v)
                    sql = db.engine.execute(find_var)
                    row = sql.fetchone()
                    if row["id"] is not None:
                        
                        if row[param_name] is not None:
                            # attribute in contract
                            contract_attr = register_info[row[param_name]]
                        
                        if row["fixed_value"] is not None:
                            var_values['{{'+row["var_name"]+'}}'] = str(row["fixed_value"])
                        
                        if row["fixed_value"] is None and row["SELECT_field"] is not None and row["INNER_joins"] is None and row["location_table"] is not None and row["location_field"] is not None:
                            # query 
                            exec_query = 'SELECT ' + row["SELECT_field"] + ' FROM ' + row["location_table"] + ' WHERE id = '+ str(contract_attr)+';'
                            sql = db.engine.execute(exec_query)
                            query = sql.fetchone()
                            
                            if query[row["location_field"]] is not None:
                                var_values['{{'+row["var_name"]+'}}'] = str(query[row["location_field"]])
                        
                        if row["fixed_value"] is None and row["SELECT_field"] is not None and row["INNER_joins"] is not None and row["location_table"] is not None and row["location_field"] is not None:
                            # query
                            exec_query = 'SELECT ' + row["SELECT_field"] + ' FROM ' + row["location_table"] + ' ' + row["INNER_joins"] + ' WHERE '+ row["location_table"]+'.'+row[param_name]+' = '+ str(contract_attr)+';'
                            sql = db.engine.execute(exec_query)
                            query = sql.fetchone()
                            
                            if query[row["location_field"]] is not None:
                                var_values['{{'+row["var_name"]+'}}'] = str(query[row["location_field"]])    
                    
                return {"success": True, "variables": var_values}, 200
                        
            else:
                return {"success": False, "message": "No es posible generar el pdf, error en la plantilla."}, 400        
        else:
            return {"success": False, "message": "No es posible generar el pdf, error en la plantilla."}, 400
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        return {"success": False, "message":"Error al reemplazar las variables en el pdf", "error": str(e)},400

# Create a relations bettwen candidate and user
def create_rel_user_candidate(data):
    try:
        doc_num = data.get("user_doc_num")
        doc_type = data.get("user_doc_type")
        id_user = data.get("user_id")
        
        
        validate_candidate = """
            SELECT cdt.id as candidate_id FROM `"""+ DATABASE_NAME +"""`.candidates cdt
            INNER JOIN `"""+ DATABASE_NAME +"""`.contracts cts
            ON cdt.id = cts.candidate
            INNER JOIN `"""+ DATABASE_NAME +"""`.document_types dc
            ON dc.id = cdt.doc_type
            WHERE cdt.doc_number = "{0}" and dc.prefix = "{1}"
            GROUP BY cdt.id
            LIMIT 1;
        """.format(doc_num, doc_type)
        sql = db.engine.execute(validate_candidate)
        cont_row = sql.rowcount
        query = sql.fetchone()
        
        if cont_row != 0:
            result = dict(query)
            
            create_relation = """
                INSERT INTO `"""+ DATABASE_NAME +"""`.`rel_users_candidate`
                (`id_candidate`,`id_user`)
                VALUES(:id_candidate, :id_user);
            """
            
            params = {
                "id_candidate": result.get("candidate_id"),
                "id_user": id_user
            }
            
            result_m = db.session.execute(create_relation, params)
            db.session.commit()
            lastInsert = result_m.lastrowid
            
            return {"success": True, "rel": str(lastInsert),"message": "Este usuario fue relacionado al candidato con el mismo número de documento."}, 200
        else:
            return {"success": False, "rel":"", "message": "No existe candidato con este número de documento"}, 200
        
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        db.session.rollback()
        return {'success': False, 'message': e },400
    finally:
        db.session.close()

def create_complaint(data):
    try:
        query = """ INSERT INTO `"""+ DATABASE_NAME +"""`.complaints
        (type, area_id, area_name, reporter, report_date, title, description, entity_account, created_by, created_at, status)
        VALUE(:type, :area_id, :area_name, :reporter, :report_date, :title, :description, :entity_account,  :created_by, :created_at, :status)"""
        params = {
            "type" : data.get('type'),
            "area_id" : data.get('area_id'),
            "area_name" : data.get('area_name'),
            "reporter" : data.get('reporter'),
            "report_date" : data.get('report_date'),
            "title" : data.get('title'),
            "description" : data.get('description'),
            "entity_account" : data.get('entity_account'),
            "created_by" : data.get('created_by'),
            "created_at" : datetime.datetime.utcnow(),
            "status" : data.get('status'),
        }
        result = db.session.execute(query, params)
        db.session.commit()
        result.close()
        lastInsert = result.lastrowid
        return {'success': True, 'message': "Queja creada exitosamente", 'id': lastInsert},200
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        return {'success': False, 'message': e },400
    
def insert_rel_complaint_support(data):
    try:
        query = """INSERT INTO `"""+ DATABASE_NAME +"""`.rel_complaint_support
        (complaint, support_file)
        VALUE (:complaint, :support_file)"""
        params = {
            'complaint': data.get('complaint'),
            'support_file': data.get('url')
            }
        result = db.session.execute(query, params)
        db.session.commit()
        result.close()
        return {"success": True, "message": "se ha insertado la relación de empleado con la queja"},200
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        return {'success': False, 'message': e },400
    
def insert_rel_complaint_employee(data):
    try:
        query = """INSERT INTO `"""+ DATABASE_NAME +"""`.rel_complaint_employee
        (complaint, employee)
        VALUE (:complaint, :employee)"""
        params = {
            'complaint': data.get('complaint'),
            'employee': data.get('employee')
            }
        result = db.session.execute(query, params)
        db.session.commit()
        result.close()
        return {"success": True, "message": "se ha insertado la relación de empleado con la queja"},200
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        return {'success': False, 'message': e },400
    
def register_disciplinary_processes(data):
    try:
        id_complaint = data.get('id_complaint')
        employees = data.get('employees_improvement')
        for e in employees:
            temp = {
                'complaint': id_complaint,
                'employee': e
            }
            complaint_emp_res = insert_rel_complaint_employee(temp)
            if complaint_emp_res[0].get('success') == False:
                return complaint_emp_res[0],400
            
        files = data.get('urls')
        for f in files:
            temp1 = {
                'complaint': id_complaint,
                'url' : f.get('url')
            }
            complaint_supp_res =insert_rel_complaint_support(temp1)
            if complaint_supp_res[0].get('success') == False:
                return complaint_supp_res[0],400
        
        data_disciplinary ={
            'implicated_employees' : employees,
            'complaint' : id_complaint,
        }
        disciplinary_process_res = create_disciplinary_processes(data_disciplinary)
        return disciplinary_process_res
        
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        return {'success': False, 'message': e },400

#### OVERTIME SECTION ################################################################
def get_overtime (args):
    account = args.get("entity_account")
    page = args.get("page")
    perpage = args.get("perpage")
    status= args.get("status")
    search= args.get("search")
    dateFrom= args.get("dateFrom")
    dateUntil= args.get("dateUntil")
    area= args.get("area")
    id= args.get("id")
    user= args.get ("user")

    if not account:
        return {"success": False, "message": "El atributo cuenta en sesión es obligatorio"}, 400
    
    if not id:

        try:
            query = """ 
                    SELECT `o`.`id` AS `id`,`c`.`full_name` AS `collaborator`,`j`.`job_title` AS `job_title`,`cc`.`site_name` AS `area`, sum(`od`.`hours_counted`) AS `hours`
                    ,DATE_FORMAT (`o`.`start_time`, '%%d/%%m/%%Y') AS `dateStart`, `o`.`start_time`  AS `dateStartTime`,  DATE_FORMAT (`o`.`end_time`, '%%d/%%m/%%Y') AS `dateEnd`
                    , `o`.`status` AS `status`, `o`.`end_time`  AS `dateEndTime`
                    FROM `"""+ DATABASE_NAME +"""`.overtime as o
                    JOIN `"""+ DATABASE_NAME +"""`.candidates as c  ON o.employee = c.id
                    JOIN `"""+ DATABASE_NAME +"""`.job_positions as j ON j.id = o.job_position
                    JOIN `"""+ DATABASE_NAME +"""`.`contracts` as cc ON cc.id = o.contract
                    JOIN `"""+ DATABASE_NAME +"""`.`overtime_details` as od ON od.overtime = o.id
                    WHERE o.entity_account =""" + account + """ 
                """
            if status:
                query = query + " AND o.status = '" + str(status) +"'"
            
            if area:
                query = query + " AND cc.site_id = '" + str(area) +"'"
            
            if dateFrom:
                query = query +  " AND DATE(o.start_time) >= '" + str(dateFrom)+"'"
            
            if dateUntil:
                query = query +  " AND DATE(o.end_time) <= '" + str(dateUntil)+"'"
            
            if user:
                queryuser ="""
                Select 
                u.id_candidate as c_id
                from rel_users_candidate u 
                WHERE u.id_user = """ + user + """ 
                """
                userresult = db.session.execute(queryuser, {"user": user}).fetchone()

                if userresult is None:
                    return {"success": False, "message":"No se encontro su usuario registrado entre los candidatos"}, 500

                userdict = dict(userresult)
                employee = userdict.get("c_id")
                query = query + " AND o.area_leader = '" + str(employee) +"'"

            if search:
                query = query + " AND (c.full_name LIKE ('%%"+ str(search)+"%%') OR  j.job_title LIKE ('%%"+ str(search)+"%%') )"
        
            query= query +   " GROUP BY `o`.`id` "
            

            sql = db.engine.execute(query)
            cont_row = sql.rowcount

            if page and perpage:
                page = int(page)
                perpage = int(perpage)
                offset = (perpage*page)-perpage
                query = query + ' LIMIT '+ str(perpage) + ' OFFSET ' + str(offset)
            
            sql = db.engine.execute(query)
            query+=';'
            return jsonify({'results': [(dict({
                'id':row.id,
                'collaborator':row.collaborator,
                'job_title':row.job_title,
                'area':row.area,
                'hours':row.hours,
                'dateStart':row.dateStart,
                'dateStartTime':datetime.datetime.strftime(row.dateStartTime,"%Y-%m-%d %H:%M:%S"),
                'dateEnd':row.dateEnd,
                'status':row.status,
                'dateEndTime': datetime.datetime.strftime(row.dateEndTime,"%Y-%m-%d %H:%M:%S"),
            })) for row in sql.fetchall()], 'row_total': cont_row,'success': True })

            
            
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            logging.error(exc_obj)
            logging.error(exc_type)
            logging.error(fname + ': ' + str(exc_tb.tb_lineno))
            db.session.rollback()
            return {'success': False, 'message': e },400
        finally:
            db.session.close()
    else:
        try:
            query = """ 
                    SELECT `od`.`id` AS `id`,`od`.`overtime` AS `overtime`,`od`.`hours_counted` AS `hours`,`od`.`hour_value` AS `hour_value`, `p`.`description` AS `description`
                    FROM `"""+ DATABASE_NAME +"""`.overtime_details as od
                    JOIN `"""+ DATABASE_NAME +"""`.overtime_params as p  ON od.overtime_param = p.id 
                    WHERE `od`.overtime = """ + str(id) + """ 
                """
            query+=';'
            sql = db.engine.execute(query)
            cont_row = sql.rowcount
            result = [(dict(row)) for row in sql.fetchall()]
            
            return {"success": True, "row_count": cont_row, "results": result},200
            
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            logging.error(exc_obj)
            logging.error(exc_type)
            logging.error(fname + ': ' + str(exc_tb.tb_lineno))
            db.session.rollback()
            return {'success': False, 'message': e },400
        finally:
            db.session.close()



def changeStatusOvertime(data, id):
    
    if not data:
        return {"success": False, "message": "La información de la hora extra es obligatoria"}, 400
    
    if not id:
        return {"success": False, "message": "El código de la hora extra es un campo obligatorio"},400

    #Initial status
    change_status=  "pending"
    change_reviewed_by= None
    change_reviewed_at= None
    
    if data.get("status_detail") == "approved" or  data.get("status_detail") == "rejected":
        change_status=  data.get("status_detail")
        change_reviewed_by= data.get("reviewed_by")
        change_reviewed_at= datetime.datetime.utcnow(),
 

    try:
        update_overtime = """
            UPDATE `"""+ DATABASE_NAME +"""`.`overtime`
            SET
            `status` = :status,
            `reviewed_by` = :reviewed_by,
            `reviewed_at` = :reviewed_at
            WHERE `id` = :id;
        """
        
        params = {
            "status": change_status,
            "reviewed_by": change_reviewed_by,
            "reviewed_at": change_reviewed_at,
            "id": str(id)
        }
        
        result = db.session.execute(update_overtime, params)
        db.session.commit()
        result.close()
        
        return {"success": True, "message": "Se ha actualizado correctamente el estado de la hora extra"}, 200
    
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        db.session.rollback()
        return {'success': False, 'message': e },400
    
    finally:
        db.session.close()

### END OVERTIME SECTION ################################################################

#### PERMISSION SECTION ################################################################
def get_permissions (args):
    account = args.get("entity_account")
    page = args.get("page")
    perpage = args.get("perpage")
    status= args.get("status")
    search= args.get("search")
    type_pemission = args.get("type")
    user= args.get ("user")

    if not account:
        return {"success": False, "message": "El atributo cuenta en sesión es obligatorio"}, 400
    
    try:
    
        query = """ 
                SELECT `p`.`id` AS `id`,`c`.`full_name` AS `collaborator`,`j`.`job_title` AS `job_title`,`cc`.`site_name` AS `area`, `p`.`days_counted` AS `count`,`p`.`type` AS `type`, `p`.`support_file` AS `file`, CONCAT (DATE_FORMAT (`p`.`start_date`, '%%d/%%m/%%Y') , " - ", DATE_FORMAT (`p`.`end_date`, '%%d/%%m/%%Y')) AS `date`, `p`.`status` AS `status`
                FROM `"""+ DATABASE_NAME +"""`.permissions as p
                JOIN `"""+ DATABASE_NAME +"""`.candidates as c  ON p.employee = c.id
                JOIN `"""+ DATABASE_NAME +"""`.job_positions as j ON j.id = p.job_position
                JOIN `"""+ DATABASE_NAME +"""`.`contracts` as cc ON cc.id = p.contract
                WHERE p.entity_account =""" + account + """ 
            """
        if status:
            query = query + " AND p.status = '" + str(status) +"'"
        
        if type_pemission:
            query = query + " AND p.type = '" + str(type_pemission) +"'"
        
        if user:
            queryuser ="""
            Select 
            u.id_candidate as c_id
            from rel_users_candidate u 
            WHERE u.id_user = """ + user + """ 
            """
            userresult = db.session.execute(queryuser, {"user": user}).fetchone()

            if userresult is None:
                return {"success": False, "message":"No se encontro su usuario registrado entre los candidatos"}, 500

            userdict = dict(userresult)
            employee = userdict.get("c_id")
            query = query + " AND p.area_leader = '" + str(employee) +"'"

        if search:
            query = query + " AND (c.full_name LIKE ('%%"+ str(search)+"%%') OR  j.job_title LIKE ('%%"+ str(search)+"%%') )"

        query= query +   " GROUP BY `p`.`id` "
        

        sql = db.engine.execute(query)
        cont_row = sql.rowcount

        if page and perpage:
            page = int(page)
            perpage = int(perpage)
            offset = (perpage*page)-perpage
            query = query + ' LIMIT '+ str(perpage) + ' OFFSET ' + str(offset)
        
        sql = db.engine.execute(query)
        query+=';'
        return jsonify({'results': [(dict(row)) for row in sql.fetchall()], 'row_total': cont_row,'success': True })

        
        
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        db.session.rollback()
        return {'success': False, 'message': e },400
    finally:
        db.session.close()


def changeStatusPermissions(data, id):
    
    if not data:
        return {"success": False, "message": "La información del permiso es obligatoria"}, 400
    
    if not id:
        return {"success": False, "message": "El atributo código del permiso es obligatorio"},400

    #Initial status
    change_status = data.get("status")
    change_reviewed_by= data.get("reviewed_by")
    change_reviewed_at= datetime.datetime.utcnow(),
 

    try:
        update_permissions = """
            UPDATE `"""+ DATABASE_NAME +"""`.`permissions`
            SET
            `status` = :status,
            `reviewed_by` = :reviewed_by,
            `reviewed_at` = :reviewed_at
            WHERE `id` = :id;
        """
        
        params = {
            "status": change_status,
            "reviewed_by": change_reviewed_by,
            "reviewed_at": change_reviewed_at,
            "id": str(id)
        }
        
        result = db.session.execute(update_permissions, params)
        db.session.commit()
        result.close()
        
        return {"success": True, "message": "Se ha actualizado correctamente el estado del permiso"}, 200
    
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        db.session.rollback()
        return {'success': False, 'message': e },400
    
    finally:
        db.session.close()
   
### END PERMISSION SECTION #############################################################


#### VACATIONS SECTION ################################################################
def get_vacations (args):
    account = args.get("entity_account")
    page = args.get("page")
    perpage = args.get("perpage")
    status= args.get("status")
    search= args.get("search")
    user= args.get ("user")
    dateFrom= args.get("dateFrom")
    dateUntil= args.get("dateUntil")
    type_vacations= args.get("type")

    if not account:
        return {"success": False, "message": "El atributo cuenta en sesión es obligatorio"}, 400
    
    try:
    
        query = """ 
                SELECT `v`.`id` AS `id`,`c`.`full_name` AS `collaborator`,`j`.`job_title` AS `job_title`,`cc`.`site_name` AS `area`, `v`.`days_counted` AS `count`, `v`.`type` AS `type`, CONCAT (DATE_FORMAT (`v`.`start_date`, '%%d/%%m/%%Y') , " - ", DATE_FORMAT (`v`.`end_date`, '%%d/%%m/%%Y')) AS `date`, `v`.`status` AS `status`
                FROM `"""+ DATABASE_NAME +"""`.vacations as v
                JOIN `"""+ DATABASE_NAME +"""`.candidates as c  ON v.employee = c.id
                JOIN `"""+ DATABASE_NAME +"""`.job_positions as j ON j.id = v.job_position
                JOIN `"""+ DATABASE_NAME +"""`.`contracts` as cc ON cc.id = v.contract
                JOIN `"""+ DATABASE_NAME +"""`.`rel_users_candidate` as rel ON rel.id_user = 
                WHERE v.entity_account =""" + account + """ 
            """
        if status:
            query = query + " AND v.status = '" + str(status) +"'"
        
        if user:
            queryuser ="""
            Select 
            u.id_candidate as c_id
            from rel_users_candidate u 
            WHERE u.id_user = """ + user + """ 
            """
            userresult = db.session.execute(queryuser, {"user": user}).fetchone()

            if userresult is None:
                return {"success": False, "message":"No se encontro su usuario registrado entre los candidatos"}, 500

            userdict = dict(userresult)
            employee = userdict.get("c_id")
            query = query + " AND v.area_leader = '" + str(employee) +"'"
        
        if type_vacations:
            query = query + " AND v.type = '" + str(type_vacations) +"'"
        
        if dateFrom:
            query = query +  " AND DATE(v.start_date) >= '" + str(dateFrom)+"'"
            
        if dateUntil:
            query = query +  " AND DATE(v.end_date) <= '" + str(dateUntil)+"'"

        if search:
            query = query + " AND (c.full_name LIKE ('%%"+ str(search)+"%%') OR  j.job_title LIKE ('%%"+ str(search)+"%%') )"

        query= query +   " GROUP BY `v`.`id` "
        

        sql = db.engine.execute(query)
        cont_row = sql.rowcount

        if page and perpage:
            page = int(page)
            perpage = int(perpage)
            offset = (perpage*page)-perpage
            query = query + ' LIMIT '+ str(perpage) + ' OFFSET ' + str(offset)
        
        sql = db.engine.execute(query)
        query+=';'
        return jsonify({'results': [(dict(row)) for row in sql.fetchall()], 'row_total': cont_row,'success': True })

        
        
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        db.session.rollback()
        return {'success': False, 'message': e },400
    finally:
        db.session.close()


def changeStatusVacations(data, id):
    
    if not data:
        return {"success": False, "message": "La información de la solicitud de vaciones es obligatorio"}, 400
    
    if not id:
        return {"success": False, "message": "El atributo código de la solicitud de vaciones es obligatorio"},400

    #Initial status
    change_status = data.get("status")
    change_reviewed_by= data.get("reviewed_by")
    change_reviewed_at= datetime.datetime.utcnow(),
 

    try:
        update_vacations = """
            UPDATE `"""+ DATABASE_NAME +"""`.`vacations`
            SET
            `status` = :status,
            `reviewed_by` = :reviewed_by,
            `reviewed_at` = :reviewed_at
            WHERE `id` = :id;
        """
        
        params = {
            "status": change_status,
            "reviewed_by": change_reviewed_by,
            "reviewed_at": change_reviewed_at,
            "id": str(id)
        }
        
        result = db.session.execute(update_vacations, params)
        db.session.commit()
        result.close()
        
        return {"success": True, "message": "Se ha actualizado correctamente el estado de la solicitud de vacaciones"}, 200
    
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        db.session.rollback()
        return {'success': False, 'message': e },400
    
    finally:
        db.session.close()
   
### END VACATIONS SECTION #############################################################


### UPDATE STATUS : INHABILITIES SECTION #############################################################
def get_inhabilities (args):
    account = args.get("entity_account")
    page = args.get("page")
    perpage = args.get("perpage")
    status= args.get("status")
    search= args.get("search")
    user= args.get ("user")
    dateFrom= args.get("dateFrom")
    dateUntil= args.get("dateUntil")

    if not account:
        return {"success": False, "message": "El atributo cuenta en sesión es obligatorio"}, 400
    
    try:
    
        query = """ 
                SELECT `i`.`id` AS `id`,`c`.`full_name` AS `collaborator`,`j`.`job_title` AS `job_title`,`cc`.`site_name` AS `area`, `i`.`days_counted` AS `count`, `i`.`support_file` AS `file`, `i`.`type` AS `type`, CONCAT (DATE_FORMAT (`i`.`start_date`, '%%d/%%m/%%Y') , " - ", DATE_FORMAT (`i`.`end_date`, '%%d/%%m/%%Y')) AS `date`, `i`.`status` AS `status`
                FROM `"""+ DATABASE_NAME +"""`.inhabilities as i
                JOIN `"""+ DATABASE_NAME +"""`.candidates as c  ON i.employee = c.id
                JOIN `"""+ DATABASE_NAME +"""`.job_positions as j ON j.id = i.job_position
                JOIN `"""+ DATABASE_NAME +"""`.`contracts` as cc ON cc.id = i.contract
                WHERE i.entity_account =""" + account + """ 
            """
        if status:
            query = query + " AND i.status = '" + str(status) +"'"
        
        if user:
            queryuser ="""
            Select 
            u.id_candidate as c_id,
            from rel_users_candidate u 
            WHERE u.id_user = """ + user + """ 
            """
            userresult = db.session.execute(queryuser, {"user": user}).fetchone()

            if userresult is None:
                return {"success": False, "message":"No se encontro su usuario registrado entre los candidatos"}, 500

            userdict = dict(userresult)
            employee = userdict.get("c_id")
            query = query + " AND i.area_leader = '" + str(employee) +"'"
        
        if dateFrom:
            query = query +  " AND DATE(i.start_date) >= '" + str(dateFrom)+"'"
            
        if dateUntil:
            query = query +  " AND DATE(i.end_date) <= '" + str(dateUntil)+"'"

        if search:
            query = query + " AND (c.full_name LIKE ('%%"+ str(search)+"%%') OR  j.job_title LIKE ('%%"+ str(search)+"%%') )"

        query= query +   " GROUP BY `i`.`id` "
        

        sql = db.engine.execute(query)
        cont_row = sql.rowcount

        if page and perpage:
            page = int(page)
            perpage = int(perpage)
            offset = (perpage*page)-perpage
            query = query + ' LIMIT '+ str(perpage) + ' OFFSET ' + str(offset)
        
        sql = db.engine.execute(query)
        query+=';'
        return jsonify({'results': [(dict(row)) for row in sql.fetchall()], 'row_total': cont_row,'success': True })

        
        
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        db.session.rollback()
        return {'success': False, 'message': e },400
    finally:
        db.session.close()

def changeStatusInhability(data, id):
    
    if not data:
        return {"success": False, "message": "La información de la incapacidad es obligatoria"}, 400
    
    if not id:
        return {"success": False, "message": "El atributo código de la incapacidad es obligatorio"},400

    #Initial status
    change_status = data.get("status")
    change_reviewed_by= data.get("reviewed_by")
    change_reviewed_at= datetime.datetime.utcnow(),
 

    try:
        update_inhabilities = """
            UPDATE `"""+ DATABASE_NAME +"""`.`inhabilities`
            SET
            `status` = :status,
            `reviewed_by` = :reviewed_by,
            `reviewed_at` = :reviewed_at
            WHERE `id` = :id;
        """
        
        params = {
            "status": change_status,
            "reviewed_by": change_reviewed_by,
            "reviewed_at": change_reviewed_at,
            "id": str(id)
        }
        
        result = db.session.execute(update_inhabilities, params)
        db.session.commit()
        result.close()
        
        return {"success": True, "message": "Se ha actualizado correctamente el estado de la incapacidad"}, 200
    
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        db.session.rollback()
        return {'success': False, 'message': e },400
    
    finally:
        db.session.close()
   
### END UPDATE STATUS : INHABILITIES SECTION #############################################################


def inhability_post(data):
    try:
        if data.get("created_by") is None:
            return {"success": False, "message":"Se requiere un id de usuario"}, 500

        queryuser ="""select 
        c.id as c_id, c.full_name, con.id as con_id
        from rel_users_candidate u 
        join candidates c ON c.id = u.id_candidate 
        join contracts con ON con.candidate = u.id_candidate 
        where u.id_user = :userid"""
        userresult = db.session.execute(queryuser, {"userid": data.get("created_by")}).fetchone()

        if userresult is None:
            return {"success": False, "message":"No se encontro su usuario registrado entre los candidatos"}, 500

        userdict = dict(userresult)
        employee = userdict.get("c_id")
        contract = userdict.get("con_id")


        query= """INSERT INTO inhabilities 
            (
                status, job_position, area_leader,
                type, start_date, end_date,
                diagnosis, support_file,
                employee, contract,
                created_by, entity_account
            )
            VALUES 
            (
                :status, :job_position, :area_leader,
                :type, :start_date, :end_date,
                :diagnosis, :support_file,
                :employee, :contract,
                :created_by, :entity_account
            )"""
        params = {
            "status": data.get("status"),
            "job_position": data.get("job_position"),
            "area_leader": data.get("area_leader"),

            "type": data.get("type"),
            "start_date": data.get("start_date"),
            "end_date": data.get("end_date"),

            "diagnosis": data.get("diagnosis"),
            "support_file": data.get("support_file"),
            
            "employee": employee,
            "contract": contract,

            "created_by": data.get("created_by"),
            "entity_account": data.get("entity_account"),
        }
        result = db.session.execute(query, params)
        lastInsert = result.lastrowid

        db.session.commit()
        return {"success":True, "id_inserted": lastInsert}, 200
    except Exception as e:
        return responseError(e, 500)
    finally:
        db.session.close()

def inhability_put(data):
    try:
        delId = data.get('id')
        if delId is None:
            return {"success":False, "message": "no hay ID para editar"}, 200

        query = """ UPDATE inhabilities SET """
        
        params={
            'id': delId
        }

        setarraytemp = []

        if data.get('support_file') is not None:
            setarraytemp.append("  support_file = :support_file ")
            params['support_file']=data.get('support_file')
        else:
            if data.get('signed_doc') is not None:
                signed_doc = data.get('signed_doc') 
                if len(signed_doc) >0:
                    setarraytemp.append("  support_file = :support_file ")
                    params['support_file']=signed_doc[0].get("url")

        if len(setarraytemp) <= 0:
            return {"success":False, "message": "No hay info para actualizar, revisar data enviada"}, 200

        query = query + ",".join(setarraytemp) + " WHERE id = :id "

        result = db.session.execute(query, params)


        db.session.commit()
        return {"success":True, "result": str(delId)+" updated with file located at "+str(params.get('support_file'))}, 200
    except Exception as e:
        return responseError(e, 500)
    finally:
        db.session.close()

### REQUEST VACATIONSgit  #############################################################

def listHistoryHolidaysEmployee(args):
    try:
        account = args.get("entity_account")
        user = args.get("user")
        if user is None:
            return {"success": False, "message":"Se requiere un id de usuario"}, 500
        
        if not account:
            return {"success": False, "message": "El atributo cuenta en sesión es obligatorio"}, 400

        queryuser ="""
        Select 
        c.id as c_id, c.full_name, con.id as con_id, con.job_position as job_id, con.start_date as con_start_date
        from `"""+ DATABASE_NAME +"""`.rel_users_candidate u 
        join `"""+ DATABASE_NAME +"""`.candidates c ON c.id = u.id_candidate 
        join `"""+ DATABASE_NAME +"""`.contracts con ON con.candidate = u.id_candidate 
        where u.id_user =:userid and con.status='current' 
        """
        userresult = db.session.execute(queryuser, {"userid": user}).fetchone()

        if userresult is None:
            return {"success": False, "message":"No se encontro su usuario registrado entre los candidatos"}, 500
        
        userdict = dict(userresult)
        employee = userdict.get("c_id")
        contract = userdict.get("con_id")
        job_position = userdict.get("job_id")
        con_start_date = userdict.get("con_start_date")
    
        queryVacations ="""
        Select 
        v.id as id, v.start_date as date, v.days_counted as days_counted, v.type as type, v.status as status
        from `mod-payroll-core-dev`.vacations v 
        where DATE(v.start_date) > DATE_SUB(curdate(),INTERVAL 12 MONTH)
        AND v.employee = :employee 
        """

        result_vacations = db.session.execute(queryVacations, {"employee": employee })
        return jsonify({'results': [(dict(row)) for row in result_vacations.fetchall()],'success': True })

    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        db.session.rollback()
        return {'success': False, 'message': e },400
    finally:
        db.session.close()


def request_holidays(data):
    try:
        account = data.get("entity_account")

        if data.get("created_by") is None:
            return {"success": False, "message":"Se requiere un id de usuario"}, 500
        
        if data.get("job_position") is None:
            return {"success": False, "message":"Se requiere un id del cargo del usuario"}, 500
        
        if data.get("leader") is None:
            return {"success": False, "message":"Se requiere un id del jefe de area del usuario"}, 500
        
        if data.get("contract") is None:
            return {"success": False, "message":"Se requiere un id del contrato del usuario"}, 500
        
        if data.get("type") is None:
            return {"success": False, "message":"Se requiere seleccionar el tipo de vaciones"}, 500
        
        if data.get("dateFrom") is None:
            return {"success": False, "message":"Se requiere elegir una fecha de inicio de vaciones"}, 500
        
        if data.get("dateUntil") is None:
            return {"success": False, "message":"Se requiere elegir una fecha de fin de vaciones"}, 500
        
        if not account:
            return {"success": False, "message": "El atributo cuenta en sesión es obligatorio"}, 500

        queryuser ="""
        Select 
        c.id as c_id, c.full_name, con.id as con_id, con.job_position as job_id, con.start_date as con_start_date
        from `"""+ DATABASE_NAME +"""`.rel_users_candidate u 
        join `"""+ DATABASE_NAME +"""`.candidates c ON c.id = u.id_candidate 
        join `"""+ DATABASE_NAME +"""`.contracts con ON con.candidate = u.id_candidate 
        where u.id_user =:userid and con.status='current' 
        """
        userresult = db.session.execute(queryuser, {"userid": data.get("created_by")}).fetchone()

        if userresult is None:
            return {"success": False, "message":"No se encontro su usuario registrado entre los candidatos"}, 500
        
        userdict = dict(userresult)
        employee = userdict.get("c_id")

        queryContract ="""
        Select 
        c.start_date as dateStart
        from `"""+ DATABASE_NAME +"""`.contracts as c
        where c.id =:contract and c.status='current'
        """
        contractresult = db.session.execute(queryContract, {"contract": data.get("contract") }).fetchone()
        
        if contractresult is None:
            return {"success": False, "message":"No se encontro activo el contrato seleccionado"}, 500
        else: 
            contractdict= dict(contractresult)
            dateStart = contractdict.get("dateStart")
            dateEnd= data.get("dateUntil")
        


        querypaidVacation ="""
        Select 
        sum(v.days_counted) as days
        from `"""+ DATABASE_NAME +"""`.vacations as v
        where v.employee =:employee and type = 'paid'
        """
        paidVacationresult = db.session.execute(querypaidVacation, {"employee": employee}).fetchone()
        if paidVacationresult is None:
            daysVacations = 0
        else:
            paidVacationdict= dict(paidVacationresult)
            if paidVacationdict.get("days") is not None:
                dayspaidVacations = paidVacationdict.get("days")
            else:
                dayspaidVacations = 0 

        queryDays="""
        SELECT 
        IF(LAST_DAY(:start)=
        LAST_DAY(:end),TIMESTAMPDIFF(DAY,:start,:end)+1,
        IF(:start>CONCAT(SUBSTR(:start,1,8),'01'),
        TIMESTAMPDIFF(DAY,:start,LAST_DAY(:start))+1,30)+
        IF(:end<LAST_DAY(:end),
        TIMESTAMPDIFF(DAY,CONCAT(SUBSTR(:end,1,8),'01'),
        :end)+1,30)+TIMESTAMPDIFF(MONTH,DATE_ADD(LAST_DAY(:start),
        INTERVAL 1 DAY),CONCAT(SUBSTR(:end,1,8),'01'))*30) as days;
        """

        paramsDays = {
            "start": dateStart,          
            "end" : dateEnd
        }

        daysresult = db.session.execute(queryDays, paramsDays).fetchone()
        daysresultdict= dict(daysresult)


        if daysresultdict.get("days") is not None:
            daysWork= daysresultdict.get("days") 
            if daysWork == 0:
                return {"success":True, "message": "El usuario posee 0 días laborados"}, 400

        
        if data.get("type") == "paid":
            queryPermissions ="""
            Select 
            sum(p.days_counted) as days_permissions
            from `"""+ DATABASE_NAME +"""`.permissions p 
            where 
            p.employee =:employee 
            AND p.status!='rejected'
            """
        else:
            queryPermissions ="""
            Select 
            sum(p.days_counted) as days_permissions
            from `"""+ DATABASE_NAME +"""`.permissions p 
            where 
            p.employee =:employee 
            AND p.status='approved'
            """
        paramsPermissions = {
            "employee": employee
        }
        if dateStart:
            queryPermissions = queryPermissions +  " AND DATE(p.start_date) >= '" + str(dateStart)+"'"
            
        if dateEnd:
            queryPermissions = queryPermissions +  " AND DATE(p.end_date) <= '" + str(dateEnd)+"'" 

        
        permissionsresult = db.session.execute(queryPermissions,paramsPermissions).fetchone()
        permissionsresultdict = dict(permissionsresult)


        if permissionsresultdict.get("days_permissions") is not None:
            if permissionsresultdict.get("days_permissions") >= 0:
                daysTotalWork = float(daysWork) - float(permissionsresultdict.get("days_permissions"))
                if daysTotalWork >=0: 
                    allowed_days = round((15*daysTotalWork)/(360))
                    if data.get("type")== "paid":
                        if  (dayspaidVacations + data.get("days_counted")) > (allowed_days*0.5):
                            return {"success":True, "message": "Los días solicitados superan los días permitidos para aplicar a vacaciones"}, 400
                        else:
                            sw=True
                    else:
                        if allowed_days>= data.get("days_counted"):
                            sw = True
                        else: 
                            sw = False
                            return {"success":True, "message": "Los días solicitados superan los días permitidos para aplicar a vacaciones"}, 400
                else:
                    return {"success":True, "message": "Los días utilizados para permisos superan los días laborados"}, 400
        else:
            allowed_days = round((15*daysWork)/(360))
            if data.get("type")== "paid":
                if  (dayspaidVacations + data.get("days_counted")) > round(allowed_days*0.5):
                    return {"success":True, "message": "Los días solicitados superan los días permitidos para aplicar a vacaciones"}, 400
                else:
                    sw=True
            else:
                if allowed_days >= data.get("days_counted"):
                    sw = True
                else: 
                    sw = False
                    return {"success":True, "message": "Los días solicitados superan los días permitidos para aplicar a vacaciones"}, 400

        if sw :
            try:

                query= """INSERT INTO  `"""+ DATABASE_NAME +"""`.vacations 
                    (
                        type, employee, job_position, contract,
                        area_leader, start_date, end_date,
                        days_counted, status, entity_account,
                        created_by, created_at
                    )
                    VALUES 
                    (
                        :type, :employee, :job_position,
                        :contract, :area_leader, :start_date,
                        :end_date, :days_counted,
                        :status, :entity_account,
                        :created_by, :created_at
                    )"""
                params = {
                    "type": data.get("type"),
                    "employee": employee ,
                    "job_position": data.get("job_position") ,
                    "contract": data.get("contract"),
                    "area_leader": data.get("leader"),
                    
                    "start_date": data.get("dateFrom"),
                    "end_date": data.get("dateUntil"),

                    "days_counted": data.get("days_counted"),
                    "status": "pending",

                    "entity_account": data.get("entity_account"),
                    "created_by": data.get("created_by"),
                    "created_at": datetime.datetime.utcnow(),
                
                }
                result = db.session.execute(query, params)
                db.session.commit()
                return {"success":True, "message": "Las vacaciones han sido solicitadas correctamente"}, 200
            except Exception as e:
                exc_type, exc_obj, exc_tb = sys.exc_info()
                fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                logging.error(exc_obj)
                logging.error(exc_type)
                logging.error(fname + ': ' + str(exc_tb.tb_lineno))
                db.session.rollback()
                return {'success': False, 'message': e },400
            finally:
                db.session.close()
        
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        db.session.rollback()
        return {'success': False, 'message': e },400
    finally:
        db.session.close()


def days_available(data):
    try:
        account = data.get("entity_account")
        user = data.get("created_by")
        if user is None:
            return {"success": False, "message":"Se requiere un id de usuario"}, 500
        
        if not account:
            return {"success": False, "message": "El atributo cuenta en sesión es obligatorio"}, 400
        
        if data.get("job_position") is None:
            return {"success": False, "message":"Se requiere un id del cargo del usuario"}, 500
        
        if data.get("leader") is None:
            return {"success": False, "message":"Se requiere un id del jefe de area del usuario"}, 500
        
        if data.get("contract") is None:
            return {"success": False, "message":"Se requiere un id del contrato del usuario"}, 500
        
        if data.get("type") is None:
            return {"success": False, "message":"Se requiere seleccionar el tipo de vaciones"}, 500

        queryuser ="""
        Select 
        c.id as c_id, c.full_name, con.id as con_id, con.job_position as job_id, con.start_date as con_start_date
        from `"""+ DATABASE_NAME +"""`.rel_users_candidate u 
        join `"""+ DATABASE_NAME +"""`.candidates c ON c.id = u.id_candidate 
        join `"""+ DATABASE_NAME +"""`.contracts con ON con.candidate = u.id_candidate 
        where u.id_user =:userid and con.status='current' 
        """
        userresult = db.session.execute(queryuser, {"userid": user}).fetchone()

        if userresult is None:
            return {"success": False, "message":"No se encontro su usuario registrado entre los candidatos"}, 500
        
        userdict = dict(userresult)
        employee = userdict.get("c_id")

        queryContract ="""
        Select 
        c.start_date as dateStart
        from `"""+ DATABASE_NAME +"""`.contracts as c
        where c.id =:contract and c.status='current'
        """
        contractresult = db.session.execute(queryContract, {"contract": data.get("contract") }).fetchone()
        
        if contractresult is None:
            return {"success": False, "days": 0, "message":"No se encontro activo el contrato seleccionado"}, 500
        else: 
            contractdict= dict(contractresult)
            dateStart = contractdict.get("dateStart")
            dateEnd= datetime.datetime.utcnow()

        querypaidVacation ="""
        Select 
        sum(v.days_counted) as days
        from `"""+ DATABASE_NAME +"""`.vacations as v
        where v.employee =:employee and type = 'paid'
        """
        paidVacationresult = db.session.execute(querypaidVacation, {"employee": employee}).fetchone()
        if paidVacationresult is None:
            daysVacations = 0
        else:
            paidVacationdict= dict(paidVacationresult)
            if paidVacationdict.get("days") is not None:
                dayspaidVacations = paidVacationdict.get("days")
            else:
                dayspaidVacations = 0 

        queryDays="""
        SELECT 
        IF(LAST_DAY(:start)=
        LAST_DAY(:end),TIMESTAMPDIFF(DAY,:start,:end)+1,
        IF(:start>CONCAT(SUBSTR(:start,1,8),'01'),
        TIMESTAMPDIFF(DAY,:start,LAST_DAY(:start))+1,30)+
        IF(:end<LAST_DAY(:end),
        TIMESTAMPDIFF(DAY,CONCAT(SUBSTR(:end,1,8),'01'),
        :end)+1,30)+TIMESTAMPDIFF(MONTH,DATE_ADD(LAST_DAY(:start),
        INTERVAL 1 DAY),CONCAT(SUBSTR(:end,1,8),'01'))*30) as days;
        """

        paramsDays = {
            "start": dateStart,          
            "end" : dateEnd
        }

        daysresult = db.session.execute(queryDays, paramsDays).fetchone()
        daysresultdict= dict(daysresult)


        if daysresultdict.get("days") is not None:
            daysWork= daysresultdict.get("days") 
            if daysWork == 0:
                return {"success":True, "days": 0, "message": "El usuario posee 0 días laborados"}, 400

        
        if data.get("type") == "paid":
            queryPermissions ="""
            Select 
            sum(p.days_counted) as days_permissions
            from `"""+ DATABASE_NAME +"""`.permissions p 
            where 
            p.employee =:employee 
            AND p.status!='rejected'
            """
        else:
            queryPermissions ="""
            Select 
            sum(p.days_counted) as days_permissions
            from `"""+ DATABASE_NAME +"""`.permissions p 
            where 
            p.employee =:employee 
            AND p.status='approved'
            """
        paramsPermissions = {
            "employee": employee
        }
        if dateStart:
            queryPermissions = queryPermissions +  " AND DATE(p.start_date) >= '" + str(dateStart)+"'"
            
        if dateEnd:
            queryPermissions = queryPermissions +  " AND DATE(p.end_date) <= '" + str(dateEnd)+"'" 

        
        permissionsresult = db.session.execute(queryPermissions,paramsPermissions).fetchone()
        permissionsresultdict = dict(permissionsresult)


        if permissionsresultdict.get("days_permissions") is not None:
            if permissionsresultdict.get("days_permissions") >= 0:
                daysTotalWork = float(daysWork) - float(permissionsresultdict.get("days_permissions"))
                allowed_days = round((15*daysTotalWork)/(360))
                if data.get("type")== "paid":
                    return {"success":True, "days": round(allowed_days*0.5) - float(dayspaidVacations)}, 200
                else:
                    return {"success":True, "days": allowed_days}, 200
        else:
            allowed_days = round((15*daysWork)/(360))
            if data.get("type")== "paid":
                return {"success":True, "days": round(allowed_days*0.5)- float(dayspaidVacations) }, 200
            else:
                return {"success":True, "days": allowed_days}, 200

    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        db.session.rollback()
        return {'success': False, 'message': e },400
    finally:
        db.session.close()

### REQUEST PERMISSION  #############################################################
def request_permission(data):
    try:
        if data.get("created_by") is None:
            return {"success": False, "message":"Se requiere un id de usuario"}, 500
        
        if data.get("job_position") is None:
            return {"success": False, "message":"Se requiere un id del cargo del usuario"}, 500
        
        if data.get("leader") is None:
            return {"success": False, "message":"Se requiere un id del jefe de area del usuario"}, 500
        
        if data.get("contract") is None:
            return {"success": False, "message":"Se requiere un id del contrato del usuario"}, 500
        
        if data.get("type") is None:
            return {"success": False, "message":"Se requiere seleccionar el tipo de vaciones"}, 500
        
        if data.get("dateFrom") is None:
            return {"success": False, "message":"Se requiere elegir una fecha de inicio de vaciones"}, 500
        
        if data.get("dateUntil") is None:
            return {"success": False, "message":"Se requiere elegir una fecha de fin de vaciones"}, 500

        queryuser ="""
        Select 
        c.id as c_id, c.full_name, con.id as con_id, con.job_position as job_id, con.start_date as con_start_date
        from `"""+ DATABASE_NAME +"""`.rel_users_candidate u 
        join `"""+ DATABASE_NAME +"""`.candidates c ON c.id = u.id_candidate 
        join `"""+ DATABASE_NAME +"""`.contracts con ON con.candidate = u.id_candidate 
        where u.id_user =:userid and con.status='current' 
        """
        userresult = db.session.execute(queryuser, {"userid": data.get("created_by")}).fetchone()

        if userresult is None:
            return {"success": False, "message":"No se encontro su usuario registrado entre los candidatos"}, 500
        
        userdict = dict(userresult)
        employee = userdict.get("c_id")

        query= """INSERT INTO  `"""+ DATABASE_NAME +"""`.permissions 
            (
                employee, job_position, contract,
                area_leader, type, start_date, end_date,
                days_counted, description,
                status, entity_account, created_by, created_at
            )
            VALUES 
            (
                :employee, :job_position,:contract, 
                :area_leader,:type,:start_date,
                :end_date, :days_counted,
                :description,
                :status, :entity_account,
                :created_by, :created_at
            )"""
        params = {
            "employee": employee ,
            "job_position": data.get("job_position") ,
            "contract": data.get("contract"),
            "area_leader": data.get("leader"),
            "type": data.get("type"),
            "start_date": data.get("dateFrom"),
            "end_date": data.get("dateUntil"),
            "days_counted": data.get("days_counted"),
            "description": data.get("description"),
            "status": "pending",
            "entity_account": data.get("entity_account"),
            "created_by": data.get("created_by"),
            "created_at": datetime.datetime.utcnow(),
        }
        result = db.session.execute(query, params)
        db.session.commit()
        lastInsert = result.lastrowid
        return {"success":True, "id": lastInsert, "document":  data.get("document"),"message": "El permiso ha sido solicitado correctamente"}, 200
            
        
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        db.session.rollback()
        return {'success': False, 'message': e },400
    finally:
        db.session.close()


def addPermissionFile(data):
    if not data:
        return {"success": False, "message": "La información del permiso es necesario"}, 400

    try:
        update_permission = """
            UPDATE `"""+ DATABASE_NAME +"""`.`permissions`
            SET
            `support_file` = :file
            WHERE `id` = :id;
        """
        params = {
            "file": data.get("url"),
            "id": str(data.get("id"))
        }
        
        result = db.session.execute(update_permission, params)
        db.session.commit()
        result.close()
        
        return {"success": True}, 200
    
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        db.session.rollback()
        return {'success': False, 'message': e },400
    
    finally:
        db.session.close()

def listHistoryPermissionEmployee(args):
    try:
        account = args.get("entity_account")
        user = args.get("user")
        if user is None:
            return {"success": False, "message":"Se requiere un id de usuario"}, 500
        
        if not account:
            return {"success": False, "message": "El atributo cuenta en sesión es obligatorio"}, 400

        queryuser ="""
        Select 
        c.id as c_id, c.full_name, con.id as con_id, con.job_position as job_id, con.start_date as con_start_date
        from `"""+ DATABASE_NAME +"""`.rel_users_candidate u 
        join `"""+ DATABASE_NAME +"""`.candidates c ON c.id = u.id_candidate 
        join `"""+ DATABASE_NAME +"""`.contracts con ON con.candidate = u.id_candidate 
        where u.id_user =:userid and con.status='current' 
        """
        userresult = db.session.execute(queryuser, {"userid": user}).fetchone()

        if userresult is None:
            return {"success": False, "message":"No se encontro su usuario registrado entre los candidatos"}, 500
        
        userdict = dict(userresult)
        employee = userdict.get("c_id")
        contract = userdict.get("con_id")
        job_position = userdict.get("job_id")
        con_start_date = userdict.get("con_start_date")
    
        queryPermissions ="""
        Select 
        p.id as id, p.start_date as date, p.days_counted as days_counted, p.description as description, p.type as type, p.status as status
        from `mod-payroll-core-dev`.permissions p
        where DATE(p.start_date) > DATE_SUB(curdate(),INTERVAL 12 MONTH)
        AND p.employee = :employee 
        """

        result_permissions = db.session.execute(queryPermissions, {"employee": employee })
        return jsonify({'results': [(dict(row)) for row in result_permissions.fetchall()],'success': True })

    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        db.session.rollback()
        return {'success': False, 'message': e },400
    finally:
        db.session.close()


        
def register_overtime(data):
    try :
        employee = data.get('employee')
        job_position = data.get('job_position')
        contract = data.get('contract')
        area_leader = data.get('area_leader')
        start_time = data.get('start_time')
        end_time = data.get('end_time')
        description = data.get('description')
        status = "pending"
        entity_account = data.get('entity_account')
        created_by = data.get('created_by')
        created_at = datetime.datetime.utcnow()
        


        if employee is None:
            return responseError("Campos Incompletos",400, "Es necesario el campo de id del candidato")
        if job_position is None:
            return responseError("Campos Incompletos",400, "Es necesario el campo cargo del empleado")
        if contract is None:
            return responseError("Campos Incompletos",400, "Es necesario el campo contrato del empleado")
        if area_leader is None:
            return responseError("Campos Incompletos",400, "Es necesario el campo jefe de área del empleado")
        if start_time is None:
            return responseError("Campos Incompletos",400, "Es necesario el campo fecha de entrada del empleado")
        if end_time is None:
            return responseError("Campos Incompletos",400, "Es necesario el campo fecha de salida del empleado")
        if description is None:
            return responseError("Campos Incompletos",400, "Es necesario el campo descripción")
        if entity_account is None:
            return responseError("Campos Incompletos",400, "Es necesario el campo cuenta de la entidad empleado")
        if created_by is None:
            return responseError("Campos Incompletos",400, "Es necesario el campo id empleado")
        execute_params = """
            CALL register_overtime ("""+str(employee)+","+str(contract) +","+str(area_leader) +",'"+description+"','"\
            +status +"',"+str(entity_account) +","+str(created_by) +",'"+datetime.datetime.strftime(created_at, "%Y-%m-%d %H:%M:%S")+"','"\
            +start_time+"','"+end_time+"',"\
            +str(job_position) +""");"""
        db.session.execute(execute_params)
        db.session.commit()
        return {'success': True, 'message': "Horas extra registradas exitosamente"},200
    except Exception as e:
        return responseError(str(e),500, "Error al momento de registrar la hora extra")
#..............................................seccion calendario FIN....................................


#-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_ payroll_employee_detail -_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

def payroll_employee_detail_get(params):
    try:

        newparams = {
        }

        query = """
        SELECT ps.payment_year, ps.payment_month, ps.range, ps.total_amount,  ps.status, ps.id as PS_ID,
        (
            SELECT COUNT(1) FROM payroll_employee_detail ped WHERE ped.payroll_template = ps.id 
        ) as employee_number
        FROM payroll_sheet ps
        WHERE 1=1 
        """
        
        if params.get('year'):
            query += " AND ps.payment_year = :year "
            newparams['year'] = params.get('year', type = int)

        query += " GROUP BY ps.payment_month, ps.payment_year "
        querycount = "SELECT COUNT(1) from ("+ query+" ) as subtable"
        rescount = db.session.execute(querycount, newparams).scalar()

        if params.get("page", type = int) is not None and params.get("perpage", type = int) is not None:
            query +=" LIMIT :perpage OFFSET :offset "
            newparams['offset'] = params.get("perpage", type = int) * (params.get("page", type = int) - 1)
            newparams['perpage'] = params.get('perpage', type = int)

        res = db.session.execute(query, newparams)

        finalresult = []
        for row in res.fetchall():
            dictrow = dict(row)
            myrow = {}
            for i in dictrow:
                if (i == "details") and dictrow[i] is not None:
                    myrow[i]=json.loads( dictrow[i]) 
                else:
                    myrow[i]=json_serial(dictrow[i])
            finalresult.append(myrow)

        return {"success":True, "results": finalresult, "total":rescount}, 200
    except Exception as e:
        return responseError(e, 500)
    finally:
        db.session.close()

def payroll_sheet_put(data):
    try:
        query = """ UPDATE payroll_sheet SET """
        
        params={
            'id': data.get('id')
        }

        setarraytemp = []

        if data.get('status') is not None:
            setarraytemp.append(" status = :status ")
            params['status']=data.get('status')
        
        if len(setarraytemp)<=0:
            return {"success": False, "message": "nothing to edit"}, 500

        query = query + ",".join(setarraytemp) + " WHERE id = :id "
        result = db.session.execute(query, params)
        db.session.commit()
        return {"success": True}, 200
            
    except Exception as e:
        return responseError(e, 500)
    finally:
        db.session.close()

#-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_ payroll_employee_detail -_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
