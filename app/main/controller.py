from flask import request
from flask_restplus import Resource, reqparse
from .dto import *
from . import service
import os

management = Management.api


@management.route('/test/')
class TestOnly(Resource):
    @management.doc("just wanna see if this works")
    def get(self):
        return service.TestonlyRead(request.args)

    def post(self):
        return service.TestonlyPost(request.json)

    def put(self):
        return service.TestonlyPut(request.json)


###### RECEIVABLE INDICATORS

#### filed Vs Unfiled Invoices
@management.route('/filedVsUnfiledInvoices/')
class filedVsUnfiledInvoices(Resource):
    @management.doc("filed Vs Unfiled Invoices")
    def get(self):
        return service.filedVsUnfiledInvoices(request.args)

#### Unfiled Invoices
@management.route('/unfiledInvoices/')
class UnfiledInvoices(Resource):
    @management.doc("Unfiled Invoices")
    def get(self):
        return service.unfiledInvoices(request.args)

#### Unfiled Invoices
@management.route('/totalReceivables/')
class TotalReceivables(Resource):
    @management.doc("Total Receivables")
    def get(self):
        return service.totalReceivables(request.args)

#### Portfolio by customer company
@management.route('/portfolioByCustomer/')
class PortfolioByCustomer(Resource):
    @management.doc("Portfolio By Customer")
    def get(self):
        return service.portfolioByCustomer(request.args)
        



###### PURCHASE INDICATORS
@management.route('/consumption_center/')
class Consumption_center(Resource):
    def get(self):
        return service.con_center_get(request.args)

@management.route('/consumption_center_purchases/')
class Consumption_center_purchases(Resource):
    def get(self):
        return service.consumption_center_purchases(request.args)
        
@management.route('/warehouse_problems/')
class WarehouseProblems(Resource):
    def get(self):
        return service.warehouse_problems(request.args)
        
@management.route('/expired_supplies/')
class InsumosVencidos(Resource):
    def get(self):
        return service.insumos_vencidos(request.args)
        


###### PAYROLL INDICATORS
#### Total number of employees
@management.route('/totalEmployees/')
class TotalEmployees(Resource):
    @management.doc("Total employees")
    def get(self):
        return service.totalEmployees(request.args)

#### Accident rate
@management.route('/accidentRate/')
class AccidentRate(Resource):
    @management.doc("Accident Rate")
    def get(self):
        return service.accidentRate(request.args)

#### Advance of resumes
@management.route('/resumePreview/')
class ResumePreview(Resource):
    @management.doc("Resume Preview")
    def get(self):
        return service.resumePreview(request.args)

#### Absenteeism
@management.route('/absenteeism/')
class Absenteeism(Resource):
    @management.doc("Absenteeism")
    def get(self):
        return service.absenteeism(request.args)

#### Total payroll investment
@management.route('/totalPayrollInvestment/')
class TotalPayrollInvestment(Resource):
    @management.doc("Total payroll investment")
    def get(self):
        return service.totalPayrollInvestment(request.args)

        

###### INVOICING INDICATORS

#### Invoicing by type of invoice
@management.route('/invoicingByType/')
class InvoicingByType(Resource):
    @management.doc("Invoicing by type of invoice")
    def get(self):
        return service.invoicingByType(request.args)

        

###### INVOICING INDICATORS

#### Invoicing by type of invoice
@management.route('/invoicingByType/')
class InvoicingByType(Resource):
    @management.doc("Invoicing by type of invoice")
    def get(self):
        return service.invoicingByType(request.args)


#### CURRENT TOTAL PROVISIONED
@management.route('/CurrentTotalProvisioned/')
class CurrentTotalProvisioned(Resource):
    @management.doc("Current Total Provisioned")
    def get(self):
        return service.currentTotalProvisioned(request.args)

# #### Portfolio By Age
# @management.route('/portfolioByAge/')
# class PortfolioByAge(Resource):
#     @management.doc("Portfolio By Age")
#     def get(self):
#         return service.portfolioByAge(request.args)

#### Details Of Account Balances
@management.route('/detailsOfAccountBalances/')
class DetailsOfAccountBalances(Resource):
    @management.doc(" Details Of Account Balances")
    def get(self):
        return service.detailsOfAccountBalances(request.args)

#### Provisioned VS Collected
@management.route('/provisionedVSCollected/')
class ProvisionedVSCollected(Resource):
    @management.doc("Provisioned VS Collected")
    def get(self):
        return service.provisionedVSCollected(request.args)


#### Provisioned VS Collected By Client
@management.route('/provisionedVSCollectedByClient/')
class ProvisionedVSCollectedByClient(Resource):
    @management.doc("Provisioned VS Collected By Client")
    def get(self):
        return service.provisionedVSCollectedByClient(request.args)



        

@management.route('/indicador_facturacion/')
class IndicadorFacturacion(Resource):
    def get(self):
        return service.indicador_facturacion(request.args)

@management.route('/gloss_objection_get/')
class GlossObjectionGet(Resource):
    def get(self):
        return service.gloss_objection_get(request.args)
        
@management.route('/treasury_vs/')
class TreasuryVS(Resource):
    def get(self):
        return service.treasury_vs(request.args)
        

@management.route('/accounts_per_bank/')
class AccountsPerBank(Resource):
    def get(self):
        return service.accounts_per_bank(request.args)
        
@management.route('/top_providers/')
class TopProviders(Resource):
    def get(self):
        return service.top_providers(request.args)
@management.route('/providers_actives/')
class ProvidersActives(Resource):
    def get(self):
        return service.actives(request.args)
@management.route('/balance_per_account/')
class BalancePerAccount(Resource):
    def get(self):
        return service.balance_per_account(request.args)
@management.route('/accounts_payable_provider/')
class AccountsPayabale(Resource):
    def get(self):
        return service.accounts_payable_provider(request.args)
@management.route('/total_income/')
class TotalIncome(Resource):
    def get(self):
        return service.total_income(request.args)
@management.route('/balance_revenues_expenses/')
class BalanceRevenuesExpenses(Resource):
    def get(self):
        return service.balance_revenues_expenses(request.args)
@management.route('/accounts_payable_balance/')
class AccountsPayableBalance(Resource):
    def get(self):
        return service.accounts_payable_balance(request.args)
@management.route('/profitBalance/')
class profitBalance(Resource):
    def get(self):
        return service.profitBalance(request.args)
        
@management.route('/overall_consumption/')
class OverallConsumption(Resource):
    def get(self):
        return service.overall_consumption(request.args)
@management.route('/top_clients/')
class TopClients(Resource):
    def get(self):
        return service.top_clients(request.args)
@management.route('/inv_rot/')
class InvRot(Resource):
    def get(self):
        return service.inv_rot(request.args)
@management.route('/period_expenses/')
class PeriodExpenses(Resource):
    def get(self):
        return service.period_expenses(request.args)
