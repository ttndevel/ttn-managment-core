import decimal
import os

from sqlalchemy.sql.visitors import CloningVisitor
from sqlalchemy.sql.expression import true
import jwt
import json
import logging
import re
#from flask_weasyprint import HTML, render_pdf
from app.main import db
from sqlalchemy.sql import base, func, desc, asc, distinct
from sqlalchemy import extract, or_, and_, not_, exc, update
from app.main.model import *
import datetime , calendar
from flask import jsonify, render_template_string
import base64
import os
import sys
from decimal import Decimal
from sqlalchemy.sql.sqltypes import Enum, Numeric

DATABASE_NAME = os.environ["MYSQL_DATABASE"]
PAYROLL_DBNAME = os.environ['PAYROLL_DBNAME']
SECURIT_DBNAME = os.environ['SECURIT_DBNAME']
INVENTORY_DBNAME = os.environ['INVENTORY_DBNAME']
INVOICING_DBNAME = os.environ['INVOICING_DBNAME']
ADMINISTRATION_DBNAME = os.environ['ADMINISTRATION_DBNAME']
URL_RECEIVABLE=os.environ['URL_RECEIVABLE']
TREASURY_DBNAME = os.environ['TREASURY_DBNAME']
RECEIVABLE_DBNAME =os.environ['RECEIVABLE_DBNAME']
ACCOUNTING_DBNAME =os.environ['ACCOUNTING_DBNAME']
ADMINIS_DBNAME = os.environ['ADMINIS_DBNAME']

# response error method 🤝 🔂       
def responseError(error,status_code,*message):
    # message get like array, if you need return a message (Not required)⬇ 
    if message:
        return {'success':False,'message':message[0],'error':error},int(status_code)
    #  if not need message, just use error, and status code (http code)
    else:
        return {'success':False,'error':error},int(status_code)

# This method receives a params array [] 🤞 
def responseSuccess(results,message,row_total):
    return jsonify({
        'success':True,
        'message':message,
        'results':results,
        'row_total':row_total
    })

def json_serial(obj):
    """JSON serializer for objects not serializable by default json code"""

    if isinstance(obj, (datetime.datetime, datetime.date)):
        return obj.isoformat()
    elif isinstance(obj, Decimal):
        return float(obj) 
    elif isinstance(obj, Enum):
        return str(obj)
    return obj

def TestonlyRead(args):
    return {"success":"true", "message":"test reached"}, 200

def TestonlyPost(data):
    return {"success":"true", "message":"test reached"}, 200

def TestonlyPut(data):
    return {"success":"true", "message":"test reached"}, 200


#-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_ para ejemplos robados de payroll miren service_old.py, lo borramos despues -_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

def con_center_get(params):
    
    if params.get('month', type= int) is None:
        return {"success": False, "message": "Se necesita especificar el mes"}, 500
    
    if params.get('year', type= int) is None:
        return {"success": False, "message": "Se necesita especificar el año"}, 500

    if params.get('account', type= int) is None:
        return {"success": False, "message": "Se necesita especificar la cuenta en sesión"}, 500
    try:

        newparams = {
            "month": params.get('month', type = int),
            "year": params.get('year', type = int)
            ,"account" : params.get('account', type =int)
        }

        query = """
        SELECT cc.id, cc.description, cc.active, cc.annual_budget, cc.current_balance, coalesce((
            SELECT SUM(art.cost_price * wmd.qty_article)
            FROM  """+INVENTORY_DBNAME+""".warehouse_movements wm
            JOIN  """+INVENTORY_DBNAME+""".warehouse_movements_detail wmd ON wmd.warehouse_movement_id = wm.id
            JOIN  """+INVENTORY_DBNAME+""".article art ON art.id = wmd.article_id
            WHERE wm.wm_coce_id = cc.id
			AND MONTH(wm.created_at) = :month
			AND YEAR(wm.created_at) = :year
            AND wm.wm_movement_type = "consumOutput"
        ), 0) as total_spent 
        FROM """+INVENTORY_DBNAME+""".consumption_center cc
        where account = :account 
        ORDER BY total_spent DESC
        """

        # WHERE 1=1 
        # AND (MONTH(cc.effective_start_date) <= :month AND :month <=  MONTH(cc.effective_end_date) )
        # AND (YEAR(cc.effective_start_date) <= :year AND :year <= YEAR(cc.effective_end_date) 

        res = db.session.execute(query, newparams)

        total_annual_budget= 0
        total_current_balance = 0
        total_total_spent = 0
        finalresult = []
        for row in res.fetchall():
            dictrow = dict(row)
            myrow = {}
            for i in dictrow:
                cur_value = dictrow[i]

########### para pruebas se comentarea el if de abajo (annual budget)
                if i == "annual_budget":
                    if cur_value is not None:
                        total_annual_budget += cur_value
                    else:
                        cur_value = 0

                if i == "current_balance":
                    if cur_value is not None:
                        total_current_balance += cur_value
                    else:
                        cur_value = 0

########### para pruebas se comentarea el if de abajo (total_spent)
                if i == "total_spent":
                    if cur_value is not None:
                        total_total_spent += cur_value
                    else:
                        cur_value = 0

                myrow[i]=json_serial(cur_value)
                
            finalresult.append(myrow)

        # prueba = [
        #     {"name": "DISPENSACION ATLANTICO", "total_spent": 33481875, "annual_budget" : 30000000 },
        #     {"name": "COORDINACION DE CIRUGIA", "total_spent": 30935861, "annual_budget" : 30000000 },
        #     {"name": "VIVA C.C COFCA-FOCA PLUS", "total_spent": 27937878, "annual_budget" : 30000000 },
        #     {"name": "DISPENSACION MAGDALENA", "total_spent": 27508733, "annual_budget" : 30000000 },
        #     {"name": "DISPENSACION BOLIVAR", "total_spent": 26312853, "annual_budget" : 30000000 },
        #     {"name": "SALA AZUL MALL PLAZA", "total_spent": 15105189, "annual_budget" : 30000000 },
        #     {"name": "EGLE by COFCA", "total_spent": 9110921, "annual_budget" : 30000000 },
        #     {"name": "CENTRAL DE ESTERILIZACION CIRUGIA", "total_spent": 7010557, "annual_budget" : 30000000 },
        #     {"name": "DISPENSACION GUAJIRA", "total_spent": 4519073, "annual_budget" : 30000000 },
        #     {"name": "CIRUGIA FOCA SANTA MARTA", "total_spent": 4286642, "annual_budget" : 30000000 },
        #     {"name": "CONSULTORIOS FOCA SANTA MARTA", "total_spent": 4233354, "annual_budget" : 30000000 },
        #     {"name": "CONSULTA FOCA 2DO PISO  MARY LUZ ", "total_spent": 3561254, "annual_budget" : 30000000 },
        #     {"name": "AYUDA DIAGNOSTICA  ROSA MARTINEZ", "total_spent": 3465256, "annual_budget" : 30000000 },
        #     {"name": "OPTICA 51B", "total_spent": 2610000, "annual_budget" : 30000000 },
        #     {"name": "CONSULTA FOCA NORTE", "total_spent": 1865649, "annual_budget" : 30000000 },
        #     {"name": "VACUNACION COVID-19", "total_spent": 1746552, "annual_budget" : 30000000 },
        #     {"name": "PRUEBAS COVID FOCA SANTA MARTA", "total_spent": 500000, "annual_budget" : 30000000 },
        #     {"name": "FACTURACION   ERIKA RODRIGUEZ", "total_spent": 401000, "annual_budget" : 30000000 },
        #     {"name": "BRIGADA   MERCADEO", "total_spent": 275500, "annual_budget" : 30000000 },
        #     {"name": "ADMISION DE CIRUGIA ENERLINDA", "total_spent": 161302, "annual_budget" : 30000000 },
        #     {"name": "RECURSOS HUMANOS BRENDA RODRIGUEZ", "total_spent": 173207, "annual_budget" : 30000000 },
        #     {"name": "OPTICA FOCA SANTA MARTA", "total_spent": 143400, "annual_budget" : 30000000 },
        #     {"name": "LABORATORIOS CASONA", "total_spent": 125050, "annual_budget" : 30000000 },
        #     {"name": "CASONA FRANCISCO", "total_spent": 121000, "annual_budget" : 30000000 },
        #     {"name": "LINEA DE FRENTE FOCA SANTA MARTA", "total_spent": 78880, "annual_budget" : 30000000 },
        #     {"name": "DIRECCION DE CALIDAD MARIA PIZARRO", "total_spent": 57150, "annual_budget" : 30000000 },
        #     {"name": "SECRETARIA DIRECCION MEDICA", "total_spent": 53015, "annual_budget" : 30000000 },
        #     {"name": "CIRVO", "total_spent": 54500, "annual_budget" : 30000000 },
        #     {"name": "SALA AZUL SANTA MARTA", "total_spent": 39000, "annual_budget" : 30000000 },
        #     {"name": "TAMIZAJE FOCA SANTA MARTA", "total_spent": 33600, "annual_budget" : 30000000 },
        #     {"name": "AUXILIAR FARMACIA", "total_spent": 27700, "annual_budget" : 30000000 },
        #     {"name": "FARMACIA COFCA", "total_spent": 18300, "annual_budget" : 30000000 },
        #     {"name": "EQUIPOS BIOMEDICOS   - ROBIN YUDEX", "total_spent": 14000, "annual_budget" : 30000000 },
        #     {"name": "FACTURACION FOCA SANTA MARTA", "total_spent": 14520, "annual_budget" : 30000000 },
        #     {"name": "CONSERJERIA FOCA SANTA MARTA", "total_spent": 25200, "annual_budget" : 30000000 },
        #     {"name": "FARMACIA FOCA SEDE SANTA MARTA", "total_spent": 16800, "annual_budget" : 30000000 },
        #     {"name": "ASISTENTE DE GERENCIA", "total_spent": 10900, "annual_budget" : 30000000 },
        #     {"name": "PROGRAMACION CIRUGIA FOCA SANTA MARTA", "total_spent": 16800, "annual_budget" : 30000000 },
        #     {"name": "CAFETERIA Y ASEO FOCA SANTA MARTA", "total_spent": 12600, "annual_budget" : 30000000 },
        #     {"name": "SISTEMAS FOCA SANTA MARTA", "total_spent": 9400, "annual_budget" : 30000000 },
        #     {"name": "SIAU FOCA SANTA MARTA", "total_spent": 8400, "annual_budget" : 30000000 },
        #     {"name": "PROGRAMACION DE CIRUGIA MERCEDES CASTRO", "total_spent": 6400, "annual_budget" : 30000000 }
        # ]
        # total_total_spent = 206089271
        percentaje = 0;
        annual_budget_otros =  0;
        total_spent_otros =  0;
        labels = []
        annual_budget = []
        total_spent = []
        #for row in prueba:
        for row in finalresult:
            if(total_total_spent == 0):
                percentaje =  100
            if(percentaje >= 80):
                annual_budget_otros += row.get('annual_budget')
                total_spent_otros += row.get('total_spent')                
            else:
                #labels.append(row.get('name'))
                labels.append(row.get('description'))
                annual_budget.append(row.get('annual_budget'))
                total_spent.append(row.get('total_spent'))
            if(total_total_spent != 0):
                percentaje =  percentaje + (row.get('total_spent') / total_total_spent * 100)
        labels.append('OTROS')
        annual_budget.append(annual_budget_otros)
        total_spent.append(total_spent_otros)
        tempito = []
        tempito.append({"name" : "Presupuesto anual", "data" :annual_budget})
        tempito.append({"name" : "Presupuesto consumido", "data" :total_spent})
        temp = {"labels" :labels,"series" : tempito}            
        #total_annual_budget = 4200000000
        total_percentage = round(total_current_balance/total_annual_budget *100, 2)
        dashboard = {
            "labels" : labels,
            "data" : total_spent
        }
        return {
            "success":True, 
            "results": temp,
            "total_annual_budget": total_annual_budget,
            "total_current_balance": total_current_balance,
            "total_total_spent": total_total_spent,
            "total_percentage": total_percentage
            ,"dashboard" : dashboard
            }, 200
    except Exception as e:
        return responseError(str(e), 500)
    finally:
        db.session.close()

def consumption_center_purchases(params):
    if params.get('cc_id', type= int) is None:
        return {"success": False, "message": "Se necesita una id de centro de consumo"}, 500
    try:

        newparams = {
            "cc_id": params.get('cc_id', type= int)
        }

        query = """
        SELECT art.id, 
        SUM(COALESCE(wmd.qty_article, 0)) as total_qty_article, 
        art.description, 
        art.cost_price as cost_per_unit, 
        art.average_cost
        FROM   """+INVENTORY_DBNAME+""".warehouse_movements wm
        JOIN   """+INVENTORY_DBNAME+""".warehouse_movements_detail wmd ON wmd.warehouse_movement_id = wm.id
        JOIN   """+INVENTORY_DBNAME+""".article art ON art.id = wmd.article_id
        WHERE wm.wm_coce_id = :cc_id
        GROUP BY art.id
        """

        res = db.session.execute(query, newparams)

        finalresult = []
        for row in res.fetchall():
            dictrow = dict(row)
            myrow = {}
            for i in dictrow:
                if (i == "details") and dictrow[i] is not None:
                    myrow[i]=json.loads( dictrow[i]) 
                else:
                    myrow[i]=json_serial(dictrow[i])
            finalresult.append(myrow)

        return {"success":True, "results": finalresult}, 200
    except Exception as e:
        return responseError(e, 500)
    finally:
        db.session.close()
#-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_ PAYROLL INDICATORS -_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

#-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_ TOTAL NUMBER OF EMPLOYEES
def totalEmployees(args):
    try:
        account = args.get("entity_account")
        month = args.get("month")
        year = args.get("year")
        if not account:
            return {"success": False, "message": "El atributo cuenta en sesión es obligatorio"}, 400
        
        if not month:
            return {"success": False, "message": "El atributo mes es obligatorio"}, 400
        
        if not year:
            return {"success": False, "message": "El atributo año es obligatorio"}, 400

        query ="""
        SELECT
        count(c.id) as totalEmployee
        FROM """+PAYROLL_DBNAME+""".`candidates` AS c
        JOIN """+PAYROLL_DBNAME+""".`contracts` con ON con.candidate = c.id
        WHERE (start_date >= '"""+ str(year) +"""-""" +"""01-01' AND  end_date <= LAST_DAY('"""+ str(year) +"""-"""+ str(month) +"""-01')) AND 
        con.status = 'current' and con.entity_account =:account;
        """

        queryresult = db.session.execute(query, {'account': account}).fetchone()
        return { 'result': queryresult.totalEmployee, 'success': True }

    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        db.session.rollback()
        return {'success': False, 'message': e },400
    finally:
        db.session.close()

#-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_ ACCIDENT RATE
def accidentRate(args):
    try:
        account = args.get("entity_account")
        month = args.get("month")
        year = args.get("year")
        if not account:
            return {"success": False, "message": "El atributo cuenta en sesión es obligatorio"}, 400
        
        if not month:
            return {"success": False, "message": "El atributo mes es obligatorio"}, 400
        
        if not year:
            return {"success": False, "message": "El atributo año es obligatorio"}, 400

        query ="""
        SELECT
        (count(w.id)/( """+ str(month) +""" *30))*100 as accidentsRate
        FROM """+PAYROLL_DBNAME+""".`work_accidents` AS w
        WHERE (accident_date  BETWEEN '"""+ str(year) +"""-""" +"""01-01' AND  LAST_DAY('"""+ str(year) +"""-"""+ str(month) +"""-01')) AND 
        w.entity_account =:account;
        ;
        """

        queryresult = db.session.execute(query, {"account": account }).fetchone()
        return { 'result': float(round(queryresult.accidentsRate, 2)), 'success': True }

    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        db.session.rollback()
        return {'success': False, 'message': e },400
    finally:
        db.session.close()

#-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_ RESUME PREVIEW
def resumePreview(args):
    try:
        account = args.get("entity_account")
        month = args.get("month")
        year = args.get("year")
        if not account:
            return {"success": False, "message": "El atributo cuenta en sesión es obligatorio"}, 400
        
        if not month:
            return {"success": False, "message": "El atributo mes es obligatorio"}, 400
        
        if not year:
            return {"success": False, "message": "El atributo año es obligatorio"}, 400

        query ="""
        SELECT
        count(DISTINCT CASE WHEN COALESCE(can.percentage,0)=100 THEN can.id ELSE NULL END) completo,
        count(DISTINCT CASE WHEN COALESCE(can.percentage,0)<>100 THEN can.id ELSE NULL END) incompleto
        FROM """+PAYROLL_DBNAME+""".`candidates` AS can
        JOIN """+PAYROLL_DBNAME+""".`contracts` AS con ON con.candidate = can.id
        WHERE (con.status in ('signed','current','pending'))
        AND
        (can.created_at  BETWEEN '"""+ str(year) +"""-""" +"""01-01' AND  LAST_DAY('"""+ str(year) +"""-"""+ str(month) +"""-01')) AND 
        can.entity_account =:account;
        ;
        """
        queryresult = db.session.execute(query, {"account": account }).fetchall()
        data = []
        for row in queryresult:
            data.append(row.completo)
            data.append(row.incompleto)
        

        return { 'data': data, 'labels' : ["Completadas", "Sin completar"],'success': True }

    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        db.session.rollback()
        return {'success': False, 'message': e },400
    finally:
        db.session.close()

#-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_ ABSENTEEISM
def absenteeism(args):
    try:
        account = args.get("entity_account")
        month = args.get("month")
        year = args.get("year")
        if not account:
            return {"success": False, "message": "El atributo cuenta en sesión es obligatorio"}, 400
        
        if not month:
            return {"success": False, "message": "El atributo mes es obligatorio"}, 400
        
        if not year:
            return {"success": False, "message": "El atributo año es obligatorio"}, 400

        query ="""
        SELECT inhabilities, type
        FROM (
            SELECT "Incapacidades" as type, count(inh.id) as inhabilities
            FROM """+PAYROLL_DBNAME+""".inhabilities as inh
            WHERE 
            (inh.status='approved')
            AND
            (inh.entity_account =:account)
            AND
            (inh.start_date >= '"""+ str(year) +"""-""" +"""01-01' AND  inh.end_date <=  LAST_DAY('"""+ str(year) +"""-"""+ str(month) +"""-01'))

            UNION ALL

            SELECT  "Permisos" as type, count(p.id) as permissions
            FROM """+PAYROLL_DBNAME+""".permissions as p
            WHERE 
            p.status='approved'
            AND
            (p.entity_account =:account)
            AND
            (p.start_date >= '"""+ str(year) +"""-""" +"""01-01' AND  p.end_date <=  LAST_DAY('"""+ str(year) +"""-"""+ str(month) +"""-01'))
        ) Absenteeism;
        """
        queryresult = db.session.execute(query, {"account": account }).fetchall()
        data = []
        labels = []
        for row in queryresult:
            data.append(row.inhabilities)
            labels.append(row.type)

        return { 'data': data, 'labels' : labels,'success': True }

    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        db.session.rollback()
        return {'success': False, 'message': e },400
    finally:
        db.session.close()

#-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_ Total Payroll Investment
def totalPayrollInvestment(args):
    try:
        account = args.get("entity_account")
        month = args.get("month")
        year = args.get("year")
        if not account:
            return {"success": False, "message": "El atributo cuenta en sesión es obligatorio"}, 400
        
        if not month:
            return {"success": False, "message": "El atributo mes es obligatorio"}, 400
        
        if not year:
            return {"success": False, "message": "El atributo año es obligatorio"}, 400

        query ="""
        SELECT m.name AS month, SUM((COALESCE(total_amount,0))) AS totalMonth
        FROM """+ PAYROLL_DBNAME +""".month_ES AS m
        LEFT JOIN """+ PAYROLL_DBNAME +""".payroll_sheet p ON (m.id = CAST(p.payment_month AS FLOAT) AND p.status = 'processed' AND p.payment_year = '"""+ str(year) + """' AND p.entity_account=:account)
        WHERE (m.id <= CAST( :month AS FLOAT)) 
        GROUP BY m.name;
        """
        queryresult = db.session.execute(query, {"account": account , "month": month}).fetchall()
        data = { "name": "Valor", "data": []}
        labels = []
        for row in queryresult:
            data["data"].append(float(row.totalMonth))
            labels.append(row.month)

        return { 'data': [data], 'labels' : labels,'success': True }

    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        db.session.rollback()
        return {'success': False, 'message': e },400
    finally:
        db.session.close()

#-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_ Invoicing By Type
def invoicingByType(args):
    try:
        account = args.get("entity_account")
        month = args.get("month")
        year = args.get("year")
        if not account:
            return {"success": False, "message": "El atributo cuenta en sesión es obligatorio"}, 400
        
        if not month:
            return {"success": False, "message": "El atributo mes es obligatorio"}, 400
        
        if not year:
            return {"success": False, "message": "El atributo año es obligatorio"}, 400

        queryCapita ="""
        SELECT  m.name AS month, SUM((COALESCE(inv.total_amount,0))) AS totalMonth 
        FROM """+PAYROLL_DBNAME+""".month_ES AS m
        LEFT JOIN `mod-invoicing-core-dev`.invoices inv ON (m.id = CAST(Month(inv.exp_date) AS FLOAT) AND inv.inv_type='1' AND CAST(Year(inv.exp_date) AS FLOAT)=2022)
        WHERE (m.id <= CAST( :month AS FLOAT)) 
        """
        queryCapitaresult = db.session.execute(queryCapita, {"account": account , "month": month, "year": year}).fetchall()

        queryLibre ="""
        SELECT  m.name AS month, SUM((COALESCE(inv.total_amount,0))) AS totalMonth 
        FROM """+PAYROLL_DBNAME+""".month_ES AS m
        LEFT JOIN `mod-invoicing-core-dev`.invoices inv ON (m.id = CAST(Month(inv.exp_date) AS FLOAT) AND inv.inv_type='2' AND CAST(Year(inv.exp_date) AS FLOAT)=2022)
        WHERE (m.id <= CAST( :month AS FLOAT))
        """
        queryLibreresult = db.session.execute(queryLibre, {"account": account , "month": month}).fetchall()

        data1 = { "name": "Capita", "data": []}
        data2 = { "name": "Libre", "data": []}

        for row in queryCapitaresult:
            data1["data"].append(float(row.totalMonth))
        
        for row in queryLibreresult:
            data2["data"].append(float(row.totalMonth))


        return { 'data': [data1, data2], 'labels' : [data1["name"], data2["name"]],'success': True }

    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        db.session.rollback()
        return {'success': False, 'message': e },400
    finally:
        db.session.close()

#-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_ Filed Vs Unfiled Invoices
def filedVsUnfiledInvoices(args):
    try:
        account = args.get("entity_account")
        month = args.get("month")
        year = args.get("year")
        if not account:
            return {"success": False, "message": "El atributo cuenta en sesión es obligatorio"}, 400
        
        if not month:
            return {"success": False, "message": "El atributo mes es obligatorio"}, 400
        
        if not year:
            return {"success": False, "message": "El atributo año es obligatorio"}, 400

        query="""
        SELECT  
        st.value as Type,
        SUM(inv.total_amount) as Total,
        COUNT(inv.id) as Amount
        FROM """+INVOICING_DBNAME+""".invoices AS inv
        JOIN """+INVENTORY_DBNAME+""".status AS st ON st.name = inv.filing_status AND st.prefix='invoiceFil'
        WHERE (inv.exp_date BETWEEN '"""+ str(year) +"""-""" +"""01-01' AND  LAST_DAY('"""+ str(year) +"""-"""+ str(month) +"""-01'))
        GROUP BY inv.filing_status;
        """
        queryresult = db.session.execute(query, {"account": account}).fetchall()

        data = []
        labels= []
        total = []

        for row in queryresult:
            data.append(row.Amount)
            labels.append(row.Type)
            total.append(float(row.Total))
        
        return { 'data': data, 'labels' : labels, 'total' : total, 'success': True }

    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        db.session.rollback()
        return {'success': False, 'message': e },400
    finally:
        db.session.close()

#-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_ Unfiled invoices
def unfiledInvoices(args):
    try:
        account = args.get("entity_account")
        month = args.get("month")
        year = args.get("year")
        if not account:
            return {"success": False, "message": "El atributo cuenta en sesión es obligatorio"}, 400
        
        if not month:
            return {"success": False, "message": "El atributo mes es obligatorio"}, 400
        
        if not year:
            return {"success": False, "message": "El atributo año es obligatorio"}, 400

        query="""
        SELECT  
        client.copyName as Client,
        SUM(inv.total_amount) as Total,
        count(inv.id) as Amount
        FROM """+INVOICING_DBNAME+""".invoices AS inv
        JOIN """+ADMINISTRATION_DBNAME+""".coporateClients AS client ON client.id= inv.corporate_client
        where inv.filing_status = 'unfiled' AND
        (inv.exp_date BETWEEN '"""+ str(year) +"""-""" +"""01-01' AND  LAST_DAY('"""+ str(year) +"""-"""+ str(month) +"""-01'))
        group by client.id
        """
        queryresult = db.session.execute(query, {"account": account}).fetchall()

        data = []
        labels= []
        total = []

        finalResult = []
        percentage = 0
        total_data = 0
        data_otros = 0
        total_otros = 0
        for row in queryresult:
            dictrow = dict(row)
            myrow = {}
            for i in dictrow:
                myrow[i]=json_serial(dictrow[i])
            finalResult.append(myrow)
            total_data += myrow.get('Amount')
        for row in finalResult:
            if(total_data == 0):
                percentage =  100
            if percentage >= 80:
                data_otros += row.get('Amount')
                total_otros += row.get('Total')
            else:
                data.append(row.get('Amount'))
                labels.append(row.get('Client'))
                total.append(row.get('Total'))
            if(total_data != 0):
                percentage =  percentage + (row.get('Amount') / total_data * 100)
        labels.append('OTROS')
        data.append(data_otros)
        total.append(total_otros)
        
        return { 'data': data, 'labels' : labels, 'total' : total, 'success': True }

    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        db.session.rollback()
        return {'success': False, 'message': e },400
    finally:
        db.session.close()


def warehouse_problems(params):
    
    try:
        if params.get('month_start') is None:
            return responseError("Se necesita especificar el mes inicial", 500)
        if params.get('month_end') is None:
            return responseError("Se necesita especificar el mes final", 500)
        if params.get('year_start') is None:
            return responseError("Se necesita especificar el año inicial", 500)
        if params.get('year_end') is None:
            return responseError("Se necesita especificar el año final", 500)
        if params.get('account') is None:
            return responseError("Se necesita especificar la cuenta en sesión", 500)

        newparams = {
            "month_start": params.get('month_start', type = int),
            "month_end": params.get('month_end', type = int),
            "year_start": params.get('year_start', type = int),
            "year_end": params.get('year_end', type = int),
            "account" : params.get('account')
        }
        query = """
        SELECT wep.*, 
        (
            SELECT count(1) 
            FROM  """+INVENTORY_DBNAME+""".warehouse_entry we 
            WHERE wep.id = we.we_problem
        """

        if params.get('month_start', type= int) is not None:
            query += " AND MONTH(we.entry_date) >= :month_start "

        if params.get('month_end', type= int) is not None:
            query += " AND MONTH(we.entry_date) <= :month_end "

        if params.get('year_start', type= int) is not None:
            query += " AND YEAR(we.entry_date) >= :year_start "

        if params.get('year_end', type= int) is not None:
            query += " AND YEAR(we.entry_date) <= :year_end "

        if params.get('account', type= int) is not None:
            query += " AND eaccount = :account "
        query += """
        ) as count
        FROM  """+INVENTORY_DBNAME+""".we_entry_problems wep
        ORDER BY count DESC
        """

        res = db.session.execute(query, newparams)

        labels = []
        data = []
        for row in res.fetchall():
            labels.append(row.value)
            data.append(row.count)
        temp = {
            "data"  : data,
            "labels" : labels
        }

        return {
            "success":True, 
            "results": temp
        }, 200

    except Exception as e:
        return responseError(e, 500)
    finally:
        db.session.close()
#-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_ Total receivables
def totalReceivables(args):
    try:
        account = args.get("entity_account")
        month = args.get("month")
        year = args.get("year")
        if not account:
            return {"success": False, "message": "El atributo cuenta en sesión es obligatorio"}, 400
        
        if not month:
            return {"success": False, "message": "El atributo mes es obligatorio"}, 400
        
        if not year:
            return {"success": False, "message": "El atributo año es obligatorio"}, 400

        query="""
        SELECT  
        SUM(inv.pending_amount) AS total
        FROM """+INVOICING_DBNAME+""".invoices AS inv
        WHERE
        (inv.exp_date BETWEEN '"""+ str(year) +"""-""" +"""01-01' AND  LAST_DAY('"""+ str(year) +"""-"""+ str(month) +"""-01'))
        """
        queryresult = db.session.execute(query).fetchone()
        
        return { 'result': float(queryresult.total) ,'success': True }

    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        db.session.rollback()
        return {'success': False, 'message': e },400
    finally:
        db.session.close()


#-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_ Portfolio by customer company
def portfolioByCustomer(args):
    try:
        account = args.get("entity_account")
        month = args.get("month")
        year = args.get("year")
        if not account:
            return {"success": False, "message": "El atributo cuenta en sesión es obligatorio"}, 400
        
        if not month:
            return {"success": False, "message": "El atributo mes es obligatorio"}, 400
        
        if not year:
            return {"success": False, "message": "El atributo año es obligatorio"}, 400

        query="""
        SELECT  
        client.copyName as Client,
        COUNT(inv.id) as Amount,
        SUM(inv.total_amount) AS Total
        FROM """+INVOICING_DBNAME+""".invoices AS inv
        JOIN """+ADMINISTRATION_DBNAME+""".coporateClients AS client ON client.id= inv.corporate_client
        WHERE
        (inv.exp_date BETWEEN '"""+ str(year) +"""-""" +"""01-01' AND  LAST_DAY('"""+ str(year) +"""-"""+ str(month) +"""-01'))
        group by client.id
        """
        queryresult = db.session.execute(query).fetchall()
        data = []
        labels= []
        total = []

        finalResult = []
        percentage = 0
        total_data = 0
        data_otros = 0
        total_otros = 0
        for row in queryresult:
            dictrow = dict(row)
            myrow = {}
            for i in dictrow:
                myrow[i]=json_serial(dictrow[i])
            finalResult.append(myrow)
            total_data += myrow.get('Amount')
        for row in finalResult:
            if(total_data == 0):
                percentage =  100
            if percentage >= 80:
                data_otros += row.get('Amount')
                total_otros += row.get('Total')
            else:
                data.append(row.get('Amount'))
                labels.append(row.get('Client'))
                total.append(row.get('Total'))
            if(total_data != 0):
                percentage =  percentage + (row.get('Amount') / total_data * 100)
        labels.append('OTROS')
        data.append(data_otros)
        total.append(total_otros)
        
        return { 'data': data, 'labels' : labels, 'total' : total,'success': True }

    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        db.session.rollback()
        return {'success': False, 'message': e },400
    finally:
        db.session.close()

def insumos_vencidos(params):

    if params.get('year', type= int) is None:
        return {"success": False, "message": "Se necesita especificar el año"}, 500

    try:

        newparams = {
            "year": params.get('year', type = int)
        }

        query = """
            SELECT m.id, m.name_es, 
            (
                SELECT SUM(l.lot_qty_received) 
                FROM  """+INVENTORY_DBNAME+""".lots l 
                WHERE MONTH(l.lot_date_expiration) = m.id
                AND YEAR(l.lot_date_expiration) = :year
            ) as count
            FROM  """+INVENTORY_DBNAME+""".months m
            ORDER BY m.id
        """

        querytotal = """
            SELECT SUM(l.lot_qty_received) 
            FROM  """+INVENTORY_DBNAME+""".lots l 
            WHERE YEAR(l.lot_date_expiration) = :year
        """

        res = db.session.execute(query, newparams)
        res_total = db.session.execute(querytotal, newparams).scalar()

        finalresult = []
        labels =[]
        data =[]
        series=[]
        for row in res.fetchall():
            dictrow = dict(row)
            myrow = {}
            for i in dictrow:
                tempvalue = dictrow[i]
                if i == "count":

                    percentage=0
                    if tempvalue is not None:
                        percentage = round(tempvalue / res_total * 100 , 2)
                    else:
                        tempvalue=0
                    myrow["percentage"]= json_serial(percentage)
                    

                myrow[i]=json_serial(tempvalue)
            finalresult.append(myrow)
            labels.append(row.name_es)
            data.append(float(percentage))
        series.append({"name": "Insumos vencidos", "data": data})
        


        return {
            "success":True,
            "labels": labels,
            "series":series,
            "total": json_serial(res_total),
        }, 200

    except Exception as e:
        return responseError(e, 500)
    finally:
        db.session.close()
    
#-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_ Current Total Provisioned
def currentTotalProvisioned(args):
    try:
        account = args.get("entity_account")
        month = args.get("month")
        year = args.get("year")
        day = args.get("day")
        if not account:
            return {"success": False, "message": "El atributo cuenta en sesión es obligatorio"}, 400
        
        if not month:
            return {"success": False, "message": "El atributo mes es obligatorio"}, 400
        
        if not year:
            return {"success": False, "message": "El atributo año es obligatorio"}, 400
        
        if not day:
            return {"success": False, "message": "El atributo día es obligatorio"}, 400

        query="""
        SELECT  
        sum(det.total_amount) as Total
        FROM """+ RECEIVABLE_DBNAME +""".deterioration_provision as det
        WHERE
        (det.det_date BETWEEN '"""+ str(year) +"""-""" +"""01-01' AND  '"""+ str(year) +"""-"""+ str(month) + """-"""+ str(day) +"""')
        """
        queryresult = db.session.execute(query).fetchone()

        return { 'data': float(queryresult.Total), 'success': True }

    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        db.session.rollback()
        return {'success': False, 'message': e },400
    finally:
        db.session.close()


#-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_ Portfolio By Age
def portfolioByAge(args):
    try:
        account = args.get("entity_account")
        if not account:
            return {"success": False, "message": "El atributo cuenta en sesión es obligatorio"}, 400

        query="""
        SELECT  
        
        """
        queryresult = db.session.execute(query).fetchall()

        return { 'data': float(queryresult.Total), 'success': True }

    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        db.session.rollback()
        return {'success': False, 'message': e },400
    finally:
        db.session.close()

#-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_ Details of account balances 
def detailsOfAccountBalances(args):
    try:
        account = args.get("entity_account")
        month = args.get("month")
        year = args.get("year")
        id_client =  args.get("id_client")
        if not account:
            return {"success": False, "message": "El atributo cuenta en sesión es obligatorio"}, 400
        
        if not month:
            return {"success": False, "message": "El atributo mes es obligatorio"}, 400
        
        if not year:
            return {"success": False, "message": "El atributo año es obligatorio"}, 400
        
        if not id_client:
            return {"success": False, "message": "El atributo id del cliente es obligatorio"}, 400

        query="""
        SELECT  
        n.name_bank,
        n.num_count,
	    n.account_type,
	    'Saldo del mes anterior' details,
        DATE_ADD(DATE(CONCAT('"""+ str(year) +"""-"""+ str(month) +"""-01')), INTERVAL -1 day) as date,
	    CAST(SUM(CASE WHEN n.nature = 'D' THEN jd.debitAmount-jd.creditAmount ELSE jd.creditAmount-jd.debitAmount END) AS DECIMAL) valor
        FROM """+ ADMINISTRATION_DBNAME +""".niifAccount n
	    INNER JOIN """+ ACCOUNTING_DBNAME +""".journal_detail jd ON (jd.niifAccount = n.id)
	    INNER JOIN """+ ACCOUNTING_DBNAME +""".journal_voucher jv ON (jv.id= jd.jrnl_id)
        WHERE check_info_bank = 1 AND n.entity_account =:account
        AND jv.transaction_date < DATE('"""+ str(year) +"""-"""+ str(month) +"""-01')
        AND  n.id =:id_client
        GROUP BY n.id
        UNION ALL
        SELECT
        n.name_bank,
        n.num_count,
        n.account_type,
        jv.details,
        jv.transaction_date,
        CAST(CASE WHEN n.nature = 'D' THEN jd.debitAmount-jd.creditAmount ELSE jd.creditAmount-jd.debitAmount END AS DECIMAL) valor
        FROM """+ ADMINISTRATION_DBNAME +""".niifAccount n
	    INNER JOIN """+ ACCOUNTING_DBNAME +""".journal_detail jd ON (jd.niifAccount = n.id)
	    INNER JOIN """+ ACCOUNTING_DBNAME +""".journal_voucher jv ON (jv.id= jd.jrnl_id)
        WHERE check_info_bank = 1 AND n.entity_account =:account
        AND YEAR(jv.transaction_date)=:year AND MONTH(jv.transaction_date)=:month
        AND n.id =:id_client;
        """
        queryresult = db.session.execute(query, {"account": account, "month": month, "year": year, "id_client": id_client})
        finalResult = []
        for row in queryresult.fetchall():
            dictrow = dict(row)
            myrow = {}
            for i in dictrow:
                myrow[i]=json_serial(dictrow[i])
            finalResult.append(myrow)


        return { "result": finalResult, 'success': True }

    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        db.session.rollback()
        return {'success': False, 'message': e },400
    finally:
        db.session.close()

def indicador_facturacion(params):

    if params.get('month', type= int) is None:
        return {"success": False, "message": "Se necesita especificar el mes"}, 500
    
    if params.get('year', type= int) is None:
        return {"success": False, "message": "Se necesita especificar el año"}, 500

    if params.get('account', type= int) is None:
        return {"success": False, "message": "Se necesita especificar la cuenta en sesión"}, 500
    
    try:

        newparams = {
            "month": params.get('month', type = int),
            "year": params.get('year', type = int),
            "account" : params.get('account', type = int)
        }

        # query_types = """
        # SELECT it.*, (
        #     SELECT SUM(i.total_amount) 
        #     FROM """+INVOICING_DBNAME+""".invoices i
        #     WHERE i.inv_type = it.id
        #     AND MONTH(i.exp_date) = :month
        #     AND YEAR(i.exp_date) = :year
        # ) AS total_val
        # FROM """+INVOICING_DBNAME+""".invoice_types it
        # """

        query_base = """
            SELECT cc.id, cc.copyName, 
            COALESCE(SUM(i.total_amount), 0) as total
            FROM """+ADMINIS_DBNAME+""".coporateClients cc 
            JOIN """+INVOICING_DBNAME+""".invoices i ON cc.id = i.corporate_client
            WHERE i.inv_type = :type
            AND MONTH(i.exp_date) = :month
            AND YEAR(i.exp_date) = :year
            AND cc.id_account = :account
            GROUP BY cc.id
        """

        newparams["type"]=1
        total_capita = 0
        result_capita = db.session.execute(query_base, newparams)
        final_capita = []
        for row in result_capita.fetchall():
            dictrow = dict(row)
            myrow = {}
            for i in dictrow:
                if i == "total":
                    total_capita += json_serial(dictrow[i])
                myrow[i]=json_serial(dictrow[i])
            final_capita.append(myrow)

        newparams["type"]=2
        total_libre = 0
        result_libre = db.session.execute(query_base, newparams)
        final_libre = []
        for row in result_libre.fetchall():
            dictrow = dict(row)
            myrow = {}
            for i in dictrow:
                if i == "total":
                    total_libre += json_serial(dictrow[i])
                myrow[i]=json_serial(dictrow[i])
            final_libre.append(myrow)



        query_behaviour = """
            SELECT 
            m.name AS month, 
            SUM((COALESCE(i.total_amount,0))) AS totalMonth
            FROM """+PAYROLL_DBNAME+""".month_ES AS m
            LEFT JOIN """+INVOICING_DBNAME+""".invoices i ON (m.id = MONTH(i.exp_date) AND year(i.exp_date)=:year)
            LEFT JOIN """+ADMINISTRATION_DBNAME+""".coporateClients cc on cc.id = i.corporate_client and cc.id_account = :account
            GROUP BY m.name
        """
        final_behaviour=[]
        data = []
        labels = []
        result_behaviour = db.session.execute(query_behaviour, newparams)
        for row in result_behaviour.fetchall():
            data.append(float(row.totalMonth))
            labels.append(row.month)        
        temp = {
            "name" : "Comportamiento de lo facturado",
            "data" : data
        }
        final_behaviour.append(temp)


        return {
            "success": True,

            "final_capita": final_capita,
            "total_capita": total_capita,

            "final_libre": final_libre,
            "total_libre": total_libre,

            "final_behaviour": final_behaviour
            ,"labels" : labels
        }, 200
    except Exception as e:
        return responseError(e, 500)
    finally:
        db.session.close()
        





#-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_ Provisioned vs collected "Pie diagram"
def provisionedVSCollected(args):
    try:
        account = args.get("entity_account")
        month = args.get("month")
        year = args.get("year")
        day = args.get("day")
        if not account:
            return {"success": False, "message": "El atributo cuenta en sesión es obligatorio"}, 400
        
        if not month:
            return {"success": False, "message": "El atributo mes es obligatorio"}, 400
        
        if not year:
            return {"success": False, "message": "El atributo año es obligatorio"}, 400
        
        query="""
        SELECT provisioned, type
         FROM (
            SELECT "Provisionado" as type, COALESCE(sum(pro.total_amount),0) as provisioned
            FROM """+ RECEIVABLE_DBNAME +""".deterioration_provision as pro
            INNER JOIN (SELECT DISTINCT pro_inv.det_pro_id, cor.id_account FROM """+ RECEIVABLE_DBNAME +""".deterioration_provision_invoices as pro_inv
            INNER JOIN """+ INVOICING_DBNAME +""".invoices as inv ON (inv.id = pro_inv.invoice_id)
            INNER JOIN """+ ADMINISTRATION_DBNAME +""".coporateClients as cor ON (cor.id = inv.corporate_client)) AS rel  ON (rel.det_pro_id = pro.id)
			WHERE rel.id_account =:account 
            AND
            (pro.det_date BETWEEN '"""+ str(year) +"""-""" +"""01-01' AND  LAST_DAY('"""+ str(year) +"""-"""+ str(month) + """-"""+ str(day) +"""'))


            UNION ALL

            SELECT  "Recaudado" as type, COALESCE(sum(dep.amount),0) as collected
            FROM  """+ TREASURY_DBNAME +""".corporate_deposit as dep
            WHERE 
            dep.status <> 1
            AND
            (dep.entity_account =:account )
            AND
            (dep.transaction_date BETWEEN '"""+ str(year) +"""-""" +"""01-01' AND  LAST_DAY('"""+ str(year) +"""-"""+ str(month) + """-"""+ str(day) +"""'))

        ) Comparative;
        """
        queryresult = db.session.execute(query, {"account": account, "month": month, "year": year})
        data = []
        labels= []

        for row in queryresult:
            data.append(row.provisioned)
            labels.append(row.type)
        
        return { 'data': data, 'labels' : labels, 'success': True }

    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        db.session.rollback()
        return {'success': False, 'message': e },400
    finally:
        db.session.close()


#-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_ Provisioned vs collected "Bar chart"
def provisionedVSCollectedByClient(args):
    try:
        account = args.get("entity_account")
        year = args.get("year")
        client =args.get("client")
        if not account:
            return {"success": False, "message": "El atributo cuenta en sesión es obligatorio"}, 400

        if not year:
            return {"success": False, "message": "El atributo año es obligatorio"}, 400


        if client =="all" or client=="":
            query1="""
            SELECT m.name AS month, SUM((COALESCE(dep.amount,0))) AS totalMonth
            FROM  """+ PAYROLL_DBNAME +""".month_ES AS m
            LEFT JOIN  """+ TREASURY_DBNAME +""".corporate_deposit as dep ON (m.id = CAST(Month(dep.transaction_date) AS FLOAT) AND dep.entity_account=:account AND  CAST(YEAR(dep.transaction_date) AS FLOAT)= '"""+ str(year) +"""')
            GROUP BY m.name
            """

            query2="""
            SELECT m.name AS month, COALESCE(rel.total, 0) AS totalMonth
            FROM """+ PAYROLL_DBNAME +""".month_ES AS m
            LEFT JOIN """+ RECEIVABLE_DBNAME +""".deterioration_provision as pro  ON (m.id = CAST(Month(pro.det_date) AS FLOAT) AND  CAST(YEAR(pro.det_date) AS FLOAT)= '"""+ str(year) +"""')
            LEFT JOIN (SELECT DISTINCT pro_inv.det_pro_id, cor.id_account, cor.id, SUM((COALESCE(pro_inv.provision_amount,0))) as total FROM """+ RECEIVABLE_DBNAME +""".deterioration_provision_invoices as pro_inv
            INNER JOIN """+ INVOICING_DBNAME +""".invoices as inv ON (inv.id = pro_inv.invoice_id)
            INNER JOIN """+ ADMINISTRATION_DBNAME +""".coporateClients as cor ON (cor.id = inv.corporate_client)
            WHERE cor.id_account =:account) AS rel  ON (rel.det_pro_id = pro.id )
            GROUP BY m.name
            """


        else:
            query1="""
            SELECT m.name AS month, SUM((COALESCE(dep.amount,0))) AS totalMonth
            FROM  """+ PAYROLL_DBNAME +""".month_ES AS m
            LEFT JOIN  """+ TREASURY_DBNAME +""".corporate_deposit as dep ON (m.id = CAST(Month(dep.transaction_date) AS FLOAT) AND dep.entity_account=:account AND  CAST(YEAR(dep.transaction_date) AS FLOAT)='"""+ str(year) +"""') AND dep.corporate_client=:client
            GROUP BY m.name
            """
            query2="""
            SELECT m.name AS month, COALESCE(rel.total, 0) AS totalMonth
            FROM """+ PAYROLL_DBNAME +""".month_ES AS m
            LEFT JOIN """+ RECEIVABLE_DBNAME +""".deterioration_provision as pro  ON (m.id = CAST(Month(pro.det_date) AS FLOAT) AND  CAST(YEAR(pro.det_date) AS FLOAT)= '"""+ str(year) +"""')
            LEFT JOIN (SELECT DISTINCT pro_inv.det_pro_id, cor.id_account, cor.id, SUM((COALESCE(pro_inv.provision_amount,0))) as total FROM """+ RECEIVABLE_DBNAME +""".deterioration_provision_invoices as pro_inv
            INNER JOIN """+ INVOICING_DBNAME +""".invoices as inv ON (inv.id = pro_inv.invoice_id)
            INNER JOIN """+ ADMINISTRATION_DBNAME +""".coporateClients as cor ON (cor.id = inv.corporate_client)
            WHERE cor.id_account =:account AND cor.id=:client) AS rel  ON (rel.det_pro_id = pro.id )
            GROUP BY m.name
            """
        


        queryresult1 = db.session.execute(query1, {"account": account, "year": year, "client": client})
        queryresult2 = db.session.execute(query2, {"account": account, "year": year, "client": client})
        data1 = []
        labels1= []
        data2 = []

        for row in queryresult1:
            data1.append(row.totalMonth)
            labels1.append(row.month)
        
        for row in queryresult2:
            data2.append(row.totalMonth)
        
        
        return { 'series': [{"data":data1, "name": "Recaudado"}, {"data":data2, "name": "Provisionado"}], 'labels' : labels1, 'success': True }

    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        db.session.rollback()
        return {'success': False, 'message': e },400
    finally:
        db.session.close()

def gloss_objection_get(params):
    if params.get('month', type= int) is None:
        return {"success": False, "message": "Se necesita especificar el mes"}, 500
    
    if params.get('year', type= int) is None:
        return {"success": False, "message": "Se necesita especificar el año"}, 500

    if params.get('account', type= int) is None:
        return {"success": False, "message": "Se necesita especificar la cuenta en sesión"}, 500

    try:
        newparams = {
            "month": params.get('month', type = int),
            "year": params.get('year', type = int)
            ,"account" : params.get('account', type = int)
        }

        query_gloss = """
            SELECT ob.*
            ,(
                SELECT COALESCE(SUM(1), 0)  
                FROM """+INVOICING_DBNAME+""".objection_replies ob_r 
                WHERE ob_r.obj_id= ob.id
            ) as replies
            , IF (ob.date < DATE_ADD(CURDATE(), INTERVAL 15 DAY), 1, 0) as vencida
            FROM """+INVOICING_DBNAME+""".objections ob
            LEFT JOIN """+ADMINISTRATION_DBNAME+""".coporateClients cc on cc.id = ob.corporate_client and cc.id_account = :account
            WHERE MONTH(ob.date) = :month
            AND YEAR(ob.date)= :year
        """
        result_gloss = db.session.execute(query_gloss, newparams)
        gloss_array=[]
        total_value_registered = 0
        total_value_all = 0
        count_registered = 0
        count_expired = 0
        count_all= 0
        for row in result_gloss.fetchall():
            dictrow = dict(row)
            myrow = {}
            count_all +=1
            for i in dictrow:
                if i == "total_amount":
                    total_value_all += dictrow['total_amount'] or 0
                if i == "vencida":
                    count_expired += dictrow[i]
                if i == "status":
                    if dictrow[i] == "registered":
                        count_registered += 1
                        total_value_registered += dictrow['total_amount'] or 0

                myrow[i]=json_serial(dictrow[i])
            gloss_array.append(myrow)


        query_concept ="""
            SELECT ot.*, (
                SELECT COALESCE(SUM(1), 0) 
                FROM """+INVOICING_DBNAME+""".objections ob 
                LEFT JOIN """+ADMINISTRATION_DBNAME+""".coporateClients cc on cc.id = ob.corporate_client and cc.id_account = :account
                WHERE ob.obj_type =  ot.id
                AND MONTH(ob.date) = :month
                AND year(ob.date) = :year
            ) as count
            FROM """+INVOICING_DBNAME+""".objection_types ot;
        """
        result_concept = db.session.execute(query_concept, newparams)
        labels_concept=[]
        data_concept = []
        for row in result_concept.fetchall():
            labels_concept.append(row.obj_name)
            data_concept.append(int(row.count))
        query_behaviour = """
            SELECT 
            m.name_es AS month
            ,(
            SELECT COALESCE(SUM(ob.total_amount), 0) 
            FROM """+INVOICING_DBNAME+""".objections ob
            LEFT JOIN """+ADMINISTRATION_DBNAME+""".coporateClients cc on cc.id = ob.corporate_client and cc.id_account = :account
            WHERE ob.status = 'inprocess'
            AND MONTH(ob.date) = m.id
            AND YEAR(ob.date) = :year
            ) as inprocess
            ,(
            SELECT COALESCE(SUM(ob.total_amount), 0) 
            FROM """+INVOICING_DBNAME+""".objections ob
            LEFT JOIN """+ADMINISTRATION_DBNAME+""".coporateClients cc on cc.id = ob.corporate_client and cc.id_account = :account
            WHERE ob.status = 'accepted'
            AND MONTH(ob.date) = m.id
            AND YEAR(ob.date) = :year
            ) as accepted
            ,(
            SELECT COALESCE(SUM(ob.total_amount), 0) 
            FROM """+INVOICING_DBNAME+""".objections ob
            LEFT JOIN """+ADMINISTRATION_DBNAME+""".coporateClients cc on cc.id = ob.corporate_client and cc.id_account = :account
            WHERE ob.status = 'concilied'
            AND MONTH(ob.date) = m.id
            AND YEAR(ob.date) = :year
            ) as concilied
            ,(
            SELECT COALESCE(SUM(ob.total_amount), 0) 
            FROM """+INVOICING_DBNAME+""".objections ob
            LEFT JOIN """+ADMINISTRATION_DBNAME+""".coporateClients cc on cc.id = ob.corporate_client and cc.id_account = :account
            WHERE ob.status = 'registered'
            AND MONTH(ob.date) = m.id
            AND YEAR(ob.date) = :year
            ) as registered
            ,(
            SELECT COALESCE(SUM(ob.total_amount), 0) 
            FROM """+INVOICING_DBNAME+""".objections ob
            LEFT JOIN """+ADMINISTRATION_DBNAME+""".coporateClients cc on cc.id = ob.corporate_client and cc.id_account = :account
            WHERE ob.status = 'rejected'
            AND MONTH(ob.date) = m.id
            AND YEAR(ob.date) = :year
            ) as rejected
            FROM """+INVENTORY_DBNAME+""".months m
            GROUP BY m.id
        """
        result_behaviour = db.session.execute(query_behaviour, newparams)
        data_behaviour=[]
        labels_behaviour=[]
        data_accepted = []
        data_concilied = []
        data_rejected = []
        for row in result_behaviour.fetchall():
            labels_behaviour.append(row.month)
            data_accepted.append(int(row.accepted))
            data_concilied.append(int(row.concilied))
            data_rejected.append(int(row.rejected))
        temp1 = {
            "name" : "Aceptada",
            "data" : data_accepted
        }
        temp2 = {
            "name" : "Conciliada",
            "data" : data_concilied
            
        }
        temp3 = {
            "name" : "Rechazada",
            "data" : data_rejected
        }
        data_behaviour.append(temp1)
        data_behaviour.append(temp2)
        data_behaviour.append(temp3)
        glosas_gestionadas = []
        glosas_gestionadas.append(count_registered)
        glosas_gestionadas.append(count_all - count_registered)
        labels = ["Pendientes", "Gestionadas"]

        if count_all > 0:
            glosas_registradas=round(count_registered / count_all * 100 , 2)
            glosas_vencidas=round(count_expired / count_all * 100 , 2)
        else:
            glosas_registradas= 0
            glosas_vencidas= 0

        return {
            "success": True,

            "monto_total": json_serial(total_value_all),
            "monto_registrado": json_serial(total_value_registered),
            "glosas_registradas": glosas_registradas, 
            "glosas_vencidas": glosas_vencidas, 

            "data_gestionadas_vs_pendientes": glosas_gestionadas,
            "labels_gestionadas_vs_pendientes": labels,

            "data_segun_concepto": data_concept,
            "labels_segun_concepto": labels_concept,
            "comportamiento_glosas": data_behaviour,
            "labels_comportamiento" : labels_behaviour
        }, 200
    except Exception as e:
        return responseError(str(e), 500)
    finally:
        db.session.close()
        
def quick_jsonify(cursor):
    final_array=[]
    for row in cursor.fetchall():
        dictrow = dict(row)
        myrow = {}
        for i in dictrow:
            myrow[i]=json_serial(dictrow[i])
        final_array.append(myrow)
    return final_array
    

def treasury_vs(params):

    # if params.get('day', type= int) is None:
    #     return {"success": False, "message": "Se necesita especificar el dia"}, 500

    # if params.get('month', type= int) is None:
    #     return {"success": False, "message": "Se necesita especificar el mes"}, 500
    
    # if params.get('year', type= int) is None:
    #     return {"success": False, "message": "Se necesita especificar el año"}, 500

    # if params.get('day', type= int) is None or params.get('month', type= int) is None or params.get('year', type= int) is None:
    #     return {"success": False, "message": "Se necesita especificar el año, mes y dia inicial"}, 500
    
    # if params.get('period', type= int) is None:
    #     return {"success": False, "message": "Se necesita especificar el periodo de tiempo en meses (entre 3 y 12)"}, 500
    try:

        # query="""
        # SELECT p.status, SUM(p.total_amount) as value
        # FROM `ttn-treasury-core-dev`.pinvoices p
        # GROUP BY p.status
        # """
        
        newparams = { }

        if params.get('day', type= int) is not None and params.get('month', type= int) is not None and params.get('year', type= int) is not None:
            nowdate = "'"+params.get('year', type= str) +"-"+ params.get('month', type= str) +"-"+ params.get('day', type= str)+"'"
        else:
            nowdate = "NOW()"


        query_limiter= ""
        if params.get("period", type= int) is not None:
            newparams["period"] = params.get('period', type= int)
            query_limiter = " limit :period "

        query_provider_section= ""
        if params.get('provider', type= int) is not None:
            newparams['provider'] = params.get('provider', type= int)
            query_provider_section = " AND provider = :provider"

        query_lan = "SET lc_time_names = 'es_ES';"
        db.session.execute(query_lan)

        query = """
        SELECT 
        t1.month,
        t1.year,
        t1.md,
        coalesce(SUM(t2.amount), 0) AS total_pagadas,
        coalesce(SUM(t3.amount), 0) AS total_por_pagar
        from (
        select 
        DATE_FORMAT(a.Date,"%b") as month,
        YEAR(a.Date) as year,
        DATE_FORMAT(a.Date, "%Y-%m") as md,
        '0' as  amount
        from (
            select curdate() - INTERVAL (a.a + (10 * b.a) + (100 * c.a)) DAY as Date
            from (select 0 as a union all select 1 union all select 2 union all select 3 union all select 4 union all select 5 union all select 6 union all select 7 union all select 8 union all select 9) as a
            cross join (select 0 as a union all select 1 union all select 2 union all select 3 union all select 4 union all select 5 union all select 6 union all select 7 union all select 8 union all select 9) as b
            cross join (select 0 as a union all select 1 union all select 2 union all select 3 union all select 4 union all select 5 union all select 6 union all select 7 union all select 8 union all select 9) as c
        ) a
        where a.Date <= """+nowdate+""" and a.Date >= Date_add("""+nowdate+""",interval - 12 month)
        group by md
        ) t1

        left join (
        SELECT DATE_FORMAT(exp_date, "%b") AS month, SUM(total_amount) as amount ,DATE_FORMAT(exp_date, "%Y-%m") as md
        FROM `ttn-treasury-core-dev`.pinvoices
        where exp_date <= """+nowdate+""" and exp_date >= Date_add("""+nowdate+""",interval - 12 month)
        AND status = 5
        """+query_provider_section+"""
        GROUP BY md
        ) t2 on t2.md = t1.md 

        left join (
        SELECT DATE_FORMAT(exp_date, "%b") AS month, SUM(total_amount) as amount ,DATE_FORMAT(exp_date, "%Y-%m") as md
        FROM `ttn-treasury-core-dev`.pinvoices
        where exp_date <= """+nowdate+""" and exp_date >= Date_add("""+nowdate+""",interval - 12 month)
        AND status IN (1, 2, 3, 4)
        """+query_provider_section+"""
        GROUP BY md
        ) t3 on t3.md = t1.md 

        group by t1.md
        order by t1.md desc
        """ + query_limiter

        results = db.session.execute(query, newparams)
        re_results=[]
        total_por_pagar = 0
        total_pagado = 0

        apexgraph_month = []
        apexgraph_pagado = []
        apexgraph_por_pagar = []
        
        for row in results.fetchall():
            dictrow = dict(row)
            myrow = {}
            total_pagado += json_serial(dictrow["total_pagadas"])
            total_por_pagar += json_serial(dictrow["total_por_pagar"])
            
            apexgraph_month.append(dictrow["month"])
            apexgraph_pagado.append(json_serial(dictrow["total_pagadas"]))
            apexgraph_por_pagar.append(json_serial(dictrow["total_por_pagar"]))

            for i in dictrow:
                myrow[i]=json_serial(dictrow[i])
            re_results.append(myrow)

        apexgraph_series = [
            {
                "name":"Cuentas pagadas",
                "data": apexgraph_pagado
            },
            {
                "name":"Cuentas por pagar",
                "data": apexgraph_por_pagar
            }
        ]

        return {
            "success": True, 
            "results": re_results, 
            "total_pagado":total_pagado, 
            "total_por_pagar":total_por_pagar,

            "categories": apexgraph_month,
            "series": apexgraph_series
            }, 200
    except Exception as e:
        return responseError(e, 500)
    finally:
        db.session.close()


def accounts_per_bank(params):

    if params.get('day', type= int) is None:
        return {"success": False, "message": "Se necesita especificar el dia"}, 500

    if params.get('month', type= int) is None:
        return {"success": False, "message": "Se necesita especificar el mes"}, 500
    
    if params.get('year', type= int) is None:
        return {"success": False, "message": "Se necesita especificar el año"}, 500
    
    if params.get('entity_account', type= int) is None:
        return {"success": False, "message": "Se necesita especificar la cuenta en sesión"}, 500

    try:
        newparams = {
            "day": params.get('day', type= int),
            "month": params.get('month', type= int),
            "year": params.get('year', type= int),
            "account": params.get('entity_account', type= int),
        }
        query="""
        SELECT
            n.id,
            n.name_bank,
            SUM(jd.debitAmount) debit,
            SUM(jd.creditAmount) credit
        FROM """+ADMINISTRATION_DBNAME+""".niifAccount n
            LEFT JOIN `mod-accounting-core-dev`.journal_detail jd ON (jd.niifAccount = n.id)
            LEFT JOIN `mod-accounting-core-dev`.journal_voucher jv ON (jv.id = jd.jrnl_id )
        WHERE n.name_bank != ''
        and jv.transaction_date <= DATE(CONCAT(:year,:month,:day))
        and jv.entity_account =:account
        GROUP BY n.name_bank
        """
        data = []
        labels = []
        table= []

        result = db.session.execute(query, newparams)
        for row in result.fetchall():
            data.append((float(row.debit)-float(row.credit)))
            labels.append(row.name_bank)
            table.append({"label":row.name_bank, "amount": float(row.debit)-float(row.credit) })


        return {"success":True, "labels":labels, "data": data, "table": table}, 200
    except Exception as e:
        return responseError(e, 500)
    finally:
        db.session.close()
        
def top_providers(data):
    try:
        year = data.get('year')
        if not year:
            return responseError('year', 400, "El campo año es obligatorio")
        month = data.get('month')
        if not month:
            return responseError('month', 400, "El campo mes es obligatorio")
        top = data.get('top')
        if not top:
            top = 5
        dashboard = data.get('dashboard')
        account = data.get('account')
        if not account:
            return responseError('account', 400, "El campo cuenta es obligatorio")
        year_end = data.get('year_end')
        month_end = data.get('month_end')
        sort = data.get('sort')
        if sort is None:
            sort = "desc"
        if (dashboard is not None ):
            filter = """ AND qo.created_at BETWEEN DATE_ADD(DATE(CONCAT(:year,'-',:month,'-','1')),INTERVAL -6 month) AND DATE_ADD(DATE_ADD(DATE(CONCAT(:year,'-',:month,'-','1')), INTERVAL 1 MONTH), INTERVAL -1 DAY) """
            filter2 = """ AND wh.created_at BETWEEN DATE_ADD(DATE(CONCAT(:year,'-',:month,'-','1')),INTERVAL -6 month) AND DATE_ADD(DATE_ADD(DATE(CONCAT(:year,'-',:month,'-','1')), INTERVAL 1 MONTH), INTERVAL -1 DAY) """
            filters3 = """ AND we.created_at BETWEEN DATE_ADD(DATE(CONCAT(:year,'-',:month,'-','1')),INTERVAL -6 month) AND DATE_ADD(DATE_ADD(DATE(CONCAT(:year,'-',:month,'-','1')), INTERVAL 1 MONTH), INTERVAL -1 DAY) """
            filters4 = """ AND we2.created_at BETWEEN DATE_ADD(DATE(CONCAT(:year,'-',:month,'-','1')),INTERVAL -6 month) AND DATE_ADD(DATE_ADD(DATE(CONCAT(:year,'-',:month,'-','1')), INTERVAL 1 MONTH), INTERVAL -1 DAY) """
        else:
            if (year_end is None or month_end is None):
                return {'success': False, 'message': "Los atributos mes y año final son necesarios" },400
            if(year_end < year):
                return {'success': False, 'message': "El año inicial debe ser menor que el final" },400
            if(year_end == year and month_end < month):
                return {'success': False, 'message': "El mes inicial debe ser menor que el final" },400
            filter = """ AND qo.created_at BETWEEN (DATE(CONCAT(:year,'-',:month,'-','1'))) AND DATE_ADD(DATE_ADD(DATE(CONCAT(:year_end,'-',:month_end,'-','1')), INTERVAL 1 MONTH), INTERVAL -1 DAY) """
            filter2 = """ AND wh.created_at BETWEEN (DATE(CONCAT(:year,'-',:month,'-','1'))) AND DATE_ADD(DATE_ADD(DATE(CONCAT(:year_end,'-',:month_end,'-','1')), INTERVAL 1 MONTH), INTERVAL -1 DAY) """
            filters3 = """ AND we.created_at BETWEEN (DATE(CONCAT(:year,'-',:month,'-','1'))) AND DATE_ADD(DATE_ADD(DATE(CONCAT(:year_end,'-',:month_end,'-','1')), INTERVAL 1 MONTH), INTERVAL -1 DAY) """
            filters4 = """ AND we2.created_at BETWEEN (DATE(CONCAT(:year,'-',:month,'-','1'))) AND DATE_ADD(DATE_ADD(DATE(CONCAT(:year_end,'-',:month_end,'-','1')), INTERVAL 1 MONTH), INTERVAL -1 DAY) """
        query = """SELECT rq.id, sum(rq.puntos) puntos, rq.name, SUM(rq.problemas) problemas, SUM(rq.compras) compras, sum(rq.cash) cash
        FROM ( 
        SELECT p.id , sum(30) puntos, p.name ,0 problemas, 0 compras, sum(pin.total_amount) cash
        FROM """+INVENTORY_DBNAME+""".providers p
        INNER JOIN """+INVENTORY_DBNAME+""".quotation qo on qo.provider = p.id
        INNER JOIN """+TREASURY_DBNAME+""".pinvoices pin on pin.provider = p.id
        where 1 = 1 """+filter+"""
        AND p.id_account = :eaccount
        group by 1
        union
        SELECT p.id ,  sum(40) puntos, p.name ,0 , count(*) compras, 0
        FROM """+INVENTORY_DBNAME+""".providers p
        INNER JOIN """+INVENTORY_DBNAME+""".warehouse_entry wh on wh.we_provider_id = p.id
        where 1 = 1 """+ filter2+  """
        AND p.id_account = :eaccount
        group by 1
        union 
        SELECT  po.provider  , sum(30)  puntos, p.name, 0, 0 compras, 0
        FROM """+INVENTORY_DBNAME+""".purchase_order po
        INNER JOIN """+INVENTORY_DBNAME+""".warehouse_entry we on we.we_purchase_order_id = po.id
        INNER JOIN """+INVENTORY_DBNAME+""".providers p on po.provider = p.id
        where we.entry_date <= po.delivery_date """+filters3+""" 
        AND po.account = :eaccount group by 1 union 
        SELECT we2.we_provider_id , sum(-30), p.name, count(*), 0 compras, 0
        FROM """+INVENTORY_DBNAME+""".warehouse_entry we2
        INNER JOIN """+INVENTORY_DBNAME+""".providers p on we2.we_provider_id = p.id
        where we_problem is not null 
        """+filters4+"""
        AND we2.eaccount = :eaccount group by 1)
        as rq
        group by 1 
        order by 2 """+sort+"""
        LIMIT :top
        """
        queryresult = db.session.execute(query, {"eaccount": account , "month": month, "year": year, "top":int(top), "year_end" : year_end, "month_end" : month_end })
        finalResult = []
        tot_points = 0
        data = []
        labels = []
        for row in queryresult.fetchall():
            tot_points += float(row.puntos)
            labels.append(row.name)
            data.append(row)
        for row in data:
            response = {"name" : row.name, "puntos" : int(round(float(row.puntos) / tot_points * 100,0)) , "problems" : int(row.problemas) , "comps" : int(row.compras), "cash" : int(row.cash)}
            finalResult.append(response)
        return { "result": finalResult, 'success': True , "labels" : labels }

    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        db.session.rollback()
        return {'success': False, 'message': str(e) },400
    finally:
        db.session.close()

def actives(data):
    try:
        year = data.get('year')
        if not year:
            return  responseError('year', 400, "El campo año inicial es obligatorio")
        month = data.get('month')
        if not month:
            return  responseError('month', 400, "El campo mes inicial es obligatorio")
        account = data.get('account')
        if not account:
            return responseError('account', 400, "El campo cuenta es obligatorio")
        year_end = data.get('year_end')
        if not year_end:
            return responseError('year_end', 400, "El campo año final es obligatorio")
        month_end = data.get('month_end')
        if not month_end:
            return responseError('month_end', 400, "El campo mes final es obligatorio")
        query = """ SELECT sum(1) activos from(
        SELECT p.id , p.name
        FROM """+INVENTORY_DBNAME+""".providers p
        INNER JOIN """+INVENTORY_DBNAME+""".quotation qo on qo.provider = p.id
        WHERE 
        qo.created_at BETWEEN (DATE(CONCAT(:year,'-',:month,'-','1'))) AND DATE_ADD(DATE_ADD(DATE(CONCAT(:year_end,'-',:month_end,'-','1' )), INTERVAL 1 MONTH), INTERVAL -1 DAY)
        AND p.id_account = :eaccount
        group by 1
        union
        SELECT p.id , p.name
        FROM """+INVENTORY_DBNAME+""".providers p
        INNER JOIN """+INVENTORY_DBNAME+""".purchase_order po ON po.provider  = p.id
        WHERE 
        po.created_at BETWEEN (DATE(CONCAT(:year,'-',:month,'-','1'))) AND DATE_ADD(DATE_ADD(DATE(CONCAT(:year_end,'-',:month_end,'-','1')), INTERVAL 1 MONTH), INTERVAL -1 DAY)  
        AND p.id_account = :eaccount
        group by 1
        union
        SELECT p.id , p.name
        FROM """+INVENTORY_DBNAME+""".providers p
        INNER JOIN """+INVENTORY_DBNAME+""".warehouse_entry wh on wh.we_provider_id = p.id
        WHERE 
        wh.created_at BETWEEN (DATE(CONCAT(:year,'-',:month,'-','1'))) AND DATE_ADD(DATE_ADD(DATE(CONCAT(:year_end,'-',:month_end,'-','1')), INTERVAL 1 MONTH), INTERVAL -1 DAY)  
        AND p.id_account = :eaccount
        group by 1
        union
        SELECT  p.id , p.name
        FROM """+INVENTORY_DBNAME+""".providers p
        INNER JOIN """+INVENTORY_DBNAME+""".warehouse_movements wm on wm.wm_provider_id = p.id
        WHERE 
        wm.created_at BETWEEN (DATE(CONCAT(:year,'-',:month,'-','1'))) AND DATE_ADD(DATE_ADD(DATE(CONCAT(:year_end,'-',:month_end,'-','1')), INTERVAL 1 MONTH), INTERVAL -1 DAY)  
        AND p.id_account = :eaccount
        group by 1 ) a
        
        """
        queryresult = db.session.execute(query, {"eaccount": account , "month": month, "year": year, "year_end" : year_end, "month_end" : int(month_end)})
        finalResult = []
        for row in queryresult.fetchall():
            dictrow = dict(row)
            myrow = {}
            for i in dictrow:
                myrow[i]=json_serial(dictrow[i])
            finalResult.append(myrow)
        return { "result": finalResult, 'success': True }
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        db.session.rollback()
        return {'success': False, 'message': str(e) },400
    finally:
        db.session.close()

def accounts_payable_provider(data):
    try:
        year = data.get('year')
        if not year:
            return  responseError('year', 400, "El campo año inicial es obligatorio")
        month = data.get('month')
        if not month:
            return  responseError('month', 400, "El campo mes inicial es obligatorio")
        day = data.get('day')
        if not day:
            return  responseError('day', 400, "El campo día es obligatorio")
        account = data.get('account')
        if not account:
            return responseError('account', 400, "El campo cuenta es obligatorio")
        query = """SELECT SUM(CAST(p.total_amount AS DECIMAL)) totalAmount , pr.name ,p.id 
        FROM """+TREASURY_DBNAME+""".pinvoices p
        INNER JOIN """+TREASURY_DBNAME+""".pinvoices_status st ON (st.id = p.status)
        INNER JOIN """+INVENTORY_DBNAME+""".providers pr ON (pr.id = p.provider)
        WHERE st.description NOT IN ('Pagada','Anulada')
        AND p.exp_date <= DATE(CONCAT(:year,:month,:day))
        AND p.entity_account = :eaccount
        group by pr.id 
        order by totalAmount desc
        """
        queryresult = db.session.execute(query, {"eaccount": account , "month": month, "year": year, "day" : day})
        data = []
        labels = []
        total=0
        finalresult=[]
        for row in queryresult.fetchall():
            total=total + float(row.totalAmount)
            finalresult.append(row)
        percentage80 = 0
        totalOtros=0
        for row in finalresult:
            if (percentage80 <= 80):
                percentage80 = percentage80 + ((100*float(row.totalAmount))/total)
                data.append(float(row.totalAmount))
                labels.append(row.name)
            else:
                totalOtros= totalOtros + float(row.totalAmount)
        
        data.append(totalOtros)
        labels.append("Otros")
        return { "data": data, "labels" : labels,'success': True }
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        db.session.rollback()
        return {'success': False, 'message': str(e) },400
    finally:
        db.session.close()
        
def balance_per_account(data):
    try:
        year = data.get('year')
        if not year:
            return  responseError('year', 400, "El campo año inicial es obligatorio")
        month = data.get('month')
        if not month:
            return  responseError('month', 400, "El campo mes inicial es obligatorio")
        day = data.get('day')
        if not day:
            return  responseError('day', 400, "El campo día es obligatorio")
        account = data.get('account')
        if not account:
            return responseError('account', 400, "El campo cuenta es obligatorio")
        query ="""SELECT
        n.id,
        n.description,
        n.num_count,
        n.name_bank,
        SUM(CASE WHEN n.nature = 'D' THEN jd.debitAmount-jd.creditAmount ELSE jd.creditAmount-jd.debitAmount END) saldo
        FROM """+ADMINISTRATION_DBNAME+""".niifAccount n
        INNER JOIN """+ACCOUNTING_DBNAME+""".journal_detail jd ON (jd.niifAccount = n.id)
        INNER JOIN """+ACCOUNTING_DBNAME+""".journal_voucher jv ON (jv.id = jd.jrnl_id )
        WHERE check_info_bank = 1 AND n.entity_account = :eaccount
        and
        jv.transaction_date <= DATE(CONCAT(:year,:month,:day))
        GROUP BY n.id
        order by n.name_bank desc
        """
        queryresult = db.session.execute(query, {"eaccount": account , "month": month, "year": year, "day" : day})
        finalResult = []
        labels = []
        response={}
        series=[]
        tot_balance = 0
        for row in queryresult.fetchall():
            tot_balance += int(row.saldo)
            response = {"name" : row.description , "percentage" : 0 , "bank" : row.name_bank , "count_num" : row.num_count, "balance" : int(row.saldo) }
            finalResult.append(response)
            labels.append(row.description + "-"  + row.num_count )
        for i in range(len(finalResult)):
            if tot_balance == 0 : 
                tot_balance = 1
            finalResult[i]['percentage'] = round(int(finalResult[i].get('balance')) / tot_balance * 100)
            series.append(["<b>" + finalResult[i]['name'] + "</b> " +  " <br/>" + finalResult[i]['count_num'] + " <br/> " + str((finalResult[i]['percentage'])) + "%" , finalResult[i]['balance']])
        return { "result": finalResult, "labels": labels, "series": series,"success": True , 'total_balance' : tot_balance}
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        db.session.rollback()
        return {'success': False, 'message': str(e) },400
    finally:
        db.session.close()

def total_income(data):
    try:
        year = data.get('year')
        if not year:
            return  responseError('year', 400, "El campo año inicial es obligatorio")
        month = data.get('month')
        if not month:
            return  responseError('month', 400, "El campo mes inicial es obligatorio")
        account = data.get('account')
        if not account:
            return responseError('account', 400, "El campo cuenta es obligatorio")
        query = """SELECT
        COALESCE(SUM(CASE WHEN n.nature = 'D' THEN jd.debitAmount-jd.creditAmount ELSE jd.creditAmount-jd.debitAmount END),0) saldo
        FROM """+ADMINISTRATION_DBNAME+""".niifAccount n
        INNER JOIN """+ACCOUNTING_DBNAME+""".journal_detail jd ON (jd.niifAccount = n.id)
        INNER JOIN """+ACCOUNTING_DBNAME+""".journal_voucher jv ON (jv.id= jd.jrnl_id)
        WHERE n.entity_account = :eaccount
        AND jv.transaction_date <= DATE(CONCAT(:year,:month, day(DATE_ADD(DATE_ADD(DATE(CONCAT(:year,'-',:month,'-','1')), INTERVAL 1 MONTH), INTERVAL -1 DAY))  ))
        AND n.complete_account LIKE '41%';
        """
        queryresult = db.session.execute(query, {"eaccount": account , "month": month, "year": year})
        if int(month) == 1:
            month = 12
            year = int(year) -1
        else:
            month = int(month) - 1
        queryresult2 = db.session.execute(query, {"eaccount": account , "month": month, "year": year})
        before = queryresult2.fetchall()[0].saldo
        indicator = "Estable"
        finalResult = []
        for row in queryresult.fetchall():
            dictrow = dict(row)
            myrow = {}
            for i in dictrow:
                myrow[i]=json_serial(dictrow[i])
            finalResult.append(myrow)
        if (int(finalResult[0].get('saldo')) - before < 0):
            indicator = "Bajó"
        elif (int(finalResult[0].get('saldo'))- before > 0):
            indicator = "Subió"
        return { "result": finalResult, 'success': True, "indicator": indicator }
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        db.session.rollback()
        return {'success': False, 'message': str(e) },400
    finally:
        db.session.close()

def balance_revenues_expenses(data):
    try:
        year = data.get('year')
        if not year:
            return  responseError('year', 400, "El campo año inicial es obligatorio")
        month = data.get('month')
        if not month:
            return  responseError('month', 400, "El campo mes inicial es obligatorio")
        account = data.get('account')
        if not account:
            return responseError('account', 400, "El campo cuenta es obligatorio")
        query = """SELECT sum(costgast.COSTOS_Y_GASTOS) COSTOS_Y_GASTOS, costgast.days from (
            SELECT
            SUM(CASE WHEN n.nature = 'D' THEN jd.debitAmount-jd.creditAmount ELSE jd.creditAmount-jd.debitAmount END) COSTOS_Y_GASTOS
            , day(jv.transaction_date) as days
            FROM """+ADMINISTRATION_DBNAME+""".niifAccount n
            INNER JOIN """+ACCOUNTING_DBNAME+""".journal_detail jd ON (jd.niifAccount = n.id)
            INNER JOIN """+ACCOUNTING_DBNAME+""".journal_voucher jv ON (jv.id= jd.jrnl_id)
            WHERE n.entity_account = :eaccount
            AND n.complete_account LIKE'5%'
            AND jv.transaction_date 
            BETWEEN (DATE(CONCAT(:year,'-',:month,'-','1'))) AND DATE_ADD(DATE_ADD(DATE(CONCAT(:year,'-',:month,'-','1')), INTERVAL 1 MONTH), INTERVAL -1 DAY) 
            group by days
            union
            SELECT
            SUM(CASE WHEN n.nature = 'D' THEN jd.debitAmount-jd.creditAmount ELSE jd.creditAmount-jd.debitAmount END) COSTOS_Y_GASTOS
            , day(jv.transaction_date) as days
            FROM """+ADMINISTRATION_DBNAME+""".niifAccount n
            INNER JOIN """+ACCOUNTING_DBNAME+""".journal_detail jd ON (jd.niifAccount = n.id)
            INNER JOIN """+ACCOUNTING_DBNAME+""".journal_voucher jv ON (jv.id= jd.jrnl_id)
            WHERE n.entity_account = :eaccount
            AND n.complete_account LIKE '6%'
            AND jv.transaction_date 
            BETWEEN (DATE(CONCAT(:year,'-',:month,'-','1'))) AND DATE_ADD(DATE_ADD(DATE(CONCAT(:year,'-',:month,'-','1')), INTERVAL 1 MONTH), INTERVAL -1 DAY) 
            group by days
            order by days asc 
            ) costgast
            group by costgast.days
            """
        queryresult = db.session.execute(query, {"eaccount": account , "month": month, "year": year})
        finalResult = []
        series = []
        data = []
        num_days = calendar.monthrange(int(year), int(month))[1]
        days = [(day) for day in range(1, num_days+1)]
        value= 0
        for row in queryresult.fetchall():
            dictrow = dict(row)
            myrow = {}
            for i in dictrow:
                myrow[i]=json_serial(dictrow[i])
            finalResult.append(myrow)
        for day in days:
            for i in range(len(finalResult)):
                if(finalResult[i].get('days') == day):
                    value =int(finalResult[i].get('COSTOS_Y_GASTOS'))
            data.append(value)
            value = 0
        temp = {"name" : "Costos y gastos",
                "data" : data}
        series.append(temp) 
        query2 = """SELECT
        SUM(CASE WHEN n.nature = 'D' THEN jd.debitAmount-jd.creditAmount ELSE jd.creditAmount-jd.debitAmount END) INGRESO
        , day(jv.transaction_date) as days
        FROM """+ADMINISTRATION_DBNAME+""".niifAccount n
        INNER JOIN """+ACCOUNTING_DBNAME+""".journal_detail jd ON (jd.niifAccount = n.id)
        INNER JOIN """+ACCOUNTING_DBNAME+""".journal_voucher jv ON (jv.id= jd.jrnl_id)
        WHERE n.entity_account = :eaccount
        AND jv.transaction_date 
        BETWEEN (DATE(CONCAT(:year,'-',:month,'-','1'))) AND DATE_ADD(DATE_ADD(DATE(CONCAT(:year,'-',:month,'-','1')), INTERVAL 1 MONTH), INTERVAL -1 DAY) 
        AND n.complete_account LIKE '41%'
        group by days
        """
        data = []
        queryresult2 = db.session.execute(query2, {"eaccount": account , "month": month, "year": year})
        finalResult2 = []
        for row in queryresult2.fetchall():
            dictrow = dict(row)
            myrow = {}
            for i in dictrow:
                myrow[i]=json_serial(dictrow[i])
            finalResult2.append(myrow)
        for day in days:
            for i in range(len(finalResult2)):
                if(finalResult2[i].get('days') == day):
                    value =int(finalResult2[i].get('INGRESO'))
            data.append(value)
            value = 0
        temp = {"name" : "Ingresos",
                "data" : data}
        series.append(temp) 
        return { "series": series, "labels": days, 'success': True }
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        db.session.rollback()
        return {'success': False, 'message': str(e) },400
    finally:
        db.session.close()

def accounts_payable_balance(data):
    try:
        year = data.get('year')
        if not year:
            return  responseError('year', 400, "El campo año inicial es obligatorio")
        month = data.get('month')
        if not month:
            return  responseError('month', 400, "El campo mes inicial es obligatorio")
        account = data.get('account')
        if not account:
            return responseError('account', 400, "El campo cuenta es obligatorio")
        query = """SELECT COALESCE(SUM(CAST(p.total_amount AS DECIMAL)),0) totalAmount 
        FROM """+TREASURY_DBNAME+""".pinvoices p
        INNER JOIN """+TREASURY_DBNAME+""".pinvoices_status st ON (st.id = p.status)
        INNER JOIN """+INVENTORY_DBNAME+""".providers pr ON (pr.id = p.provider)
        WHERE st.description NOT IN ('Pagada','Anulada')
        AND p.exp_date <= DATE(CONCAT(:year,:month,day(DATE_ADD(DATE_ADD(DATE(CONCAT(:year,'-',:month,'-','1')), INTERVAL 1 MONTH), INTERVAL -1 DAY)) ))
        AND p.entity_account = :eaccount
        order by totalAmount desc
        """
        queryresult = db.session.execute(query, {"eaccount": account , "month": month, "year": year})
        if int(month) == 1:
            month = 12
            year = int(year) -1
        else:
            month = int(month) - 1
        queryresult2 = db.session.execute(query, {"eaccount": account , "month": month, "year": year})
        before = queryresult2.fetchall()[0].totalAmount
        indicator = "Estable"
        finalResult = []
        for row in queryresult.fetchall():
            dictrow = dict(row)
            myrow = {}
            for i in dictrow:
                myrow[i]=json_serial(dictrow[i])
            finalResult.append(myrow)
        if (int(finalResult[0].get('totalAmount')) - before < 0):
            indicator = "Bajó"
        elif (int(finalResult[0].get('totalAmount'))- before > 0):
            indicator = "Subió"
        return { "result": finalResult, 'success': True, "indicator": indicator }
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        db.session.rollback()
        return {'success': False, 'message': str(e) },400
    finally:
        db.session.close()
        
def profitBalance(params):


    if params.get('year', type= int) is None:
        return {"success": False, "message": "Se necesita especificar el año"}, 500
    if params.get('account') is None:
        return responseError("Se necesita especificar la cuenta en sesión", 500)

    try:

        newparams = { 
            "year": params.get('year', type= int),
            "account" : params.get('account', type= int)
            ,"niifAccount" : params.get('niifAccount', type= int) 
        }
        if params.get('niifAccount', type= int) is not None:
            queryniif = "and jd.niifAccount = :niifAccount"
        else:
            queryniif = ""
        query = """
        SELECT 
        t1.month,
        t1.year,
        t1.md,
        coalesce(SUM(t2.amount), 0) AS total_deposits,
        coalesce(SUM(t3.amount), 0) AS total_gastos
        from (
        select
        mn.name as month,
        YEAR(a.Date) as year,
        DATE_FORMAT(a.Date, "%Y-%m") as md,
        '0' as  amount
        from (
            select curdate() - INTERVAL (a.a + (10 * b.a) + (100 * c.a)) DAY as Date
            from (select 0 as a union all select 1 union all select 2 union all select 3 union all select 4 union all select 5 union all select 6 union all select 7 union all select 8 union all select 9) as a
            cross join (select 0 as a union all select 1 union all select 2 union all select 3 union all select 4 union all select 5 union all select 6 union all select 7 union all select 8 union all select 9) as b
            cross join (select 0 as a union all select 1 union all select 2 union all select 3 union all select 4 union all select 5 union all select 6 union all select 7 union all select 8 union all select 9) as c
        ) a
        inner join """+PAYROLL_DBNAME+""".month_ES mn on mn.id = MONTH(a.Date)
        where a.Date <=  DATE(CONCAT(:year, '12', '31')) and a.Date >= Date_add(date_add( DATE(CONCAT(:year, '12', '31')),interval - 12 month), interval 1 month)
        group by md
        ) t1

        left join (
        SELECT
        mn.name as month
        , SUM(jv.amount) as amount 
        ,DATE_FORMAT(jv.transaction_date, "%Y-%m") as md
        FROM """+ACCOUNTING_DBNAME+""".journal_voucher jv
        JOIN """+ACCOUNTING_DBNAME+""".journal_detail jd ON jd.jrnl_id = jv.id
        inner join """+PAYROLL_DBNAME+""".month_ES mn on mn.id = MONTH(jv.transaction_date)
        where jv.transaction_date <=  DATE(CONCAT(:year, '12', '31')) 
        and jv.transaction_date >= Date_add( DATE(CONCAT(:year, '12', '31')),interval - 12 month)
        """+queryniif+"""
        and jv.entity_account = :account
        GROUP BY md
        ) t2 on t2.md = t1.md 

        left join (
        SELECT
        mn.name as month
        ,SUM(amount) as amount 
        ,DATE_FORMAT(transaction_date, "%Y-%m") as md
        FROM """+ACCOUNTING_DBNAME+""".journal_voucher
        inner join """+PAYROLL_DBNAME+""".month_ES mn on mn.id = MONTH(transaction_date)
        where transaction_date <=  DATE(CONCAT(:year, '12', '31')) 
        and transaction_date >= Date_add( DATE(CONCAT(:year, '12', '31')),interval - 12 month)
        and entity_account = :account
        GROUP BY md
        ) t3 on t3.md = t1.md 

        group by t1.md
        order by t1.md desc
        """
        re_results = []
        labels = []
        total_deposits = []
        total_gastos = []
        result = db.session.execute(query, newparams)
        for row in result.fetchall():
            labels.append(row.month)
            total_deposits.append(float(row.total_deposits))
            total_gastos.append(float(row.total_gastos))
        temp = {"name" : "Ingresos", "data": total_deposits}
        temp1 = {"name" : "Gastos", "data" : total_gastos}
        re_results.append(temp)
        re_results.append(temp1)
        return {"success": True, "series": re_results, "labels" : labels}, 200
    except Exception as e:
        return responseError(str(e), 500)
    finally:
        db.session.close()


def overall_consumption(data):
    try:
        year = data.get('year')
        if not year:
            return  responseError('year', 400, "El campo año inicial es obligatorio")
        month = data.get('month')
        if not month:
            return  responseError('month', 400, "El campo mes inicial es obligatorio")
        account = data.get('account')
        if not account:
            return responseError('account', 400, "El campo cuenta es obligatorio")
        query = """SELECT COALESCE(SUM(wmd.qty_article*art.cost_price), 0) overall_consumption 
        FROM """+INVENTORY_DBNAME+""".warehouse_movements wm 
        INNER JOIN """+INVENTORY_DBNAME+""".warehouse_movements_detail wmd ON wmd.warehouse_movement_id = wm.id
        INNER JOIN """+INVENTORY_DBNAME+""".article art on art.id = wmd.article_id
        WHERE wm.wm_movement_type = "consumOutput"
        and date(wm.created_at ) between DATE(CONCAT(:year,'-',:month,'-','1')) and DATE(CONCAT(:year,:month, day(DATE_ADD(DATE_ADD(DATE(CONCAT(:year,'-',:month,'-','1')), INTERVAL 1 MONTH), INTERVAL -1 DAY))  ))
        """
        queryresult = db.session.execute(query, {"eaccount": account , "month": month, "year": year})
        if int(month) == 1:
            month = 12
            year = int(year) -1
        else:
            month = int(month) - 1
        queryresult2 = db.session.execute(query, {"eaccount": account , "month": month, "year": year})
        before = queryresult2.fetchall()[0].overall_consumption
        indicator = "Estable"
        finalResult = []
        for row in queryresult.fetchall():
            dictrow = dict(row)
            myrow = {}
            for i in dictrow:
                myrow[i]=json_serial(dictrow[i])
            finalResult.append(myrow)
        if (finalResult[0].get('overall_consumption') - before < 0):
            indicator = "Bajó"
        elif (finalResult[0].get('overall_consumption') - before > 0):
            indicator = "Subió"
        return { "result": finalResult, 'success': True , "indicator": indicator}
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        db.session.rollback()
        return {'success': False, 'message': str(e) },400
    finally:
        db.session.close()

def top_clients(data):
    try:
        year = data.get('year')
        if not year:
            return  responseError('year', 400, "El campo año inicial es obligatorio")
        month = data.get('month')
        if not month:
            return  responseError('month', 400, "El campo mes inicial es obligatorio")
        account = data.get('account')
        if not account:
            return responseError('account', 400, "El campo cuenta es obligatorio")
        query = """SELECT SUM(asd.puntos) points ,asd.copyName as name , sum(asd.tot) as total_facturado
        FROM (SELECT
        SUM(IF( inv.due_date >= ca.receipt_date, 1, 0)) / COUNT(*) *100 *0.4 as puntos
        ,cp.id
        ,cp.copyName
        , 0 tot
        FROM """+INVOICING_DBNAME+""".invoices inv
        INNER JOIN """+ADMINISTRATION_DBNAME+""".coporateClients cp ON cp.id =inv.corporate_client
        INNER JOIN """+TREASURY_DBNAME+""".collack_invoices ci ON ci.corp_invoice = inv.id
        INNER JOIN """+TREASURY_DBNAME+""".collections_ack ca ON ca.id = ci.collection_ack
        WHERE ca.receipt_date BETWEEN DATE_ADD(DATE(CONCAT(:year,'-',:month,'-','1')),INTERVAL -6 month) AND DATE_ADD(DATE_ADD(DATE(CONCAT(:year,'-',:month,'-','1')), INTERVAL 1 MONTH), INTERVAL -1 DAY)
        AND cp.id_account = :eaccount
        GROUP BY cp.id
        UNION
        SELECT
        SUM(IF( inv.pending_amount = 0, 1, 0)) / COUNT(*) *100 *0.5 as puntos
        ,cp.id
        ,cp.copyName
        ,0 tot
        FROM """+INVOICING_DBNAME+""".invoices inv
        INNER JOIN """+ADMINISTRATION_DBNAME+""".coporateClients cp ON cp.id =inv.corporate_client
        INNER JOIN """+TREASURY_DBNAME+""".collack_invoices ci ON ci.corp_invoice = inv.id
        INNER JOIN """+TREASURY_DBNAME+""".collections_ack ca ON ca.id = ci.collection_ack
        WHERE ca.receipt_date BETWEEN DATE_ADD(DATE(CONCAT(:year,'-',:month,'-','1')),INTERVAL -6 month) AND DATE_ADD(DATE_ADD(DATE(CONCAT(:year,'-',:month,'-','1')), INTERVAL 1 MONTH), INTERVAL -1 DAY)
        AND cp.id_account = :eaccount
        GROUP BY cp.id
        UNION
        SELECT
        SUM(inv.total_amount) / tot  * 0.10  * 100 as puntos
        ,cp.id
        ,cp.copyName
        ,tot
        FROM """+INVOICING_DBNAME+""".invoices inv
        INNER JOIN """+ADMINISTRATION_DBNAME+""".coporateClients cp ON cp.id =inv.corporate_client,
        (SELECT SUM(inv2.total_amount ) tot FROM """+INVOICING_DBNAME+""".invoices inv2 ) total
        WHERE inv.exp_date BETWEEN DATE_ADD(DATE(CONCAT(:year,'-',:month,'-','1')),INTERVAL -6 month) AND DATE_ADD(DATE_ADD(DATE(CONCAT(:year,'-',:month,'-','1')), INTERVAL 1 MONTH), INTERVAL -1 DAY)
        AND cp.id_account = :eaccount
        GROUP BY cp.id
        ) asd
        GROUP BY  asd.id
        limit 5;
        """
        queryresult = db.session.execute(query, {"eaccount": account , "month": month, "year": year})
        finalResult = []
        data = []
        tot_points = 0
        for row in queryresult.fetchall():
            tot_points += float(row.points)
            data.append(row)
        for row in data:
            response = {"name" : row.name, "puntos" : int(round(float(row.points) / tot_points * 100,0))}
            finalResult.append(response)
            
        return { "result": finalResult, 'success': True }
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        db.session.rollback()
        return {'success': False, 'message': str(e) },400
    finally:
        db.session.close()

def inv_rot(data):
    try:
        year = data.get('year')
        if not year:
            return  responseError('year', 400, "El campo año inicial es obligatorio")
        account = data.get('account')
        if not account:
            return responseError('account', 400, "El campo cuenta es obligatorio")
        query = """SELECT
        INVENTARIO,
        COSTO_DE_VENTAS,
        ROUND((INVENTARIO * 360 / COSTO_DE_VENTAS) * 100,2) ROT_INV
        FROM (
            SELECT
            SUM(CASE WHEN n.nature = 'D' THEN jd.debitAmount-jd.creditAmount ELSE jd.creditAmount-jd.debitAmount END) INVENTARIO
            FROM """+ADMINISTRATION_DBNAME+""".niifAccount n
            INNER JOIN """+ACCOUNTING_DBNAME+""".journal_detail jd ON (jd.niifAccount = n.id)
            INNER JOIN """+ACCOUNTING_DBNAME+""".journal_voucher jv ON (jv.id= jd.jrnl_id)
            WHERE n.entity_account = :eaccount
            AND n.id IN (SELECT
            DISTINCT niifAccount
            FROM """+ACCOUNTING_DBNAME+""".account_assignment_model
            WHERE transacName = 'purEntry' and concept = 'subtotal')
            AND jv.transaction_date <= DATE_ADD(DATE_ADD(DATE(CONCAT(:year,'-',:month,'-','1')), INTERVAL 1 month), INTERVAL -1 day)
            ) AS inv,(
                SELECT
                SUM(CASE WHEN n.nature = 'D' THEN jd.debitAmount-jd.creditAmount ELSE jd.creditAmount-jd.debitAmount END) COSTO_DE_VENTAS
                FROM """+ADMINISTRATION_DBNAME+""".niifAccount n
                INNER JOIN """+ACCOUNTING_DBNAME+""".journal_detail jd ON (jd.niifAccount = n.id)
                INNER JOIN """+ACCOUNTING_DBNAME+""".journal_voucher jv ON (jv.id= jd.jrnl_id)
                WHERE n.entity_account = :eaccount
                AND n.id IN (SELECT
                DISTINCT niifAccount
                FROM """+ACCOUNTING_DBNAME+""".account_assignment_model
                WHERE transacName = 'invoice' and concept = 'costTotal')
                AND jv.transaction_date 
                BETWEEN (DATE(CONCAT(:year,'-',:month,'-','1'))) AND DATE_ADD(DATE_ADD(DATE(CONCAT(:year,'-',:month,'-','1')), INTERVAL 1 MONTH), INTERVAL -1 DAY) ) AS cost
                """
        labels = ["Enero", "Febrero" , "Marzo" ,"Abril", "Mayo", "Junio", "Julio", "Agosto" , "Septiembre", "Octubre", "Noviembre", "Diciembre"]
        data = []
        series = []
        month = 1;
        for i in labels:
            queryresult = db.session.execute(query, {"eaccount": account , "month": month, "year": year})
            month += 1;            
            for row in queryresult.fetchall():
                if row.ROT_INV is not None:
                    data.append(float(row.ROT_INV))
                else:
                    data.append(0.0)
        temp = {
            "name" : "Rotación de inventario",
            "data" : data
        }
        series.append(temp) 
        return {
            "series": series, 
                "labels" : labels,
                'success': True }
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        db.session.rollback()
        return {'success': False, 'message': str(e) },400
    finally:
        db.session.close()

def period_expenses(data):
    try:
        date_start = data.get('date_start')
        if not date_start:
            return  responseError('date_start ', 400, "El campo fecha inicial es obligatorio")
        date_end = data.get('date_end')
        if not date_end:
            return  responseError('date_end', 400, "El campo fecha final es obligatorio")
        account = data.get('account')
        if not account:
            return responseError('account', 400, "El campo caja menor es obligatorio")
        pettyCash = data.get('petty_cash')
        if pettyCash is not None:
            queryPetty = "AND pt.id = :pettyCash"
        else:
            queryPetty = ""
        query = """SELECT exp.niifAccount as Account, nf.description as Name, SUM(exp.amount) as Ammount 
        FROM """+ACCOUNTING_DBNAME+""".expense exp
        INNER JOIN """+ACCOUNTING_DBNAME+""".petty_cash pt on pt.id = exp.pettyCash
        INNER JOIN """+ADMINISTRATION_DBNAME+""".niifAccount nf on nf.id = exp.niifAccount
        WHERE exp.exp_date BETWEEN (:date_start) AND (:date_end) AND
        nf.entity_account = :account
        AND exp.status = "approved"
        """+queryPetty+"""
        GROUP BY exp.niifAccount
        ORDER BY Ammount desc
        """
        queryresult = db.session.execute(query, {"account": account , "date_start": date_start, "date_end": date_end, "pettyCash" : pettyCash})
        data = []
        labels = []
        total=0
        finalresult=[]
        for row in queryresult.fetchall():
            dictrow = dict(row)
            myrow = {}
            for i in dictrow:
                myrow[i]=json_serial(dictrow[i])
            finalresult.append(myrow)
            
            total=total + float(row.Ammount)
        percentage80 = 0
        totalOtros=0
        for row in finalresult:
            if (percentage80 <= 80):
                percentage80 = percentage80 + ((100*row.get('Ammount'))/total)
                data.append(float(row.get('Ammount')))
                labels.append(row.get('Name'))
            else:
                totalOtros= totalOtros + row.get('Ammount')
        
        data.append(totalOtros)
        labels.append("Otros")
        return { "data": data, "labels" : labels,'success': True , "Fontainebleau" : finalresult , "total_ammount" : total}
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(exc_obj)
        logging.error(exc_type)
        logging.error(fname + ': ' + str(exc_tb.tb_lineno))
        db.session.rollback()
        return {'success': False, 'message': str(e) },400
    finally:
        db.session.close()
