import os

# uncomment the line below for postgres database url from environment variable
# postgres_local_base = os.environ['DATABASE_URL']
basedir = os.path.abspath(os.path.dirname(__file__))

class Config:
    SECRET_KEY = os.getenv('SECRET_KEY', 'my_precious_secret_key')
    DEBUG = False


class DevelopmentConfig(Config):
    # uncomment the line below to use postgres
    # SQLALCHEMY_DATABASE_URI = postgres_local_base
    DEBUG = True
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SECRET_KEY = os.getenv('SECRET_KEY', 'my_secret_key')
    DB_USER = os.getenv('MYSQL_USER')
    DB_PASSWORD = os.getenv('MYSQL_PASSWORD')
    DB_NAME = os.getenv('MYSQL_DATABASE')
    DB_HOST = os.getenv('MYSQL_HOST')
    DB_PORT = os.getenv('MYSQL_ROOT')
    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://%s:%s@%s:%s/%s' % (
        DB_USER, DB_PASSWORD, DB_HOST, DB_PORT, DB_NAME)
    MAIL_SERVER='smtp.gmail.com'
    MAIL_PORT = 465
    MAIL_USERNAME = 'ttndevelop@gmail.com'
    MAIL_PASSWORD = 'ghnsejxvfzauidog'
    MAIL_USE_TLS = False
    MAIL_USE_SSL = True

class TestingConfig(Config):
    DEBUG = True
    TESTING = True
    DB_USER = os.getenv('MYSQL_USER')
    DB_PASSWORD = os.getenv('MYSQL_PASSWORD')
    DB_NAME = os.getenv('MYSQL_DATABASE')
    DB_HOST = os.getenv('MYSQL_HOST')
    DB_PORT = os.getenv('MYSQL_ROOT')
    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://%s:%s@%s:%s/%s' % (
        DB_USER, DB_PASSWORD, DB_HOST, DB_PORT, DB_NAME)
    PRESERVE_CONTEXT_ON_EXCEPTION = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    MAIL_SERVER='smtp.gmail.com'
    MAIL_PORT = 465
    MAIL_USERNAME = 'ttndevelop@gmail.com'
    MAIL_PASSWORD = 'ghnsejxvfzauidog'
    MAIL_USE_TLS = False
    MAIL_USE_SSL = True


class ProductionConfig(Config):
    DEBUG = False
    DB_USER = os.getenv('MYSQL_USER')
    DB_PASSWORD = os.getenv('MYSQL_PASSWORD')
    DB_NAME = os.getenv('MYSQL_DATABASE')
    DB_HOST = os.getenv('MYSQL_HOST')
    DB_PORT = os.getenv('MYSQL_ROOT')
    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://%s:%s@%s:%s/%s' % (
        DB_USER, DB_PASSWORD, DB_HOST, DB_PORT, DB_NAME)
    # uncomment the line below to use postgres
    # SQLALCHEMY_DATABASE_URI = postgres_local_base


config_by_name = dict(
    dev=DevelopmentConfig,
    test=TestingConfig,
    prod=ProductionConfig
)

key = Config.SECRET_KEY