from flask_restplus import Api
from flask import Blueprint
from flask_cors import CORS
import os
from .main.controller import *

blueprint = Blueprint('api', __name__)
CORS(blueprint)

api = Api(blueprint,
          prefix=os.environ["URL_MS"] + os.environ["API_VERSION"],
          title='Management',
          version=os.environ["VERSION"],
          description='Management API'
        )
api.add_namespace(management)
