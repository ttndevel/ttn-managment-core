FROM python:3.9

COPY . /app
WORKDIR /app

RUN pip install pipenv
RUN pipenv install --system --deploy --ignore-pipfile

# apt-get ofrece una forma sencilla de instalar paquetes desde la línea de órdenes.
RUN apt-get update && apt-get install python-dev gcc -y
RUN apt-get install -y vim
ENV TZ America/Bogota

EXPOSE 5000

CMD ["python", "manage.py", "run"]